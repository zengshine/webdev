import 'module-alias/register';

import fs from 'fs';

import { getWebWebpackConfig, getNodeWebpackConfig } from 'easywebpack-vue';
import { mapReduce } from '@root/shared/utils';
import { dedupePlugins } from '@root/build/utils';
import { isProduction } from '@root/shared/utils';

import webConfig from '@root/build/config/web';
import serverConfig from '@root/build/config/server';

const isProd = isProduction();

function normalizeConfig(config) {
  config.plugins = dedupePlugins(config);

  if (isProd) {
    const entry = config.entry;
    Object.entries(entry).forEach(([key, value]) => {
      const val = value as string[];
      if (val.filter)
        entry[key] = val.filter((item) => !item.includes('__webpack_hmr'));
    });
  }

  delete config.resolve.alias.vue;
  return config;
}

function getWebConfig(webConfig) {
  const config = mapReduce([getWebWebpackConfig, normalizeConfig])(webConfig);
  return config;
}

function getServerConfig(serverConfig) {
  const config = mapReduce([getNodeWebpackConfig, normalizeConfig])(
    serverConfig
  );
  return config;
}

const webpackConfig = [getWebConfig(webConfig), getServerConfig(serverConfig)];

fs.writeFileSync(
  './build-config.json',
  JSON.stringify(webpackConfig, null, 2),
  'utf-8'
);

export default webpackConfig;

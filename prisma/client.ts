import { PrismaClient } from '@prisma/client';

const PRISMA = Symbol('Application#prisma');

export default {
  get $prisma(): PrismaClient {
    if (!this[PRISMA]) {
      this[PRISMA] = new PrismaClient();
    }

    return this[PRISMA];
  }
};

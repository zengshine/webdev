/*
  Warnings:

  - You are about to alter the column `displayName` on the `authorization` table. The data in that column could be lost. The data in that column will be cast from `VarChar(512)` to `VarChar(128)`.
  - Added the required column `providerId` to the `users` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `authorization` MODIFY `displayName` VARCHAR(128) NOT NULL;

-- AlterTable
ALTER TABLE `users` ADD COLUMN `providerId` VARCHAR(128) NOT NULL;

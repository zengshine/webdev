import { Service } from 'egg';
import type { Prisma } from '@prisma/client';
/**
 * User Service
 */

export default class User extends Service {
  public model: Prisma.usersDelegate<any>;
  constructor(ctx) {
    super(ctx);
    this.model = this.app.$prisma.users;
  }

  public async findUnique(data) {
    return await this.model.findUnique({
      where: data
    });
  }

  public async findFirst(data) {
    return await this.model.findFirst({
      where: data
    });
  }

  public async create(data: Prisma.usersCreateInput) {
    return await this.model.create({ data });
  }
}

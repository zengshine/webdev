import { Service } from 'egg';
import type { Prisma, authorization } from '@prisma/client';
/**
 * Authorization Service
 */

export default class Authorization extends Service {
  public model: Prisma.authorizationDelegate<any>;
  constructor(ctx) {
    super(ctx);
    this.model = this.app.$prisma.authorization;
  }

  public async findUnique(query) {
    return await this.model.findUnique({
      where: query
    });
  }

  public async create(data: authorization) {
    return await this.model.create({
      data
    });
  }
}

import { Service } from 'egg';

/**
 * Base Service
 */

interface ServiceOptions {
  name: string;
}

export default class Base<T> extends Service {
  public model: T;
  constructor(ctx, options: ServiceOptions) {
    super(ctx);
    const { app } = this;
    const { name } = options;
    this.model = app.$prisma[name];
  }
}

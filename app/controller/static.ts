import { Controller } from 'egg';

import fs from 'fs';
import path from 'path';

import coRequest from 'request';

export default class StaticController extends Controller {
  public async serve() {
    const { ctx, app } = this;
    if (app.config.env === 'local') {
      const request = coRequest;
      ctx.body = request({
        url: `${app.config.webpack.proxy.host}/public/service-worker.js`,
        method: 'get'
      });
    } else {
      ctx.body = fs
        .readFileSync(path.join(this.app.baseDir, 'public/service-worker.js'), {
          encoding: 'utf-8'
        })
        .toString();
      ctx.set('Content-type', 'application/javascript; charset=utf-8');
      ctx.set('accept-ranges', 'bytes');
    }
  }
}

import { Controller } from 'egg';

export default class HomeController extends Controller {
  public async index() {
    const { ctx } = this;
    const { url } = ctx.request;
    await ctx.render('app.js', { name: 'webdev', url });
    const { body: renderContent } = ctx;
    ctx.body = renderContent;
  }
}

import { Controller } from 'egg';

export default class UserController extends Controller {
  public async authCallback(ctx) {
    ctx.body = {
      code: 0,
      success: true,
      msg: 'success',
      data: '/'
    };
  }

  logout(ctx) {
    ctx.logout();
    ctx.body = {
      code: 0,
      success: true,
      msg: 'success',
      data: '/login'
    };
  }
}

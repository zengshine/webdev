import type { Application } from 'egg';

export default (app: Application) => {
  const { controller, router } = app;

  app.passport.mount('github');
  app.passport.mount('yuque');

  router.post(
    '/api/login',
    app.passport.authenticate('local', { successRedirect: '/authCallback' })
  );
  router.get('/api/logout', controller.user.logout);

  router.get('/authCallback', controller.user.authCallback);
  router.get('/service-worker.js', controller.static.serve);

  router.redirect('/robots.txt', '/public/assets/robots.txt');

  // put it to the last to prevent override
  router.get('/*', controller.home.index);
};

import type { Context } from 'egg';

export default function authenticate() {
  return async (ctx: Context, next) => {
    if (!ctx.isAuthenticated()) {
      console.log('authenticate redirect=========================>');
      // ctx.redirect('/login');
    }

    await next();
  };
}

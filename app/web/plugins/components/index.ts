import type { App } from 'vue';

import VIcon from '@web/components/feature/v-icon';

export default {
  install(app: App) {
    app.component(VIcon.name, VIcon);
  }
};

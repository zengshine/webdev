import mitt from 'mitt';

const emitter = mitt();

export default {
  $on: (evt: string, handler: TypeEventHandler) => emitter.on(evt, handler),
  $off: (evt: string, handler: TypeEventHandler) => emitter.off(evt, handler),
  $emit: (evt: string, payload: TypeEventPayload) => emitter.emit(evt, payload)
};

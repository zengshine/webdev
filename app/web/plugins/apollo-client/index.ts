import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  split
} from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { setContext } from '@apollo/client/link/context';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { createClient as createWsClient } from 'graphql-ws';

import { isProduction } from '@root/shared/utils';

import type { NormalizedCacheObject } from '@apollo/client';

import { getCsrfToken } from '@web/utils/security';

export function createClient(args): ApolloClient<NormalizedCacheObject> {
  const { uri = 'graphql' } = args;

  const httpLink = createHttpLink({
    uri
  });

  const authLink = setContext((_, { headers }) => {
    const csrfToken = getCsrfToken();
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        'x-csrf-token': csrfToken
      }
    };
  });

  const protocol = process.env.NODE_ENV === 'production' ? 'wss' : 'ws';
  const port =
    process.env.NODE_ENV === 'production'
      ? ENV_APOLLO_WS_PORT_PROXY
      : ENV_APOLLO_WS_PORT;
  const wsLink = new GraphQLWsLink(
    createWsClient({
      url: `${protocol}://${SERVER_HOST}:${port}/graphql`
    })
  );

  // The split function takes three parameters:
  //
  // * A function that's called for each operation to execute
  // * The Link to use for an operation if the function returns a "truthy" value
  // * The Link to use for an operation if the function returns a "falsy" value
  const splitLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      );
    },
    wsLink,
    authLink.concat(httpLink)
  );

  const client = new ApolloClient({
    connectToDevTools: !isProduction(),
    link: splitLink,
    cache: new InMemoryCache()
  });

  return client;
}

/**
 * @file create axios
 */

import axios from 'axios';
import type { App } from 'vue';

import { getCsrfToken } from '@web/utils/security';

import requestSuccessInterceptor from '@web/plugins/axios/interceptors/requestSuccess';
import requestErrorInterceptor from '@web/plugins/axios/interceptors/requestError';
import responseSuccessInterceptor from '@web/plugins/axios/interceptors/responseSuccess';
import responseErrorInterceptor from '@web/plugins/axios/interceptors/responseError';

const csrfToken = getCsrfToken();

const defaultConfig = {
  baseURL: '',
  headers: {
    'x-csrf-token': csrfToken,
    'Content-Type': 'application/json'
  }
};

const axiosInstance = axios.create(defaultConfig);

axiosInstance.interceptors.request.use(
  requestSuccessInterceptor,
  requestErrorInterceptor
);
axiosInstance.interceptors.response.use(
  responseSuccessInterceptor,
  responseErrorInterceptor
);

export function $axios(options) {
  return axiosInstance(options) as unknown as Promise<ResponseData>;
}

export default {
  install: (app: App) => {
    app.config.globalProperties.$axios = axiosInstance;
  }
};

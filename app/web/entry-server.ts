import { createSSRApp } from 'vue';
import type { RenderOptions } from '@web/main';

export default (options: RenderOptions) => {
  const { App, router, store, renderCommon } = options;
  return async ({ url }) => {
    const app = createSSRApp(App);

    app.use(router);
    router.push(url);
    await router.isReady();

    // initial after router ready
    app.use(store);
    renderCommon(app);

    return { app, store };
  };
};

interface TypeEventHandler {
  (...args: any[]): void
}

interface TypeEventPayload {
  [key: string]: any
}

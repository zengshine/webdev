import '@root/typings/global';

interface stringObject {
  [key: string]: string;
}

declare global {
  interface Window {
    d3: any;
  }

  const process: {
    env: stringObject;
  };
}

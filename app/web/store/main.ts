import { defineStore } from 'pinia';

const useStore = defineStore('mainStore', {
  state() {
    return {
      name: 'pinia'
    };
  },
  getters: {
    computedName: (state) => `computed${state.name}`,

    referThisName() {
      return `refer this ${this.computedName}`;
    }
  },
  actions: {
    setName(val: string) {
      this.name = val;
    }
  }
});

export default useStore;

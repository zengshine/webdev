import { defineStore } from 'pinia';

const useStore = defineStore('mainStore', {
  state() {
    return {
      name: 'pinia'
    };
  }
});

export default useStore;

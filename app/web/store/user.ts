import { defineStore } from 'pinia';

import { THEME_TYPE } from '@web/const/common';

const useStore = defineStore('userStore', {
  state() {
    return {
      config: {
        theme: THEME_TYPE.default
      }
    };
  },

  actions: {
    setTheme(val: THEME_TYPE) {
      this.config.theme = val;
    }
  }
});

export default useStore;

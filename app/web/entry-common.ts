import type { App } from 'vue';

import PluginUseHead from '@root/shared/plugins/useHead';
import PluginComponent from '@web/plugins/components';

export default function (app: App) {
  app.use(PluginComponent);
  app.use(PluginUseHead);
}

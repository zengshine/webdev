import type { RouteRecordRaw } from 'vue-router';

const Login = () => import('@web/pages/login.vue');
const Home = () => import('@web/pages/home.vue');
const Content = () => import('@web/pages/content.vue');

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/content',
    name: 'content',
    component: Content
  }
];

export default routes;

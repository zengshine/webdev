import {
  createRouter,
  createWebHistory,
  createMemoryHistory
} from 'vue-router';

import routes from '@web/router/routes';

const createHistory = EASY_ENV_IS_NODE ? createMemoryHistory : createWebHistory;

const createVueRouter = () => {
  return createRouter({
    history: createHistory(),
    routes
  });
};

export default createVueRouter;

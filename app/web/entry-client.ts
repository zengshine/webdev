import { createSSRApp } from 'vue';
import type { RenderOptions } from '@web/main';

import pluginAxios from '@web/plugins/axios';
import { setAfterRenderedEntries } from '@root/shared/plugins/useHead';

function onBeforeMount() {
  const fontSvg = document.querySelector('svg[aria-hidden="true"]');
  if (fontSvg) {
    document.body.appendChild(fontSvg.cloneNode(true));
    fontSvg.remove();
  }
}

function onMounted() {
  setAfterRenderedEntries();
  document.documentElement.style.removeProperty('opacity');
}

export default async (options: RenderOptions) => {
  const { App, router, store, renderCommon } = options;
  const app = createSSRApp(App);

  app.use(pluginAxios);
  app.use(router);
  app.use(store);

  store.state.value = (window as any).__INITIAL_STATE__;
  renderCommon(app);
  await router.isReady();
  onBeforeMount();
  app.mount('#app', true);
  onMounted();
};

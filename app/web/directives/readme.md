## Hooks

```js
const myDirective = {
  // called before bound element's attributes
  // or event listeners are applied
  created(el, binding, vnode, prevVnode) {
    // see below for details on arguments
  },
  // called right before the element is inserted into the DOM.
  beforeMount(el, binding, vnode, prevVnode) {},
  // called when the bound element's parent component
  // and all its children are mounted.
  mounted(el, binding, vnode, prevVnode) {},
  // called before the parent component is updated
  beforeUpdate(el, binding, vnode, prevVnode) {},
  // called after the parent component and
  // all of its children have updated
  updated(el, binding, vnode, prevVnode) {},
  // called before the parent component is unmounted
  beforeUnmount(el, binding, vnode, prevVnode) {},
  // called when the parent component is unmounted
  unmounted(el, binding, vnode, prevVnode) {}
};
```

### Hook arguments

- `el`: the element the directive is bound to. This can be used to directly manipulate the DOM.

- `binding`: an object containing the following properties.

  - `value`: The value passed to the directive.
  - `oldValue`: The previous value, only available in `beforeUpdate` and `updated`.
  - `arg`: The argument passed to the directive, if any. For example in `v-my-directive:foo`, the argument would be: `"foo"`
  - `modifiers`: An object containing modifiers, if any. For example in `v-my-directive.foo.bar`, the modifiers object would be `{ foo: true, bar: true }`
  - `instance`: The instance of the component where the directive is used.
  - `dir`: The directive definition object.

- `vnode`: the underlying VNode representing the bound element.
- `preVnode`: the VNode representing the bound element from the previous render. Only available in `beforeUpdate` and `updated` hooks.

> Apart from `el`, you should treat these arguments as read-only and never modify them. If you need to share information across hooks, it >is recommended to do so through element's [dataset](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/dataset).

## Function Shorthand

It's common for a custom directive to have the same behavior for `mounted` and `updated`, with no need for the other hooks. In such cases we can define the directive as a function.

```js
app.directive('custom-directive', (el, binding) => {
  // this will be called for both `mounted` and `updated`
});
```

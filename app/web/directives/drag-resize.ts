import type { Directive } from 'vue';

const DIRECTION_MAP = {
  top: 'top',
  left: 'left',
  right: 'right',
  bottom: 'bottom'
};

function isHorizontal(direction) {
  const { left, right } = DIRECTION_MAP;
  return direction === left || direction === right;
}

function getDirection(modifiers) {
  return (
    Object.values(DIRECTION_MAP).find((dir) => modifiers[dir]) ||
    DIRECTION_MAP.right
  );
}

function getDragElementStyle(direction, size) {
  const offset = `${-size / 2}px`;
  const CURSOR_MAP = {
    left: 'e-resize',
    right: 'e-resize',
    top: 'n-resize',
    bottom: 'n-resize'
  };

  const styleObject: any = {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    cursor: CURSOR_MAP[direction]
  };

  const { top, right, bottom, left } = DIRECTION_MAP;

  // dragElement direction
  switch (direction) {
    case top:
      delete styleObject.bottom;
      styleObject.top = offset;
      break;
    case right:
      delete styleObject.left;
      styleObject.right = offset;
      break;
    case bottom:
      delete styleObject.top;
      styleObject.bottom = offset;
      break;
    case left:
      delete styleObject.right;
      styleObject.left = offset;
      break;
    default:
      break;
  }

  return styleObject;
}

function getDragElement(direction, size) {
  const el = document.createElement('div');
  const styleObject = getDragElementStyle(direction, size);
  const property = isHorizontal(direction) ? 'width' : 'height';
  Object.assign(styleObject, { [property]: `${size}px` });
  Object.assign(el.style, styleObject);

  return el;
}

function setupDrag(args: {
  el: HTMLDivElement;
  minSize: number;
  maxSize: number;
  direction: string;
}) {
  const { el, minSize, maxSize, direction } = args;
  const position = {
    preX: 0,
    preY: 0
  };
  const isHorizon = isHorizontal(direction);

  el.addEventListener('mousedown', handleMouseDown);

  function setPosition(args) {
    Object.assign(position, args);
  }

  function validatePosition(args) {
    const { x, y, rect } = args;
    const { left, right, top, bottom } = rect;
    const { left: l, right: r, top: t, bottom: b } = DIRECTION_MAP;
    let x1, x2, y1, y2;
    switch (direction) {
      case t:
        y1 = bottom - maxSize;
        y2 = bottom - minSize;
        break;
      case b:
        y1 = top + minSize;
        y2 = top + maxSize;
        break;
      case l:
        x1 = right + maxSize;
        x2 = right + minSize;
        break;
      case r:
        x1 = left + minSize;
        x2 = left + maxSize;
        break;
      default:
        break;
    }
    return isHorizon ? x1 <= x && x <= x2 : y1 <= y && y <= y2;
  }

  function validateDimension({ rect }) {
    const { width, height } = rect;
    const size = isHorizon ? width : height;
    return size > minSize && size < maxSize;
  }

  function handleMouseDown(evt) {
    const { pageX, pageY } = evt;
    setPosition({ preX: pageX, preY: pageY });
    document.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('mouseup', handleMouseUp);
  }

  function handleMouseUp() {
    document.removeEventListener('mousemove', handleMouseMove);
    document.removeEventListener('mouseup', handleMouseUp);
  }

  function handleMouseMove(evt) {
    evt.preventDefault();

    const { pageX, pageY } = evt;
    const rect = el.getBoundingClientRect();

    const { preX, preY } = position;
    const offsetX = pageX - preX;
    const offsetY = pageY - preY;
    setPosition({ preX: pageX, preY: pageY });

    const isPositionValid = validatePosition({ x: pageX, y: pageY, rect });
    const isDimensionValid = validateDimension({ rect });
    if (!isPositionValid && !isDimensionValid) return;

    const { width, height } = rect;
    const { bottom } = DIRECTION_MAP;
    if (isHorizon) {
      let w = width + offsetX; // TODO: offsetX value
      w = w > maxSize ? maxSize : w < minSize ? minSize : w;
      el.style.width = `${w}px`;
    } else {
      let h = direction === bottom ? height + offsetY : height - offsetY;
      h = h > maxSize ? maxSize : h < minSize ? minSize : h;
      el.style.height = `${h}px`;
    }
  }
}

function getMinSize(el: Element, min, direction) {
  const { clientWidth, clientHeight } = el;
  return min ? min : isHorizontal(direction) ? clientWidth : clientHeight;
}

function handleMounted(...args) {
  const [el, binding] = args;
  const { modifiers = {}, arg: dragEleSize = 5, value = {} } = binding;
  const { min, max } = value;
  const direction = getDirection(modifiers);

  const minSize = getMinSize(el, min, direction);
  const maxSize = max;

  const dragEle = getDragElement(direction, dragEleSize);
  el.appendChild(dragEle);

  setupDrag({ el, minSize, maxSize, direction });
}

const dragResize: Directive = {
  mounted(...arg) {
    handleMounted(...arg);
  }
};

export default dragResize;

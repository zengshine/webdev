import { NOOP } from '@root/shared/utils';

type Position = {
  x: number; // x of container
  y: number; // y of container
  preX: number; // x of dragged position
  preY: number; // y of dragged position
  offsetX: number; // offsetX between contain(left, top) an dragged position
  offsetY: number; // offsetY between contain(left, top) an dragged position
};

const setupDrag = (el, { wrapper, onDrag }) => {
  const position: Position = {
    x: 0,
    y: 0,
    preX: 0,
    preY: 0,
    offsetX: 0,
    offsetY: 0
  };
  const { width, height } = wrapper.getBoundingClientRect();

  el.addEventListener('mousedown', handleMousedown);
  el.addEventListener('mouseup', handleMouseup);

  function handleMousedown(evt) {
    const { pageX, pageY } = evt;
    const { x, y } = wrapper.getBoundingClientRect();
    const offsetX = pageX - x; // 点距离容器左边距离
    const offsetY = pageY - y; // 点距离容器顶边距离

    Object.assign(position, {
      x,
      y,
      preX: pageX,
      preY: pageY,
      offsetX,
      offsetY
    });
    document.addEventListener('mousemove', handleMousemove);
  }

  function handleMousemove(evt) {
    if (evt) evt.preventDefault();
    onDrag();

    let { pageX, pageY } = evt;
    const { preX, preY, x, y, offsetX, offsetY } = position;
    const { innerWidth, innerHeight } = window;

    const rangeTop = offsetY;
    const rangeBottom = innerHeight - (height - offsetY);
    const rangeLeft = offsetX;
    const rangeRight = innerWidth - (width - offsetX);

    pageX =
      pageX > rangeRight ? rangeRight : pageX < rangeLeft ? rangeLeft : pageX;
    pageY =
      pageY > rangeBottom ? rangeBottom : pageY < rangeTop ? rangeTop : pageY;

    const moveToX = x + pageX - preX;
    const moveToY = y + pageY - preY;

    Object.assign(wrapper.style, { top: `${moveToY}px`, left: `${moveToX}px` });
    Object.assign(position, {
      x: moveToX,
      y: moveToY,
      preX: pageX,
      preY: pageY
    });
  }

  function handleMouseup() {
    document.removeEventListener('mousemove', handleMousemove);
  }
};

const dragMove = {
  mounted: (el, binding) => {
    const { value = {} } = binding;
    const { selector, onDrag = NOOP } = value;
    const wrapper = (selector && document.querySelector(selector)) || el;
    setupDrag(el, { wrapper, onDrag });
  },
  unmounted: (el, binding) => {
    const { value = {} } = binding;
    const { selector, reset = true } = value;
    const wrapper = (selector && document.querySelector(selector)) || el;

    if (reset) {
      wrapper.style.left = null;
      wrapper.style.top = null;
    }
  }
};

export default dragMove;

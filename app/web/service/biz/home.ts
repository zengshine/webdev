import { $axios } from '@web/plugins/axios';

export const execGraphqlQuery = (data) => {
  return $axios({
    url: '/graphql',
    method: 'post',
    data
  });
};

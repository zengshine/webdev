import { $axios } from '@web/plugins/axios';

export const login = (data) => {
  return $axios({
    url: '/api/login',
    method: 'post',
    data
  });
};

export const logout = () => {
  return $axios({
    url: '/api/logout',
    method: 'get'
  });
};

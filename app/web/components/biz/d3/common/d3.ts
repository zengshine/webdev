let asyncD3;

export async function asyncGetD3() {
  if (asyncD3) return asyncD3;

  asyncD3 = await import('d3');

  // 挂载到全局对象
  if (window) window.d3 = asyncD3;

  return asyncD3;
}

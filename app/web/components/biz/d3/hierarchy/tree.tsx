import { defineComponent, onMounted, ref } from 'vue';

import './style.scss';

import Sidebar from '@web/components/biz/d3/hierarchy/components/side-bar.vue';
import ZoomTip from '@web/components/biz/d3/hierarchy/components/zoom-tip.vue';

import useInitEmitter from '@web/composables/useInitEmitter';
import useRegisterEvent from '@web/composables/useRegisterEvent';
import useFullScreen from '@web/composables/useFullScreen';

import getRenderUtils from '@web/components/biz/d3/hierarchy/utils/render';
import { debounce } from '@root/shared/utils/timer';
import { getNodeDirection } from '@web/components/biz/d3/hierarchy/utils/helper';
import { getInitialData } from '@web/components/biz/d3/hierarchy/const/data';
import {
  LOAD_STATE_TYPE,
  SIDE_BAR_TYPE,
  ZOOM_TYPE
} from '@web/components/biz/d3/hierarchy/const/common';

const { refresh, fullscreen, download, plus, minus } = SIDE_BAR_TYPE;
const actionList = [refresh, fullscreen, download, plus, minus].map(
  (item) => item.id
);

export default defineComponent({
  name: 'HierarchyWrapper',

  props: {
    visible: Boolean
  },

  setup() {
    const loadingState = ref();
    const zoomLevel = ref(0);
    const { drawChart, exportChart, addNode, handleRepaint, handleManualZoom } =
      getRenderUtils();
    const { enterFullScreen, exitFullScreen } = useFullScreen();
    const { registerEventListener } = useRegisterEvent();
    useInitEmitter([
      {
        event: 'company-diagram-co-graph-load-children',
        handler: handleLoadChildren
      },
      {
        event: 'company-diagram-relation-zoom-down',
        handler: handleZoomDown
      },
      {
        event: 'company-diagram-relation-zoom-up',
        handler: handleZoomUp
      },
      {
        event: 'company-diagram-relation-zoom-change',
        handler: handleZoomChange
      },
      {
        event: 'company-diagram-relation-refresh',
        handler: handleDrawChart
      },
      {
        event: 'company-diagram-relation-fullscreen',
        handler: handleEnterFullscreen
      },
      {
        event: 'company-diagram-relation-exit-fullscreen',
        handler: handleExitFullscreen
      },
      {
        event: 'company-diagram-relation-export',
        handler: handleExport
      }
    ]);

    registerEventListener({
      element: window,
      event: 'resize',
      handler: debounce(() => {
        handleRepaint(false);
      }, 200)
    });

    function setLoadingState(val) {
      loadingState.value = val;
    }

    function handleDrawChart() {
      drawChart({
        selector: '.js-co-graph__wrapper',
        data: getInitialData()
      });
    }

    async function handleExport() {
      setLoadingState(LOAD_STATE_TYPE.loading);
      await exportChart();
      setLoadingState(LOAD_STATE_TYPE.success);
    }

    async function handleEnterFullscreen() {
      await enterFullScreen();
      handleRepaint(true);
    }

    async function handleExitFullscreen() {
      await exitFullScreen();
      handleRepaint(false);
    }

    function handleZoomDown() {
      handleManualZoom(ZOOM_TYPE.down);
    }

    function handleZoomUp() {
      handleManualZoom(ZOOM_TYPE.up);
    }

    function handleZoomChange(val) {
      zoomLevel.value = val;
    }

    async function handleLoadChildren(node) {
      // 添加下级子节点
      const { _id, code, name, pid = '' } = node.data;
      const params = { _id, code, name, pid };
      if (pid) {
        params.pid = pid;
      }
      setLoadingState(LOAD_STATE_TYPE.loading);
      const { children = [] } = getInitialData(params);
      const dataList = children.filter(
        (child) => getNodeDirection(node.data) === getNodeDirection(child)
      );
      addNode({ dataList, parent: node });
      setLoadingState(LOAD_STATE_TYPE.success);
    }

    onMounted(() => {
      handleDrawChart();
    });

    return () => (
      <div class="h-full">
        <div class=" js-co-graph__wrapper h-full"></div>
        <Sidebar class="side-bar__wrapper" visibleList={actionList}></Sidebar>
        <ZoomTip data={zoomLevel.value}></ZoomTip>
      </div>
    );
  }
});

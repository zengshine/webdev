export const ZOOM_TYPE = {
  up: 'up',
  down: 'down'
};

export const LOAD_STATE_TYPE = {
  loading: 'loading',
  failed: 'failed',
  empty: 'empty',
  initialLoading: 'initialLoading',
  success: 'success',
  done: 'done'
};

export const SIDE_BAR_TYPE = {
  edit: { id: 1, slot: 'edit' }, // 编辑
  plus: { id: 2, slot: 'plus' }, // 放大
  minus: { id: 3, slot: 'minus' }, // 缩小
  refresh: { id: 4, slot: 'refresh' }, // 刷新
  fullscreen: { id: 5, slot: 'fullscreen' }, // 全屏
  exitFullscreen: { id: 5.1, slot: 'exitFullscreen' },
  download: { id: 6, slot: 'download' }, // 保存
  search: { id: 7, slot: 'search' } // 搜索
};

export const SIDE_BAR_CONFIG = [
  {
    id: SIDE_BAR_TYPE.search.id,
    name: '搜索',
    slot: SIDE_BAR_TYPE.search.slot,
    icon: 'icon-a-fuchuangshaixuan',
    evtName: 'company-diagram-relation-sidebar-search'
  },
  {
    id: SIDE_BAR_TYPE.edit.id,
    name: '编辑',
    slot: SIDE_BAR_TYPE.edit.slot,
    icon: 'icon-a-fuchuangbianji',
    evtName: 'company-diagram-relation-sidebar-edit'
  },
  {
    id: SIDE_BAR_TYPE.plus.id,
    name: '放大',
    slot: SIDE_BAR_TYPE.plus.slot,
    icon: 'icon-scale-up',
    evtName: 'company-diagram-relation-zoom-up',
    data: ZOOM_TYPE.up
  },
  {
    id: SIDE_BAR_TYPE.minus.id,
    name: '缩小',
    slot: SIDE_BAR_TYPE.minus.slot,
    icon: 'icon-scale-down',
    evtName: 'company-diagram-relation-zoom-down',
    data: ZOOM_TYPE.down
  },
  {
    id: SIDE_BAR_TYPE.refresh.id,
    name: '刷新',
    slot: SIDE_BAR_TYPE.refresh.slot,
    icon: 'icon-refresh',
    evtName: 'company-diagram-relation-refresh'
  },
  {
    id: SIDE_BAR_TYPE.fullscreen.id,
    name: '全屏',
    slot: SIDE_BAR_TYPE.fullscreen.slot,
    icon: 'icon-fullscreen',
    evtName: 'company-diagram-relation-fullscreen'
  },
  {
    id: SIDE_BAR_TYPE.exitFullscreen.id,
    name: '退出',
    slot: SIDE_BAR_TYPE.exitFullscreen.slot,
    icon: 'icon-fullscreen',
    evtName: 'company-diagram-relation-exit-fullscreen'
  },
  {
    id: SIDE_BAR_TYPE.download.id,
    name: '保存',
    slot: SIDE_BAR_TYPE.download.slot,
    icon: 'icon-export',
    evtName: 'company-diagram-relation-export'
  }
];

import * as d3 from 'd3';

import $emitter from '@web/plugins/emit';

import { sleep } from '@root/shared/utils/async';
import {
  getZoomRange,
  svgToImage
} from '@web/components/biz/d3/hierarchy/utils/common';

import {
  NODE_TYPE,
  NODE_TYPE_ORDER,
  diagonalType,
  hasNext,
  hasChildren,
  isDirectionIconVisible,
  initHierarchy,
  isInnerRelation,
  isPerson,
  isChildVisible,
  getInitTreeData,
  getNodeColor,
  getCommonProperty,
  getNodeFieldText,
  setDirection,
  nodeTextRender,
  appendNodeFieldText,
  calChartDimension,
  visit,
  getSearchNodeLength,
  isMajorStockHolder,
  getChildLength,
  getNodeTypeClass,
  checkIsEmptyStockPercent
} from '@web/components/biz/d3/hierarchy/utils/helper';
import type { HierarchyNode } from 'd3';

type NodeType = d3.HierarchyPointNode<unknown> & {
  nodeHeight?: number;
};

type SelectionNode = d3.Selection<any, unknown, null, undefined> & {
  $setTransition?: any;
};

const nodeMargin = { top: 5, right: 20, bottom: 5, left: 100 };
const strokeWidth = 0.5;
const rectDimension = {
  paddingH: 10,
  toggleIconWidth: 12,
  toggleIconPadding: 4,
  toggleIconColor: '#404247'
};

let rootList,
  rootSvg,
  rootSelector,
  rootSvgGroup,
  rootViewWidth,
  rootViewHeight,
  rootZoomListener;
let searchNodeParent, layout;

let _id = 0; // unique id
const defaultDuration = 500;
let globalDuration = defaultDuration;
let isFullScreenMode = false;
const expandCount = 10;
const searchCount = 20;
const rectangleWidth = 150;
let rectangleHeight = 0;
const nodeFontSize = 12;
let isAnimating = false;
let animatingTimeoutId: number;
const diagonal = diagonalType.align;

/**
 * differ from mobile - start
 */
const { zoomRange, getZoomLevel, setZoomLevel } = getZoomRange({
  min: 0.1,
  max: 3
});

function setInitialZoom() {
  console.log('setInitialZoom=========================>');
}

function handleOpenDetailPage(d) {
  const { _id } = d.data;
  console.log('handleOpenDetailPage=========================>', _id);
}

function handleSearchNode(searchNodes) {
  $emitter.$emit('company-diagram-co-graph-search-node', searchNodes);
}

function handleLoadChildren(d) {
  $emitter.$emit('company-diagram-co-graph-load-children', d);
}

function handleShowPopup(params) {
  $emitter.$emit('company-diagram-co-graph-pop-up', params);
}

function handleMobilePopup(evt, d) {
  console.log('handleMobilePopup=========================>', evt, d);
}

// function getEncoderOptions() {
//   return 0.5;
// }

function handleZoomCallback(k) {
  setZoomLevel(k);
  $emitter.$emit('company-diagram-relation-zoom-change', k);
}

function getTranslateOptions(d) {
  const { left } = d.data;
  const hOffset = left ? 1 / 2 : 1 / 2;
  const offsetParams = { hOffset };
  return offsetParams;
}

function setPathAnimationFlag() {
  if (animatingTimeoutId) clearTimeout(animatingTimeoutId);

  isAnimating = true;
  animatingTimeoutId = setTimeout(() => {
    isAnimating = false;
  }, 1000);
}

/**
 * differ from mobile - end
 */

const setD3 = async () => {
  d3.selection.prototype.$setTransition = function (
    params = {
      duration: globalDuration,
      ease: d3.easeQuadOut
    }
  ) {
    const { duration = globalDuration, ease = d3.easeQuadOut } = params; // TODO: 默认duration 获取方式
    return this.transition().ease(ease).duration(duration);
  };
};

const setDuration = (val = defaultDuration) => {
  globalDuration = val;
};

async function drawChart(
  options = {} as {
    selector: any;
    data: any;
  }
) {
  // Async import d3
  await setD3();

  if (rootSvg) rootSvg.remove();

  const { selector, data } = options;
  rootSelector = selector;

  // Init Root Data
  setDirection(data.children);
  initDataSource(data, true);
  const treeDataList = getInitTreeData(data);
  rootList = treeDataList!.map((item) => getInitialRoot(item));

  // Init Layout
  setInitialWH();
  // layout = getInitialLayout({ width: rootViewHeight, height: rootViewWidth })

  // Init Svg Container
  const svgId = `exportSvg-id-${++_id}`;
  rootSvg = getInitialSvg({
    selector,
    width: rootViewWidth,
    height: rootViewHeight,
    id: svgId
  });
  rootSvgGroup = rootSvg.append('g');
  rootZoomListener = getInitialZoom(rootSvgGroup, handleZoomCallback);
  rootSvg.call(rootZoomListener);
  setInitialZoom();

  setPathAnimationFlag();

  // Initial Paint
  setDuration();
  rootList.forEach((root) => {
    calNodeSize(root);
    update(root, root);
  });
  setDuration(0);
  centerNode(rootList[0]);
  setDuration();
}

function setInitialWH() {
  let width, height;

  if (isFullScreenMode) {
    const { innerWidth, innerHeight } = window;
    width = innerWidth;
    height = innerHeight;
  } else {
    const wrapper = document.querySelector(rootSelector);
    const { offsetWidth, offsetHeight } = wrapper;
    width = offsetWidth;
    height = offsetHeight;
  }

  rootViewWidth = width;
  rootViewHeight = height;
}

function getInitialZoom(svgGroup = rootSvgGroup, callback?) {
  const handleZoom = (evt) => {
    if (!evt.transform) return;

    const { k } = evt.transform;
    if (callback) callback(k);
    svgGroup.attr('transform', evt.transform);
  };

  const zoomListener = d3.zoom().scaleExtent(zoomRange).on('zoom', handleZoom);

  return zoomListener;
}

function getInitialSvg({ width, height, selector, id }) {
  const rootSvg = d3
    .select(selector)
    .append('svg')
    .attr('id', id)
    .attr('width', width)
    .attr('height', height)
    .attr('xmlns', 'http://www.w3.org/2000/svg')
    .attr('version', '1.1')
    .attr('class', 'svg-overlay')
    .attr('font-size', nodeFontSize);

  return rootSvg;
}

function getInitialRoot(data) {
  const root: d3.HierarchyNode<any> & {
    x0?: number;
    y0?: number;
  } = initHierarchy(data);

  root.x0 = rootViewHeight / 2;
  root.y0 = 0;

  return root;
}

function setSvgDimension(svg = rootSvg) {
  svg.attr('width', rootViewWidth).attr('height', rootViewHeight);
}

function handleRepaint(isFullScreen) {
  isFullScreenMode = isFullScreen;

  setInitialWH();
  setSvgDimension();
  centerNode(rootList[0]);
}

function initDataSource(root, isRoot?) {
  // 处理搜索节点
  const setSearchable = (d) => {
    let expandable = false;
    let searchable = false;
    let limitCount = 0;
    const { children = [] } = d;
    const childCount = children.length;

    if (childCount <= expandCount) {
      return;
    } else if (childCount > expandCount && childCount <= searchCount) {
      expandable = true;
      limitCount = expandCount;
    } else {
      searchable = true;
      limitCount = expandCount;
    }

    const searchNodes = children.splice(limitCount);
    d.searchNodes = searchNodes;

    // 新增搜索节点
    children.push({
      searchable,
      expandable,
      code: NODE_TYPE.SEARCH,
      name: searchable ? '搜索' : '展开'
    });
  };

  // 继承父节点属性（方向）
  const inheritProps = (d) => {
    const { children = [] } = d;

    if (!children.length) return;

    for (const child of children) {
      Object.assign(child, {
        left: d.left
      });
    }
  };

  const callback = (d) => {
    if (isRoot && d === root) return; // 根节点不做处理

    setSearchable(d);
    inheritProps(d); // 需要后置，保证包含前置处理
  };

  visit(root, callback);

  return root;
}

function calNodeSize(root) {
  const effectData = {};
  let maxHeight = 0;
  const padding = {
    v: 6,
    h: 10
  }; // 需要跟样式配置保持一致

  const gWrapper = rootSvgGroup.append('g');
  const wrapper = gWrapper
    .append('text')
    .append('tspan')
    .attr('style', `font-size: ${nodeFontSize}px;`);

  const visitCallback = (d) => {
    wrapper.text(nodeTextRender(d));

    const wrapperNode = wrapper.node();
    const { width, height } = wrapperNode.getBBox
      ? wrapperNode.getBBox()
      : wrapperNode.getBoundingClientRect();
    const nodeHeight = height;
    const nodeWidth = width;

    const { toggleIconWidth, toggleIconPadding } = rectDimension;
    const iconWidth = hasNext(d) ? toggleIconWidth + toggleIconPadding : 0;
    d.nodeHeight = nodeHeight + padding.v * 2;
    d.nodeWidth = nodeWidth + padding.h * 2 + iconWidth;
    // 无持股比例的时候，宽度重置
    if (!checkIsEmptyStockPercent(d)) {
      d.nodeWidth = nodeWidth + iconWidth;
    }
    maxHeight = Math.max(nodeHeight, maxHeight);
  };

  visit(root, visitCallback, (d) => d.children, effectData);
  gWrapper.remove();

  rectangleHeight = Math.max(rectangleHeight, maxHeight);
}

function centerNode(
  node,
  options: {
    svg?: any;
    zoomListener?: any;
    viewWidth?: number;
    viewHeight?: number;
    hOffset?: number;
  } = {
    svg: rootSvg,
    zoomListener: rootZoomListener,
    viewWidth: rootViewWidth,
    viewHeight: rootViewHeight,
    hOffset: 0.5
  }
) {
  const {
    svg = rootSvg,
    zoomListener = rootZoomListener,
    viewWidth = rootViewWidth,
    viewHeight = rootViewHeight,
    hOffset = 0.5
  } = options;
  let x, y;

  const { left } = node.data;
  const direction = node.depth ? (left ? 1 : -1) : 0;

  const t = d3.zoomTransform(svg.node());
  x = -node.y0;
  y = -node.x0;
  x = x * t.k + viewWidth * hOffset + (node.nodeWidth * direction) / 2;
  y = y * t.k + viewHeight / 2;

  svg
    .$setTransition()
    .call(zoomListener.transform, d3.zoomIdentity.translate(x, y).scale(t.k));
}

function handleManualZoom(
  type,
  svg = rootSvg,
  zoomListener = rootZoomListener
) {
  const zoomLevel = getZoomLevel(type);
  const t = d3.zoomTransform(svg.node());

  svg
    .$setTransition()
    .call(
      zoomListener.transform,
      d3.zoomIdentity.translate(t.x, t.y).scale(zoomLevel)
    );
}

function calLevelWidth(level, nodeList, levelWidth) {
  const width = Math.max(...nodeList.map((node) => node.nodeWidth));
  levelWidth[level] = width;
  const childList = nodeList.reduce((pre, cur) => {
    const children = cur.children || [];
    return [...pre, ...children];
  }, []);

  if (!childList.length) return;

  calLevelWidth(++level, childList, levelWidth);
}

function setUpdateLayout() {
  return d3
    .tree()
    .nodeSize([
      rectangleHeight + nodeMargin.top + nodeMargin.bottom,
      rectangleWidth
    ])
    .separation((a: NodeType, b: NodeType) => {
      if (a.parent !== b.parent) {
        return 2;
      }

      return a.nodeHeight === b.nodeHeight ? 1.45 : 1.6;
    });
}

function update(source, root, svgGroup = rootSvgGroup) {
  // TODO: seperate data from rendering

  // Compute the new height, function counts total children of root node and sets tree height accordingly.
  // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
  // This makes the layout more consistent.

  const { left } = root.data;
  const direction = left ? -1 : 1;

  const levelWidth = [0];
  calLevelWidth(0, [root], levelWidth);

  layout = setUpdateLayout();

  root.sort((a, b) => {
    return d3.descending(
      NODE_TYPE_ORDER[a.data.code],
      NODE_TYPE_ORDER[b.data.code]
    );
  });

  const rootData = layout(root);

  const nodes = rootData.descendants();
  const links = rootData.descendants().slice(1);

  nodes.forEach((d) => {
    const { data, depth } = d;

    if (depth === 0) return 0;

    const direction = data.left ? -1 : 1;
    const levelOffset =
      levelWidth
        .slice(1, depth)
        .reduce((pre, cur) => pre + cur, levelWidth[0] / 2) +
      depth * 80;

    d.y = levelOffset * direction;
  });

  const { class: nodeClass } = root.data;

  // binding
  const node = svgGroup
    .selectAll(`g.${nodeClass}`)
    .data(nodes, (d) => d.id || (d.id = ++_id));

  const getSourceOffsetX = (d, isPath?) => {
    let offsetX = d.nodeWidth;
    if (left) {
      if (!isPath) {
        offsetX = -offsetX;
      } else {
        offsetX = 0;
      }
    } else {
      offsetX = source.nodeWidth;
    }
    return offsetX;
  };

  const getSourceX = (d, isPath?) => {
    const translateX = getGTranslateX(source);
    const offsetX = getSourceOffsetX(d, isPath);
    return translateX + offsetX;
  };

  const nodeEnter = node
    .enter()
    .append('g')
    .attr('class', (d) => {
      const nodeTypeClass = getNodeTypeClass(d);
      return `node ${nodeClass} ${nodeTypeClass}`;
    })
    .attr('width', (d) => `${d.nodeWidth}px`)
    .attr('height', (d) => `${d.nodeHeight}px`)
    .attr('transform', (d) => {
      return `translate(${getSourceX(d)}, ${source.x0 || source.x})`;
    })
    .attr('id', (d) => d.id)
    .attr('cursor', 'pointer')
    .on('click', handleNodeClick)
    .on('mouseover', handleNodeMouseOver)
    .on('mouseout', handleNodeMouseOut);

  const nodeEnterRect = nodeEnter
    .append('rect')
    .attr('width', (d) => `${d.nodeWidth}px`)
    .attr('height', (d) => `${d.nodeHeight}px`)
    .attr('y', (d) => -d.nodeHeight / 2)
    .attr('rx', 2)
    .attr('ry', 2)
    .attr('fill', (d) => (isPerson(d) ? getNodeColor(d) : '#fff'))
    .attr('fill-opacity', (d) => (isPerson(d) ? 0.2 : 0))
    .attr('stroke-width', strokeWidth)
    .attr('stroke', (d) =>
      isDirectionIconVisible(d) ? 'none' : getNodeColor(d)
    );

  nodeEnterRect
    .filter((d) => d.depth === 0)
    .attr('fill', '#3981F4')
    .attr('fill-opacity', 1)
    .attr('stroke', 'none');

  const nodeEnterText = nodeEnter
    .append('text')
    .attr('class', 'nodeText')
    .attr('dy', '.35em')
    .attr('x', (d) => {
      const { depth } = d;
      const { toggleIconPadding, toggleIconWidth, paddingH } = rectDimension;

      if (!depth || !hasNext(d)) return paddingH;

      return left ? paddingH + toggleIconPadding + toggleIconWidth : paddingH;
    });

  nodeEnterText.filter((d) => d.depth === 0).attr('fill', '#fff');
  nodeEnterText
    .append('tspan')
    .attr('class', 'tspan-field-name')
    .text((d) => d.data.name);

  appendNodeFieldText({
    node: nodeEnterText,
    filterFn: getNodeFieldText,
    valueFn: getNodeFieldText
  });
  appendNodeFieldText({
    node: nodeEnterText,
    filterFn: isMajorStockHolder,
    valueFn: () => '大股东',
    append: true
  });
  appendNodeFieldText({
    node: nodeEnterText,
    filterFn: isDirectionIconVisible,
    valueFn: (d) => getChildLength(d),
    char: ['(', ')']
  });

  const symbolList = [
    {
      name: 'symbolTriangle',
      offset: 24,
      size: 40
    },
    {
      name: 'symbolCircle',
      offset: 4,
      size: 100
    }
  ];

  symbolList.forEach((item) => {
    nodeEnter
      .append('path')
      .attr('d', d3.symbol().size(item.size).type(d3[item.name])())
      .attr('transform', (d) => {
        const nodeWidth = left ? d.nodeWidth : 0;
        const offsetX = (item.offset + nodeWidth) * direction * -1;
        const rotateDeg = isInnerRelation(d)
          ? d.data.left
            ? 90
            : -90
          : d.data.left
          ? -90
          : 90;
        return `translate(${offsetX}, ${0}) rotate(${rotateDeg})`;
      })
      .attr('fill', (d) =>
        isDirectionIconVisible(d) ? getNodeColor(d) : 'none'
      );
  });

  const getNodeTranslate = (d) => {
    const { nodeWidth } = d;
    const { toggleIconWidth, paddingH } = rectDimension;
    const visible = isDirectionIconVisible(d);
    const visibleOffsetX = visible
      ? left
        ? -toggleIconWidth
        : toggleIconWidth
      : 0;
    const offsetX = left ? paddingH : nodeWidth - toggleIconWidth - paddingH;
    return `translate(${offsetX + visibleOffsetX}, 0)`;
  };

  const nodeEnterGroup = nodeEnter
    .filter((d) => hasNext(d))
    .append('g')
    .attr('transform', getNodeTranslate)
    .attr('class', (d) => {
      const iconType = isChildVisible(d)
        ? 'g-rect-toggle-icon--minus'
        : 'g-rect-toggle-icon--plus';
      return `g-rect-toggle-icon-toggle ${iconType}`;
    });

  nodeEnterGroup
    .append('circle')
    .attr('r', 6)
    .attr('cx', 6)
    .attr('fill', '#fff')
    .attr('fill-opacity', 0)
    .attr('stroke', rectDimension.toggleIconColor);

  nodeEnterGroup
    .append('line')
    .attr('x1', 2)
    .attr('y1', 0)
    .attr('x2', 10)
    .attr('y2', 0)
    .attr('stroke-width', 1)
    .attr('stroke', rectDimension.toggleIconColor);

  const nodeUpdate = nodeEnter.merge(node);

  function getGTranslateX(d) {
    const { data, nodeWidth, parent } = d;
    let translateX = d.y;

    if (data.left) translateX = d.y - nodeWidth;

    if (!parent) translateX = d.y - nodeWidth / 2;
    return translateX;
  }

  nodeUpdate.$setTransition({}).attr('transform', (d) => {
    const translateX = getGTranslateX(d);

    return `translate(${translateX}, ${d.x})`;
  });

  const nodeUpdateText = nodeUpdate.selectAll('text.nodeText');
  nodeUpdateText.selectAll('tspan.tspan-field-name').text((d) => d.data.name);
  appendNodeFieldText({
    node: nodeUpdateText,
    filterFn: (d) => d.data.searchable,
    valueFn: getSearchNodeLength,
    color: '#000',
    char: ['(', ')']
  });
  appendNodeFieldText({
    node: nodeUpdateText,
    filterFn: (d) => d.data.expandable,
    valueFn: getSearchNodeLength,
    color: '#000',
    char: ['(', ')']
  });

  const nodeUpdatePlus = nodeUpdate.filter((d) => !isChildVisible(d));
  const nodeUpdateMinus = nodeUpdate.filter((d) => isChildVisible(d));
  nodeUpdatePlus.selectAll('line.g-toggle-line-plus').remove();
  nodeUpdatePlus
    .selectAll('g.g-rect-toggle-icon-toggle')
    .append('line')
    .attr('class', 'g-toggle-line-plus')
    .attr('x1', 6)
    .attr('y1', -4)
    .attr('x2', 6)
    .attr('y2', 4)
    .attr('stroke-width', 1)
    .attr('stroke', rectDimension.toggleIconColor);
  nodeUpdateMinus.selectAll('line.g-toggle-line-plus').remove();

  const nodeExit = node.exit();
  nodeExit
    .$setTransition()
    .attr('transform', (d) => {
      const offsetX = getSourceX(d);
      return `translate(${offsetX}, ${source.x})`;
    })
    .remove();

  nodeExit
    .selectAll('rect')
    .$setTransition()
    .attr('stroke', 'none')
    .attr('fill', 'none');

  nodeExit.selectAll('.g-rect-toggle-icon-toggle').remove();
  nodeExit.selectAll('path').remove();

  nodeExit
    .select('text')
    .$setTransition({ duration: globalDuration / 2 })
    .style('fill-opacity', 0);

  nodeExit.each((d) =>
    rootSvg.selectAll(`.path-parent-id-${d.parent.id}`).remove()
  );

  const link = svgGroup.selectAll(`path.${nodeClass}`).data(links, (d) => d.id);

  const linkEnter = link
    .enter()
    .insert('path', 'g')
    .attr('id', (d) => `path-${d.id}`)
    .attr(
      'class',
      (d) =>
        `link ${nodeClass} path-parent-id-${d.parent.id} path-self-id-${d.id}`
    )
    .attr('fill', 'none')
    .attr('stroke', '#AFB1B7')
    .attr('stroke-width', strokeWidth)
    .attr('d', (d) => {
      const offsetX = getSourceX(d, true);
      const o = {
        x: source.x0 || source.x,
        y: offsetX
      };
      return diagonal(o, o);
    });

  const linkUpdate = linkEnter.merge(link);

  // Transition back to the parent element position
  linkUpdate
    .$setTransition()
    .attr('stroke-width', strokeWidth)
    .attr('d', function (d) {
      const { parent, data } = d;
      const { x, y, nodeWidth } = parent;
      const offset = parent.depth ? nodeWidth : nodeWidth / 2;

      const targetY = data.left ? y - offset : y + offset;
      const target = { x, y: targetY };

      return diagonal(d, target);
    });

  // Remove any exiting links
  link
    .exit()
    .$setTransition()
    .attr('d', (d) => {
      const offsetX = getSourceX(d, true);
      const o = { x: source.x, y: offsetX };
      return diagonal(o, o);
    })
    .remove();

  // Stash the old positions for transition.
  nodes.forEach(function (d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });

  function updateNode(d) {
    const root = rootList.find((item) => !!item.data.left === !!d.data.left);
    update(d, root);
  }

  function handleNodeClick(evt, d) {
    // 节点点击事件中心 处理节点展开收起
    if (evt.defaultPrevented) return;

    if (!d.depth) {
      handleOpenDetailPage(d);
      return;
    }

    handlePathAnimation(d);
    setPathAnimationFlag();

    const { expandable, searchable, expanded, type } = d.data;

    const searchNodes = d.parent.data.searchNodes;

    const offsetParams = getTranslateOptions(d);

    if (expandable && !expanded) {
      searchNodeParent = d.parent;
      if (expanded === false) {
        searchNodeParent.children.splice(
          searchNodeParent.children.length - 1,
          0,
          ...searchNodes.splice(0)
        );
        d.data.expanded = true;
        d.data.name = '收起';
        updateNode(d.parent);
      } else {
        addNode({
          dataList: [...searchNodes],
          parent: searchNodeParent,
          expandable
        });
      }
      return;
    }

    if (expandable && expanded) {
      searchNodeParent = d.parent;
      const { children } = searchNodeParent;
      children.pop();
      searchNodeParent.data.searchNodes = children.slice(expandCount);
      searchNodeParent.children = children.slice(0, expandCount);
      d.data.expanded = false;
      d.data.name = '展开';
      searchNodeParent.children.push(d);
      updateNode(d.parent);
      centerNode(searchNodeParent, offsetParams);
      return;
    }

    if (searchable) {
      searchNodeParent = d.parent;
      handleSearchNode(searchNodes);
      return;
    }

    if (type) {
      handleMobilePopup(evt, d);
    }
    // 判断下级节点是第一次展开新调接口，还是折叠展开
    if (hasChildren(d)) {
      d = toggleChildren(d);
      updateNode(d);
      centerNode(d, offsetParams);
    } else {
      // 第一次展开 新调用接口
      if (hasNext(d)) {
        handleLoadChildren(d);
        centerNode(d, offsetParams);
      } else {
        centerNode(d, offsetParams);
      }
    }
  }

  function handlePathAnimation(d, type?) {
    if (isAnimating) return;

    const dashArray = [6, 6];
    const fillColor = getNodeColor(d);
    const direction = isInnerRelation(d) ? 1 : -1;
    const parentPath = rootSvg.selectAll(`.path-parent-id-${d.id}`);
    const selfPath = rootSvg.selectAll(`.path-self-id-${d.id}`);

    const animate = (node) => {
      const pathLength = node.getTotalLength();
      const dashCount = parseInt(
        String(pathLength / dashArray.reduce((a, b) => a + b, 0))
      );
      const pathDashArray = new Array(dashCount).join(
        dashArray.join(',') + ' '
      );

      const duration = parseInt(String(pathLength / 0.05));

      const selectNode: SelectionNode = d3.select(node);

      selectNode
        .attr('stroke', fillColor)
        .attr('stroke-dashoffset', pathLength * direction)
        .attr('stroke-dasharray', pathDashArray)
        .$setTransition({ duration, ease: d3.easeLinear })
        .attr('stroke-dashoffset', 0)
        .on('end', () => {
          animate(node);
        });
    };

    const removeAnimate = (node) => {
      node
        .attr('stroke-dashoffset', 0)
        .attr('stroke-dasharray', '')
        .attr('stroke', '#AFB1B7')
        .transition();
    };

    if (type === 'add') {
      const pathList = [...parentPath.nodes(), ...selfPath.nodes()];
      pathList.forEach((node) => {
        animate(node);
      });
    } else {
      removeAnimate(parentPath);
      removeAnimate(selfPath);
    }
  }

  function handleNodeMouseOver(evt, d) {
    const { type } = d.data;

    if (!type) {
      handlePathAnimation(d, 'add');
      return;
    }

    const params = {
      target: evt.currentTarget,
      d,
      status: true
    };
    handleShowPopup(params);
  }

  function handleNodeMouseOut(evt, d) {
    const { type } = d.data;

    if (!type) {
      handlePathAnimation(d);
    }

    const params = {
      d,
      status: false,
      target: evt.target
    };
    handleShowPopup(params);
  }
}

function setEditState(state) {
  const callback = (d) => {
    d.editable = state;
  };

  rootList.forEach((root) => {
    visit(root, callback);
    update(root, root);
  });
}

function addNode({
  dataList = [],
  parent = searchNodeParent,
  expandable
}: {
  dataList: any[];
  parent: any;
  expandable?: boolean;
}) {
  // TODO: 统一参数
  if (!dataList.length) return;

  setPathAnimationFlag();

  const { searchNodes = [] } = parent.data;

  const nodeList: HierarchyNode<any>[] = [];

  const updateHierarchy = (node) => {
    // later added some properties to Node like child,parent,depth
    const { parent } = node;
    node.depth = parent.depth + 1;
    node.height = parent.height - 1;
    node.parent = parent;
    node.id = ++_id;
    calNodeSize(node);
  };

  for (const data of dataList) {
    // Creates a Node from newNode object using d3.hierarchy(.)
    const initialData = initDataSource(
      Object.assign(getCommonProperty(parent.data), data)
    );
    const node = initHierarchy(initialData);
    node.parent = parent;
    visit(node, updateHierarchy);

    nodeList.push(node);
  }

  // parent is a node, to which we are adding the new node as a child
  // If no child array, create an empty array
  if (!parent.children) {
    parent.children = [];
    parent.data.children = [];
  }

  if (searchNodes.length) {
    for (const data of dataList) {
      const nodeIndex = parent.data.searchNodes.findIndex(
        (item) => item.name === data.name
      );
      if (nodeIndex > -1) parent.data.searchNodes.splice(nodeIndex, 1);
    }
  }

  if (!searchNodes.length) {
    const lastChild = parent.children.pop();
    // parent.data.children = []
    if (expandable) {
      lastChild.data.name = '收起';
      lastChild.data.expanded = true;
      lastChild.id = ++_id;
      parent.children.push(lastChild);
    }
  }

  // Push it to parent.children array
  const insertIndex =
    searchNodes.length || expandable
      ? parent.children.length - 1
      : parent.children.length;
  parent.children.splice(insertIndex, 0, ...nodeList);
  parent.data.children.splice(insertIndex, 0, ...dataList);

  rootList.forEach((root) => {
    update(parent, root);
  });
  const offsetParams = getTranslateOptions(parent);
  centerNode(parent, offsetParams);
}

function toggleChildren(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else if (d._children) {
    d.children = d._children;
    d._children = null;
  }

  return d;
}

async function exportChart() {
  await sleep();

  const padding = {
    v: 150,
    h: 400
  };
  const dimension = calChartDimension(rootList);
  const { vw, vh, minX, minY } = dimension;
  const wrapperWidth = vw + padding.v * 2;
  const wrapperHeight = vh + padding.h * 2;
  const selectorClass = 'js-diagram-export__wrapper';
  const selector = `.${selectorClass}`;
  const rootEle = document.querySelector(rootSelector);
  const exportWrapper = document.createElement('div');
  exportWrapper.setAttribute(
    'style',
    `position: absolute;z-index: -1;width:${wrapperHeight}px;height:${wrapperWidth}px;`
  );
  exportWrapper.setAttribute('class', `${selectorClass} qzd-watermark-bg`);
  rootEle.appendChild(exportWrapper);
  const tipWrapper = document.querySelector('.js-data-source__tips');
  if (tipWrapper) {
    const exportTipWrapper = tipWrapper!.cloneNode(true);
    exportWrapper.appendChild(exportTipWrapper);
  }

  const svgId = `exportSvg-id-${++_id}`;
  const svg = getInitialSvg({
    selector,
    width: wrapperHeight,
    height: wrapperWidth,
    id: svgId
  });
  const svgGroup = svg.append('g');
  const zoomListener = getInitialZoom(svgGroup);
  svg.call(zoomListener as any);

  setDuration(0);
  rootList.forEach((root) => update(root, root, svgGroup));
  setDuration();
  svg.call(
    zoomListener.transform as any,
    d3.zoomIdentity
      .translate(Math.abs(minY) + padding.h, Math.abs(minX) + padding.v)
      .scale(1)
  );

  await sleep(globalDuration);
  const date = new Date();
  await svgToImage({
    svgElement: document.getElementById(svgId),
    name: `${rootList[0].data.name}-企业图谱-${date.getFullYear()}${
      date.getMonth() + 1
    }${date.getDate()}`
    // encoderOptions: getEncoderOptions()
  });
  rootEle.removeChild(exportWrapper);
}

function clearChart() {
  if (rootSvg) rootSvg.remove();
}

export default function () {
  return {
    drawChart,
    setEditState,
    addNode,
    handleManualZoom,
    handleRepaint,
    exportChart,
    clearChart
  };
}

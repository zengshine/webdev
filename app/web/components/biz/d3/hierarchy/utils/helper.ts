import * as d3 from 'd3';

function curveDiagonal(s, d) {
  const path = `M ${s.y} ${s.x}
    C ${(s.y + d.y) / 2} ${s.x},
      ${(s.y + d.y) / 2} ${d.x},
      ${d.y} ${d.x}`;

  return path;
}

function alignDiagonal(s, d) {
  const pathNodes: [number, number][] = [
    [s.y, s.x],
    [s.y + (s.y > 0 ? -40 : 40), s.x],
    [s.y + (s.y > 0 ? -40 : 40), d.x],
    [d.y, d.x]
  ];

  const pathFn = d3
    .line()
    .curve(d3.curveStepAfter)
    .x((d) => d[0])
    .y((d) => d[1]);

  return pathFn(pathNodes);
}

export const diagonalType = {
  curve: curveDiagonal,
  align: alignDiagonal
};

export function getTooltipHandler() {
  let timerId;
  function handleMouseOver(evt, d) {
    // const d3 = window.d3;

    if (timerId) clearTimeout(timerId);

    d3.select('body')
      .append('div')
      .attr('class', 'co-graph-tooltip')
      .html(`${d.data.name}`)
      .style('left', evt.pageX + 50 + 'px')
      .style('top', evt.pageY + -50 + 'px')
      .style('display', 'flex')
      .on('mouseover', () => {
        clearTimeout(timerId);
      })
      .on('mouseout', () => {
        removeTooltipEle();
      });
  }

  function handleMouseOut() {
    removeTooltipEle();
  }

  function removeTooltipEle() {
    // const d3 = window.d3;
    timerId = setTimeout(() => {
      d3.selectAll('.co-graph-tooltip').remove();
    }, 200);
  }

  return {
    handleMouseOver,
    handleMouseOut
  };
}

export function getInitTreeData(root) {
  if (!root) return;

  const { children } = root;

  const leftChildren = children.filter((item) => item.left);
  const rightChildren = children.filter((item) => !item.left);

  const leftTree = Object.assign({}, root, {
    children: leftChildren,
    class: 'left-tree',
    left: true
  });
  const rightTree = Object.assign({}, root, {
    children: rightChildren,
    class: 'right-tree'
  });

  return [rightTree, leftTree];
}

export function getSvgIcon(id) {
  return `<svg class="symbol-icon" aria-hidden="true">
            <use xlink:href="#${id}" />
          </svg>`;
}

// A recursive helper function for performing some setup by walking through all nodes
export function visit(
  parent,
  visitFn,
  childrenFn: (d, data?) => any = (d) => {
    return d.children;
  },
  data = {}
) {
  if (!parent) return;

  visitFn(parent, data);
  const children = childrenFn(parent, data);
  if (children) {
    const count = children.length;
    for (let i = 0; i < count; i++) {
      visit(children[i], visitFn, childrenFn, data);
    }
  }
}

export function initHierarchy(data) {
  // const { d3 } = window;
  return d3.hierarchy(data);
}

export function flatTree(root) {
  const dataList: any[] = [];

  const callback = (d) => {
    dataList.push(d);
  };

  visit(root, callback);
  return dataList;
}

export function calChartDimension(rootList) {
  let minX, maxX, minY, maxY;
  minX = maxX = minY = maxY;
  const xList: number[] = [];
  const yList: number[] = [];
  const callback = (d) => {
    const { x, y } = d;
    xList.push(x);
    yList.push(y);
  };
  rootList.forEach((root) => visit(root, callback));
  minX = Math.min(...xList);
  maxX = Math.max(...xList);
  minY = Math.min(...yList);
  maxY = Math.max(...yList);

  const vw = maxX - minX;
  const vh = maxY - minY;

  return {
    minX,
    maxX,
    minY,
    maxY,
    vh,
    vw
  };
}

export function exportSvgToImage(svg) {
  // const svg = `<svg version="1.1"  width="300" height="200"
  // xmlns="http://www.w3.org/2000/svg">
  //    <rect width="100%" height="100%" fill="red" />
  //    <circle cx="150" cy="100" r="80" fill="green" />
  //    <text x="150" y="125" font-size="60" text-anchor="middle" fill="white">SVG</text></svg>`
  svgToPng(svg);

  function svgToPng(svg, callback?) {
    const url = getSvgUrl(svg);
    svgUrlToPng(url, (imgData) => {
      callback(imgData);
      URL.revokeObjectURL(url);
    });
  }

  function getSvgUrl(svg) {
    return URL.createObjectURL(
      new Blob([svg], { type: 'image/svg+xml;charset=utf-8' })
    );
  }

  function svgUrlToPng(svgUrl, callback) {
    const svgImage = document.createElement('img');
    document.body.appendChild(svgImage);
    svgImage.onload = function () {
      const canvas = document.createElement('canvas');
      canvas.width = svgImage.clientWidth;
      canvas.height = svgImage.clientHeight;
      const canvasCtx = canvas.getContext('2d');
      canvasCtx!.drawImage(svgImage, 0, 0);
      const imgData = canvas.toDataURL('image/png');
      document.body.removeChild(svgImage);
      callback(imgData);
    };
    svgImage.onerror = function () {
      console.log('onerror=========================>');
    };

    svgImage.src = svgUrl;
  }
}

export function getNodeTemplate(d) {
  const { editable, depth } = d;
  const { searchable, name } = d.data;

  const isCategory = depth === 1;

  const getNodeClass = () => {
    const rootClass = d.parent ? '' : 'node-template__box--root';
    const categoryNodeClass = isCategory ? 'node-template__category' : '';
    const searchableClass = searchable ? 'node-template__search' : '';
    const className = `p-rel h-p-100 flex-center node-template__box ${rootClass} ${categoryNodeClass} ${searchableClass}`;
    return className;
  };

  const getNodeContent = () => {
    if (searchable) {
      return `搜索<span class="f-14 m-h-4">${getSvgIcon(
        'icon-kaiyanjing'
      )}</span>`;
    }

    return `${name}`;
  };

  const getEditIcon = () => {
    const editIcon = editable ? getSvgIcon('icon-shouyeshurukuangshanchu') : '';
    const iconWrapper = `<span data-id=${d.id} data-parentId=${d.parentId} class="node-remove__icon" onclick="coGraphHandleRemoveNode(event, this)">${editIcon}</span>`;
    return iconWrapper;
  };

  const getTemplate = () => {
    return `<div class="${getNodeClass()}">
              ${getNodeContent()}
              ${getEditIcon()}
            </div>`;
  };

  return getTemplate();
}

// ( 01 法定代表人 02 疑似最终受益人 03 股东 04 高管 05 对外投资 06 历史对外投资 07 控股企业 08 分支机构)
export const NODE_TYPE = {
  OPER_NAME: '01', // 法定代表人
  BENEFICIARY: '02', // 疑似最终受益人
  SHAREHOLDER: '03', // 股东
  SENIOR_EXECUTIVE: '04', // 高管
  OUTBOUND_INVESTMENT: '05', // 对外投资
  HIS_OUTBOUND_INVESTMENT: '06', // 历史对外投资
  HOLDING_ENTERPRISE: '07', // 控股企业
  BRANCH: '08', // 分支机构
  ACT_AS_LEGAL_PERSON: '09', // 担任法人
  PERSON_OUTBOUND_INVESTMENT: '10', // 人员对外投资
  WORKING_ABROAD: '11', // 在外任职

  SEARCH: '00' // 搜索节点
};

export const NODE_TYPE_ORDER = {
  [NODE_TYPE.OPER_NAME]: '999',
  [NODE_TYPE.BENEFICIARY]: '989',
  [NODE_TYPE.SHAREHOLDER]: '979',
  [NODE_TYPE.SENIOR_EXECUTIVE]: '969',
  [NODE_TYPE.OUTBOUND_INVESTMENT]: '959',
  [NODE_TYPE.ACT_AS_LEGAL_PERSON]: '959',
  [NODE_TYPE.PERSON_OUTBOUND_INVESTMENT]: '959',
  [NODE_TYPE.WORKING_ABROAD]: '959',
  [NODE_TYPE.HIS_OUTBOUND_INVESTMENT]: '939',
  [NODE_TYPE.HOLDING_ENTERPRISE]: '949',
  [NODE_TYPE.BRANCH]: '929',
  [NODE_TYPE.SEARCH]: '100'
};

export const getNodeColor = (d) => {
  const code = d.data.code;

  const NODE_CONFIG = {
    [NODE_TYPE.OPER_NAME]: {
      color: '#FFA408'
    },
    [NODE_TYPE.BENEFICIARY]: {
      color: '#FF7D7D'
    },
    [NODE_TYPE.SHAREHOLDER]: {
      color: '#49B371'
    },
    [NODE_TYPE.SENIOR_EXECUTIVE]: {
      color: '#37A8A4'
    },
    [NODE_TYPE.OUTBOUND_INVESTMENT]: {
      color: '#3981F4'
    },
    [NODE_TYPE.HIS_OUTBOUND_INVESTMENT]: {
      color: '#73C4F9'
    },
    [NODE_TYPE.HOLDING_ENTERPRISE]: {
      color: '#FF8AE0'
    },
    [NODE_TYPE.BRANCH]: {
      color: '#FFA408'
    },
    [NODE_TYPE.SEARCH]: {
      color: '#3981F4'
    },
    [NODE_TYPE.ACT_AS_LEGAL_PERSON]: {
      color: '#F0CD42'
    },
    [NODE_TYPE.PERSON_OUTBOUND_INVESTMENT]: {
      color: '#3981F4'
    },
    [NODE_TYPE.WORKING_ABROAD]: {
      color: '#757DF1'
    }
  };

  return (NODE_CONFIG[code] || NODE_CONFIG[NODE_TYPE.SEARCH]).color;
};

export const isDirectionIconVisible = (d) => {
  return d.depth % 2 === 1;
};

export const getCommonProperty = (d) => {
  const { left } = d;

  return {
    left
  };
};

export const hasNext = (d) => {
  const { data, depth } = d;
  const { hasNext, _id, pid } = data;
  const id = _id || pid;
  const isRoot = !depth;
  const hasChild = hasNext === 1;
  return (!isRoot && hasChild && id) || isDirectionIconVisible(d);
};

export const hasChildren = (d) => {
  const { children, _children } = d;
  const hasChild =
    (children && children.length) || (_children && _children.length);
  return hasNext(d) && hasChild;
};

export const getChildList = (d) => {
  return d.children || d._children || [];
};

export const getChildLength = (d) => {
  const { searchNodes } = d.data;
  const searchNodeLength = (searchNodes || []).length;
  const childList = getChildList(d);
  const childNodeLength = childList.filter(
    (item) => !item.data.expandable && !item.data.searchable
  ).length;
  return searchNodeLength + childNodeLength;
};

export const isInnerRelation = (d) => {
  const { SHAREHOLDER, OPER_NAME, BENEFICIARY, SENIOR_EXECUTIVE } = NODE_TYPE;
  const innerRelateType = [
    SHAREHOLDER,
    OPER_NAME,
    BENEFICIARY,
    SENIOR_EXECUTIVE
  ];

  const { code } = d.data;
  return innerRelateType.includes(code);
};

export const isCompany = (d) => {
  return d.data.type === 2;
};

export const isPerson = (d) => {
  return d.data.type === 1;
};

export const setDirection = (nodeList) => {
  for (const node of nodeList) {
    const { code } = node;
    const { HOLDING_ENTERPRISE, BRANCH } = NODE_TYPE;
    const leftNodeType = [HOLDING_ENTERPRISE, BRANCH];
    node.left = leftNodeType.includes(code);
  }
};

export const getNodeDirection = (node) => {
  const { code } = node;
  const { HOLDING_ENTERPRISE, BRANCH } = NODE_TYPE;
  const leftNodeType = [HOLDING_ENTERPRISE, BRANCH];
  return leftNodeType.includes(code) ? 'left' : 'right';
};

export function calLevelWidth(level, n, levelWidth = [1]) {
  if (n.children && n.children.length > 0) {
    if (levelWidth.length <= level + 1) levelWidth.push(0);

    levelWidth[level + 1] += n.children.length;
    n.children.forEach(function (d) {
      calLevelWidth(level + 1, d, levelWidth);
    });
  }
}

export const getSearchNodeLength = (d) => {
  const { searchable, expandable } = d.data;

  if (searchable || expandable) {
    return d.parent.data.searchNodes.length;
  }

  return null;
};

export const getNodeFieldText = (d) => {
  const { job, stockPercent, code } = d.data;

  switch (code) {
    case NODE_TYPE.SHAREHOLDER:
      return stockPercent;
    case NODE_TYPE.BENEFICIARY:
      return stockPercent;
    case NODE_TYPE.SENIOR_EXECUTIVE:
      return job;
    case NODE_TYPE.OUTBOUND_INVESTMENT:
      return stockPercent;
    case NODE_TYPE.HIS_OUTBOUND_INVESTMENT:
      return stockPercent;
    case NODE_TYPE.HOLDING_ENTERPRISE:
      return stockPercent;
    default:
      return '';
  }
};

// TODO: 跟真实渲染保持一致
export const nodeTextRender = (d) => {
  const { name } = d.data;

  const searchNodeLength = getSearchNodeLength(d);
  if (searchNodeLength) return `${name}[${searchNodeLength}]`;

  const isTypeNode = isDirectionIconVisible(d);
  if (isTypeNode) return `${name}[${getChildLength(d)}]`;

  let fieldText = '';
  const jobPercentFieldText = getNodeFieldText(d);
  const majorHolderFieldText = isMajorStockHolder(d) ? '大股东' : '';
  const fieldList = [jobPercentFieldText, majorHolderFieldText];
  fieldText = fieldList.reduce((a, b) => (b ? a + `[${b}]` : a), '');

  return `${name}${fieldText}`;
};

export function isMajorStockHolder(d) {
  const { code, stockPercentRation } = d.data;
  return code === NODE_TYPE.SHAREHOLDER && stockPercentRation > 50;
}

export const checkIsEmptyStockPercent = (d) => {
  if (d?.data) {
    // 股东类型
    if (NODE_TYPE.SHAREHOLDER === d.data.code) {
      if (d.data.name === '股东') {
        // 第一级的股东默认显示
        return true;
      }
      // 子节点根据 持股比例字段判断
      return !!d.data.stockPercentRation;
    }
    // 非股东类型
    return true;
  }
  return true;
};
export const appendNodeFieldText = ({
  node,
  append,
  filterFn,
  valueFn,
  color = '#3981F4',
  char = ['[', ']']
}: {
  node: any;
  append?: boolean;
  filterFn: any;
  valueFn: any;
  color?: string;
  char?: string[];
}) => {
  const targetNode = node.filter(filterFn);

  if (!append) targetNode.selectAll('tspan.tspan-extra-field').remove();

  // 展开节点不显示数值
  const fieldNode = targetNode.filter((d) => !d.data.expanded); // TODO：展开收起逻辑删除旧节点，添加新节点，简化节点更新逻辑

  fieldNode
    .append('tspan')
    .attr('class', 'tspan-extra-field')
    .text((d) => {
      if (checkIsEmptyStockPercent(d)) {
        return char[0];
      }
      return '';
    })
    .attr('dx', 2);

  fieldNode
    .append('tspan')
    .attr('class', 'tspan-extra-field')
    .text((d) => {
      if (checkIsEmptyStockPercent(d)) {
        return valueFn(d);
      }
      return '';
    })
    .attr('fill', color);
  fieldNode
    .append('tspan')
    .attr('class', 'tspan-extra-field')
    .text((d) => {
      if (checkIsEmptyStockPercent(d)) {
        return char[1];
      }
      return '';
    })
    .attr('fill', '');
};

export const isChildVisible = (d) => {
  const { children = [] } = d;
  return children && children.length;
};

export const getNodeTypeClass = (d) => {
  const { code } = d.data;

  const NODE_TYPE_CLASS = { [NODE_TYPE.SEARCH]: 'g-node-type-search' };

  return NODE_TYPE_CLASS[code] || '';
};

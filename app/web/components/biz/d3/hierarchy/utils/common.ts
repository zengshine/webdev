import { ZOOM_TYPE } from '@web/components/biz/d3/hierarchy/const/common';

const html2canvas: any = {};

export function getZoomRange({ min = 0.1, max = 3, initialZoom = 1 }) {
  let zoom = initialZoom;

  const getZoomLevel = (type) => {
    let step = 0;
    const integerStep = 0.4;
    const decimalStep = 0.15;

    const stepType = type === ZOOM_TYPE.up ? 1 : -1;

    if (zoom > 1) {
      step = integerStep;
    } else if (zoom < 1) {
      step = decimalStep;
    } else {
      step = type === ZOOM_TYPE.up ? 0.4 : 0.15;
    }

    zoom += step * stepType;

    if (zoom < min) zoom = min;
    if (zoom > max) zoom = max;

    return zoom;
  };

  const setZoomLevel = (val) => {
    if (val < min || val > max) return;
    zoom = val;
  };

  const zoomRange: [number, number] = [min, max];

  return {
    zoomRange,
    getZoomLevel,
    setZoomLevel
  };
}

export const downloadUrl = ({ url, name }) => {
  const downloadLink = document.createElement('a');
  downloadLink.href = url;
  downloadLink.download = `${name}.png`;
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
};

export const exportHtmlAsImage = ({
  el,
  name,
  encoderOptions = 1,
  config = {}
}) => {
  return html2canvas(el, { ...config }).then((canvas) => {
    downloadUrl({
      url: canvas.toDataURL('image/png', encoderOptions),
      name
    });
  });
};

export const exportHtmlAsCanvas = ({ el, config = {} }) => {
  return html2canvas(el, { ...config }).then((canvas) => canvas);
};

/**
 * By Ken Fyrstenberg Nilsen
 *
 * drawImageProp(context, image [, x, y, width, height [,offsetX, offsetY]])
 *
 * If image and context are only arguments rectangle will equal canvas
 */
export function drawImageProp(ctx, img, x, y, w, h, offsetX?, offsetY?) {
  if (arguments.length === 2) {
    x = y = 0;
    w = ctx.canvas.width;
    h = ctx.canvas.height;
  }

  // default offset is center
  offsetX = typeof offsetX === 'number' ? offsetX : 0.5;
  offsetY = typeof offsetY === 'number' ? offsetY : 0.5;

  // keep bounds [0.0, 1.0]
  if (offsetX < 0) offsetX = 0;
  if (offsetY < 0) offsetY = 0;
  if (offsetX > 1) offsetX = 1;
  if (offsetY > 1) offsetY = 1;

  const iw = img.width;
  const ih = img.height;
  const r = Math.min(w / iw, h / ih);
  let nw = iw * r; // new prop. width
  let nh = ih * r; // new prop. height
  let cx;
  let cy;
  let cw;
  let ch;
  let ar = 1;

  // decide which gap to fill
  if (nw < w) ar = w / nw;
  if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh; // updated
  nw *= ar;
  nh *= ar;

  // calc source rectangle
  cw = iw / (nw / w);
  ch = ih / (nh / h);

  cx = (iw - cw) * offsetX;
  cy = (ih - ch) * offsetY;

  // make sure source rectangle is valid
  if (cx < 0) cx = 0;
  if (cy < 0) cy = 0;
  if (cw > iw) cw = iw;
  if (ch > ih) ch = ih;

  // fill image in dest. rectangle
  ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h);
}

export function loadImage(url) {
  return new Promise((resolve) => {
    const image = new Image();
    image.onload = () => {
      resolve(image);
    };
    // 设置图片跨域访问
    image.setAttribute('crossOrigin', 'anonymous');
    image.src = url;
  });
}

export function svgToImage({ svgElement, name }) {
  return new Promise((resolve) => {
    const clonedSvgElement = svgElement.cloneNode(true);
    const outerHTML = clonedSvgElement.outerHTML;
    const blob = new Blob([outerHTML], { type: 'image/svg+xml' });
    const URL = window.URL || window.webkitURL || window;
    const blobURL = URL.createObjectURL(blob);
    const image = new Image();
    image.onload = async () => {
      const canvas = document.createElement('canvas');
      canvas.setAttribute('class', 'qzd-watermark-bg');

      const { width: w, height: h } = image;
      canvas.width = w;
      canvas.height = h;
      const context = canvas.getContext('2d');
      context!.fillStyle = '#ebeff7';
      context!.fillRect(0, 0, w, h);
      // const bgImageUrl = wrapRequired('@web/assets/images/d3/watermark.png');
      // const tipImageUrl = wrapRequired('@web/assets/images/d3/logo.png');
      // const bgImageUrl = '';
      // const tipImageUrl = '';
      // const bgImage = await loadImage(bgImageUrl);
      // const tipImage = await loadImage(tipImageUrl);
      // drawImageProp(context, bgImage, 0, 0, w, h);
      // if (tipImage) {
      //   const preText = '以上数据是';
      //   const suffixText =
      //     '大数据分析引擎基于公开信息挖掘的成果，仅供参考。该成果不构成任何明示或暗示的观点或保证。';
      //   const imW = 53;
      //   const imH = 14;
      //   const imPh = 2;
      //   const preTextWidth = context!.measureText(preText).width + imPh;
      //   const suffixTextWidth = context!.measureText(suffixText).width;
      //   const tipWrapperWidth = preTextWidth + imW + suffixTextWidth + imPh * 2;
      //   const offsetX = (w - tipWrapperWidth) / 2;
      //   const offsetY = h - 25;
      //   const textOffsetY = offsetY + imH / 2;
      //   context!.font = '12px';
      //   context!.fillStyle = '#7e7f81';
      //   context!.textBaseline = 'middle';
      //   context!.fillText(preText, offsetX, textOffsetY);
      //   context!.drawImage(
      //     tipImage as unknown as CanvasImageSource,
      //     offsetX + preTextWidth,
      //     offsetY,
      //     imW,
      //     imH
      //   );
      //   context!.fillText(
      //     suffixText,
      //     offsetX + preTextWidth + imW + imPh,
      //     textOffsetY
      //   );
      // }
      context!.drawImage(image, 0, 0, w, h);
      const url = canvas.toDataURL();
      downloadUrl({ name, url });
      resolve(true);
    };
    image.onerror = function (...args) {
      console.log('image error=========================>', args);
    };
    image.setAttribute('crossorigin', 'anonymous');
    image.src = blobURL;
  });
}

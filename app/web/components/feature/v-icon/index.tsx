import { defineComponent } from 'vue';

import './style.scss';

export default defineComponent({
  name: 'VIcon',

  props: {
    icon: {
      type: String,
      default: ''
    },
    prefix: {
      type: String,
      default: ''
    }
  },

  setup(props, { attrs }) {
    const { icon, prefix } = props;
    return () => (
      <svg class="symbol-icon__svg" aria-hidden="true" {...attrs}>
        <use xlinkHref={`#${prefix}${icon}`} />
      </svg>
    );
  }
});

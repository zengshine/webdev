import { defineComponent } from 'vue';

import './style.scss';

export default defineComponent({
  name: 'SideFloatBar',

  setup() {
    const isHovering = ref(false);
    const { x, y } = useMouse();
    const isInRange = computed(() => {
      const range = {
        left: 0,
        right: 15,
        top: 50,
        bottom: 50
      };
      const { left, right, top, bottom } = range;
      return (
        x.value >= left &&
        x.value <= right &&
        y.value >= top &&
        y.value >= bottom
      );
    });
    const visible = computed(() => {
      return isInRange.value || isHovering.value;
    });

    function handleMouseEnter() {
      isHovering.value = true;
    }

    function handleMouseLeave() {
      isHovering.value = false;
    }

    return () => {
      const hiddenClass = visible.value ? '' : 'float-bar--hidden';
      return (
        <div
          onMouseenter={handleMouseEnter}
          onMouseleave={handleMouseLeave}
          class={`float-bar__wrapper ${hiddenClass}`}
        ></div>
      );
    };
  }
});

import {
  defineComponent,
  h,
  ref,
  toRef,
  computed,
  onMounted,
  type VNode
} from 'vue';
import { array, func, string } from 'vue-types';

import useRegisterEvent from '@web/composables/useRegisterEvent';

import { transformObjectProperties } from '@root/shared/utils';
import { transformDomAttribute } from '@web/utils/dom';
import { createScrollCalculator, SCROLL_AXIS } from '@web/utils/scroll';
import { getWindowBoundary, createDataGetter } from './util';

export default defineComponent({
  name: 'VirtualizedRender',
  props: {
    scrollAxis: string().def(SCROLL_AXIS.y),
    dataList: array<any>().def([]),
    getItemPosition: func().def(() => ({}))
  },

  setup(props, { attrs, slots }) {
    const { getItemPosition } = props;

    const dataList = toRef(props, 'dataList');
    const content = ref<VNode>();
    const scrollWrapperRef = ref<HTMLElement>();
    const wrapperStyle = computed(() => {
      const { scrollAxis } = props;
      const { x, y } = SCROLL_AXIS;
      const length = dataList.value.length;
      const styleCalculator = {
        [x]: () => {
          const lastItem = dataList.value[length - 1];
          const position = getItemPosition!(lastItem);
          const width = position.left + position.width;
          return { width };
        },
        [y]: () => {
          const lastItem = dataList.value[length - 1];
          const position = getItemPosition!(lastItem);
          const height = position.top + position.height;
          return { height };
        }
      };

      return transformObjectProperties(
        styleCalculator[scrollAxis](),
        transformDomAttribute
      );
    });

    const { dataGetter } = createDataGetter({ getItemPosition });
    const { getScrollDirection, getScrollOffset } = createScrollCalculator();
    const { addEventListener } = useRegisterEvent();

    onMounted(() => {
      const scrollEle = scrollWrapperRef.value!;
      renderContent(scrollEle);
      addEventListener({
        element: scrollEle,
        event: 'scroll',
        handler() {
          const scrollInterval = 250;
          const scrollOffset = Math.abs(getScrollOffset(scrollEle));
          if (scrollOffset < scrollInterval) return;

          renderContent(scrollEle);
        }
      });
    });

    function renderContent(scrollEle) {
      const direction = getScrollDirection(scrollEle);
      const windowBoundary = getWindowBoundary(scrollEle);
      const result = dataGetter(dataList.value, windowBoundary, direction);
      content.value = getRenderContent(result);
    }

    function getRenderContent(dataList) {
      return dataList.map((item) => {
        const { id } = item;
        const { top, left, width, height } = getItemPosition!(item);
        const dimensionStyle = transformObjectProperties(
          { top, left, width, height },
          transformDomAttribute
        );

        return h(
          'div',
          {
            key: id,
            style: {
              position: 'absolute',
              ...dimensionStyle
            }
          },
          slots.default!({ data: item })
        );
      });
    }

    return () =>
      h(
        'div',
        {
          ...attrs,
          ref: scrollWrapperRef
        },
        [
          h(
            'div',
            {
              style: {
                ...wrapperStyle.value,
                position: 'relative'
              }
            },
            content.value!
          )
        ]
      );
  }
});

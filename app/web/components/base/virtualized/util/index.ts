import { SCROLL_DIRECTION, SCROLL_AXIS } from '@web/utils/scroll';

export function getWindowBoundary(ele: HTMLElement, rate = 1) {
  const { clientWidth, clientHeight, scrollTop, scrollLeft } = ele;
  const xOffset = clientWidth * rate;
  const yOffset = clientHeight * rate;
  const left = scrollLeft - xOffset;
  const right = scrollLeft + clientWidth + xOffset;
  const top = scrollTop - yOffset;
  const bottom = scrollTop + clientHeight + yOffset;
  return {
    top,
    right,
    bottom,
    left
  };
}

export function createDataGetter({ getItemPosition }) {
  let startIndex = 0,
    endIndex = 0;

  const { forward } = SCROLL_DIRECTION;
  const { x, y } = SCROLL_AXIS;
  const boundaryCalculator = {
    [x]: {
      start(boundary, position) {
        return position.left > boundary.left;
      },
      end(boundary, position) {
        return position.left > boundary.right;
      }
    },
    [y]: {
      start(boundary, position) {
        return position.top > boundary.top;
      },
      end(boundary, position) {
        return position.top > boundary.bottom;
      }
    }
  };

  const dataGetter = (dataList, boundary, direction, axis = y) => {
    const result = <any>[];
    let findStart = false,
      findEnd = false;
    const length = dataList.length;
    const calculator = boundaryCalculator[axis];

    if (direction === forward) {
      let i = startIndex;
      for (; i < length; i++) {
        const data = dataList[i];
        const position = getItemPosition!(data);
        if (!findStart && calculator.start(boundary, position)) {
          startIndex = i;
          findStart = true;
        }
        if (findStart && calculator.end(boundary, position)) {
          findEnd = true;
          endIndex = i;
          break;
        }
      }
      if (i >= length) endIndex = length - 1;
    } else {
      let i = endIndex;
      for (; i >= 0; i--) {
        const data = dataList[i];
        const position = getItemPosition!(data);
        if (!findEnd && !calculator.end(boundary, position)) {
          endIndex = i;
          findEnd = true;
        }
        if (findEnd && !calculator.start(boundary, position)) {
          findStart = true;
          startIndex = i;
          break;
        }
      }
      if (i < 0) startIndex = 0;
    }
    for (let i = startIndex; i <= endIndex; i++) {
      result.push(dataList[i]);
    }
    return result;
  };

  return {
    dataGetter
  };
}

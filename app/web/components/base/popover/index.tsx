import { defineComponent, ref, onMounted, nextTick, Teleport } from 'vue';
import { computePosition, flip, shift, offset, arrow } from '@floating-ui/dom';

import useRegisterEvent from '@web/composables/useRegisterEvent';

import './style.scss';

enum Trigger {
  hover = 'hover',
  click = 'click'
}

enum ArrowDirection {
  up = 'up',
  right = 'right',
  down = 'down',
  left = 'left'
}

export default defineComponent({
  name: 'BasePopover',

  inheritAttrs: false,

  props: {
    className: {
      type: String,
      default: ''
    },
    trigger: {
      type: String,
      default: Trigger.hover
    },
    hideDelay: {
      type: Number,
      default: 300
    }
  },

  setup(props, context) {
    const visible = ref(false);
    const referenceRef = ref<HTMLElement>();
    const popoverRef = ref<HTMLElement>();
    const arrowRef = ref<HTMLElement>();
    let hideTimeoutId: TimerId;
    const { slots, attrs } = context;

    const arrowDirection = ref(ArrowDirection.up);

    const { registerEventListener } = useRegisterEvent();

    onMounted(() => {
      setupTrigger();
    });

    function setupTrigger() {
      props.trigger === Trigger.hover
        ? setupHoverTrigger()
        : setupClickTrigger();
    }

    function setupHoverTrigger() {
      const mouseenterHandler = () => {
        display();
      };
      const mouseleaveHandler = () => {
        hide();
      };
      registerEventListener({
        element: referenceRef.value!,
        event: 'mouseenter',
        handler: mouseenterHandler
      });
      registerEventListener({
        element: referenceRef.value!,
        event: 'mouseleave',
        handler: mouseleaveHandler
      });
    }

    function setupClickTrigger() {
      const outsideClickHandler = (evt: MouseEvent) => {
        const isOutSide = !popoverRef.value?.contains(evt.target! as Node);

        if (!isOutSide) return;

        hide();
        document.removeEventListener('click', outsideClickHandler);
      };
      const clickHandler = () => {
        if (visible.value) return;
        setTimeout(() => {
          document.addEventListener('click', outsideClickHandler);
        });
        display();
      };
      registerEventListener({
        element: referenceRef.value!,
        event: 'click',
        handler: clickHandler
      });
    }

    function display() {
      if (hideTimeoutId) clearTimeout(hideTimeoutId);

      visible.value = true;
      nextTick(() => {
        setupPopover();
      });
    }

    function hide() {
      hideTimeoutId = setTimeout(() => {
        visible.value = false;
      }, props.hideDelay);
    }

    function setupPopover() {
      computePosition(referenceRef.value!, popoverRef.value!, {
        placement: 'top',
        middleware: [
          offset(5),
          flip(),
          shift({ padding: 5 }),
          arrow({ element: arrowRef.value! })
        ]
      }).then(({ x, y, placement, middlewareData }) => {
        Object.assign(popoverRef.value!.style, {
          left: `${x}px`,
          top: `${y}px`
        });

        const { x: arrowX, y: arrowY = 0 } = middlewareData.arrow as any; // TODO: type error: y not exist on middlewareData.arrow
        const transformedPlacement = placement.split('-')[0];
        const { down, left, up, right } = ArrowDirection;
        const arrowPlacement = {
          top: 'bottom',
          right: 'left',
          bottom: 'top',
          left: 'right'
        }[transformedPlacement]!;
        arrowDirection.value = {
          top: down,
          right: left,
          bottom: up,
          left: right
        }[transformedPlacement]!;
        Object.assign(arrowRef.value!.style, {
          left: `${arrowX}px`,
          top: `${arrowY}px`,
          [arrowPlacement]: '-4px'
        });
      });
    }

    return () => {
      const { className } = props;
      const getPopover = () => {
        if (!visible.value) return;

        const handleMouseEnter = () => {
          if (props.trigger === Trigger.hover) display();
        };

        const handleMouseLeave = () => {
          if (props.trigger === Trigger.hover) hide();
        };

        const arrowClass = `arrow-${arrowDirection.value}`;
        return (
          <div
            class={`${className} popover-dark absolute`}
            ref={popoverRef}
            onMouseenter={handleMouseEnter}
            onMouseleave={handleMouseLeave}
          >
            {visible.value ? slots.default?.() : ''}
            <div class={`absolute ${arrowClass}`} ref={arrowRef}></div>
          </div>
        );
      };

      return (
        <>
          <span {...attrs} ref={referenceRef}>
            {slots.reference?.()}
          </span>
          <Teleport to="body">
            <span>{getPopover()}</span>
          </Teleport>
        </>
      );
    };
  }
});

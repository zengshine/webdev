import type { FunctionalComponent, VNode } from 'vue';
import {
  defineComponent,
  defineAsyncComponent,
  h,
  ref,
  toRef,
  onMounted
} from 'vue';

import { isBrowser } from '@root/shared/utils';

const triggerType = {
  never: 'never',
  visible: 'visible'
};

const getAsyncHydrate = (VNode: VNode | Array<VNode>) => {
  let hydrate;

  const hydrateComponent = () => VNode;

  const hydratePromise: Promise<FunctionalComponent> = new Promise(
    (resolve) => {
      hydrate = () => {
        resolve(hydrateComponent);
      };
    }
  );

  const asyncHydrateComponent = defineAsyncComponent({
    loader: () => hydratePromise
  });

  return { hydrate, asyncHydrateComponent };
};

export default defineComponent({
  name: 'LazyHydrate',

  props: {
    trigger: {
      type: String,
      default: triggerType.visible
    }
  },

  setup(props, { slots }) {
    const trigger = toRef(props, 'trigger');
    const wrapperRef = ref<Element>();
    const renderVNode = slots.default?.();
    const cleanUps: any[] = [];
    const { hydrate, asyncHydrateComponent } = getAsyncHydrate(renderVNode!);

    onMounted(() => {
      setupTrigger();
    });

    function handleHydrate() {
      hydrate();
      handleCleanUp();
    }

    function handleCleanUp() {
      for (const cleanUp of cleanUps) {
        cleanUp();
      }
    }

    function setupTrigger() {
      const { visible } = triggerType;
      if (trigger.value === visible) {
        setupVisibleTrigger();
      }
    }

    function setupVisibleTrigger() {
      const callback = (entries) => {
        for (const entry of entries) {
          const { intersectionRatio } = entry;
          if (!intersectionRatio) return;
          handleHydrate();
        }
      };
      const io = new IntersectionObserver(callback);
      io.observe(wrapperRef.value!);
      cleanUps.push(() => {
        io.disconnect();
      });
    }

    return () => {
      return h(
        'div',
        {
          ref: wrapperRef
        },
        [isBrowser() ? h(asyncHydrateComponent) : renderVNode]
      );
    };
  }
});

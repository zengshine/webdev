import { createApp, defineComponent, h, createTextVNode } from 'vue';
import type { VNode, Component, App } from 'vue';

import './style.scss';

type Options = string | VNode;

const getMountElement = (target?: HTMLElement) => {
  const parentEle = target ? target : document.body;
  const ele = document.createElement('div');
  parentEle.append(ele);
  return ele;
};

const getRenderVNode = (options: Options) => {
  if (typeof options === 'string') {
    return createTextVNode(options);
  }

  return options;
};

const getComponentManager = () => {
  let vueInstance: App;
  let component: Component;
  let mountEle: Element;

  const setComponent = (comp: Component) => {
    component = comp;
  };

  const mount = (comp?: Component) => {
    if (comp) setComponent(comp);
    if (!component) return;

    vueInstance = createApp(component);
    mountEle = getMountElement();
    mountEle.classList.add('message__wrapper');
    vueInstance.mount(mountEle);
  };

  const unmount = () => {
    vueInstance?.unmount();
    mountEle?.remove();
  };

  return {
    setComponent,
    mount,
    unmount
  };
};

function Message(options: Options) {
  const manager = getComponentManager();
  const renderComponent = defineComponent({
    setup() {
      const closeBtnText = 'close';
      const renderNode = getRenderVNode(options);
      return () => [
        renderNode,
        h(
          'div',
          {
            onClick: manager.unmount
          },
          [closeBtnText]
        )
      ];
    }
  });

  manager.mount(renderComponent);
}

export default Message;

export enum THEME_TYPE {
  dark = 'dark',
  light = 'light',
  default = ''
}

export const THEME_CLASS = {
  [THEME_TYPE.dark]: 'js-dark-theme',
  [THEME_TYPE.light]: 'js-light-theme'
};

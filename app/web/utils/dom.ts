/**
 * dom.ts
 * utils about dom
 */

import { getDataType } from '@root/shared/utils';

export const setAttribute = (el: Element, key: string, value: any) => {
  el.setAttribute(key, value);
};

/**
 * transform attribute value
 * @param val input value
 */
export function transformDomAttribute(val) {
  const type = getDataType(val);
  switch (type) {
    case 'Number':
      return `${val}px`;
    default:
      return val;
  }
}

/**
 * get scrollbar width
 */
function getScrollbarWidth() {
  // Creating invisible container
  const outer = document.createElement('div')!;
  outer.style.visibility = 'hidden';
  outer.style.overflow = 'scroll'; // forcing scrollbar to appear
  document.body.appendChild(outer);

  // Creating inner element and placing it in the container
  const inner = document.createElement('div');
  outer.appendChild(inner);

  // Calculating difference between container's full width and the child width
  const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

  // Removing temporary elements from the DOM
  outer.parentNode!.removeChild(outer);

  return scrollbarWidth;
}

/**
 * toggle scroll
 * @param el: target element
 * @param type: true: enable scroll, false: disable scroll
 */

export function toggleScroll(el, type) {
  const { scrollHeight, clientHeight } = el;
  const { documentElement, body } = document;
  const { innerHeight } = window;
  const isGlobalScroll =
    (el === documentElement || el === body) && scrollHeight > innerHeight;
  const isOverflow = isGlobalScroll || scrollHeight > clientHeight;

  if (!isOverflow) return;

  const scrollBarWidth = getScrollbarWidth();

  if (type) {
    const { overflowY = '', paddingRight = '' } = el.dataset;
    Object.assign(el.style, {
      overflowY,
      paddingRight
    });
  } else {
    const { overflowY, paddingRight } = el.style;
    if (overflowY || paddingRight)
      Object.assign(el.dataset, {
        overflowY,
        paddingRight
      });

    Object.assign(el.style, {
      overflowY: 'hidden',
      paddingRight: `${scrollBarWidth}px`
    });
  }
}

/**
 * browser.ts
 * utils about browser environment
 */

/**
 * whether system prefer dark
 * @return boolean
 */

export function isPreferDarkTheme(): boolean {
  const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: dark)');

  if (prefersDarkScheme.matches) {
    return true;
  }
  return false;
}

/**
 * @file scroll utils
 */

export const SCROLL_DIRECTION = {
  forward: 'forward',
  backward: 'backward'
};

export const SCROLL_AXIS = {
  x: 'x',
  y: 'y'
};

type axisType = (typeof SCROLL_AXIS)[keyof typeof SCROLL_AXIS];

export function createScrollCalculator() {
  let preScrollLeft = 0;
  let preScrollTop = 0;

  const getScrollOffset = (
    ele: HTMLElement,
    axis: axisType = SCROLL_AXIS.y
  ): number => {
    const { scrollLeft, scrollTop } = ele;
    const verticalOffset = scrollTop - preScrollTop;
    const horizontalOffset = scrollLeft - preScrollLeft;
    const offset = axis === SCROLL_AXIS.x ? horizontalOffset : verticalOffset;
    return offset;
  };

  const getScrollDirection = (
    ele: HTMLElement,
    axis: axisType = SCROLL_AXIS.y
  ) => {
    const offset = getScrollOffset(ele, axis);
    const { scrollLeft, scrollTop } = ele;
    preScrollLeft = scrollLeft;
    preScrollTop = scrollTop;
    const { forward, backward } = SCROLL_DIRECTION;
    return offset >= 0 ? forward : backward;
  };

  return {
    getScrollDirection,
    getScrollOffset
  };
}

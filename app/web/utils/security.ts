/**
 * security.ts
 * utils about security
 */

import { getCookie } from '@web/utils/cookie';

export function getCsrfToken(key = 'csrfToken') {
  return EASY_ENV_IS_NODE ? '' : getCookie(key);
}

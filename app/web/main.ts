import type { App as TypeApp } from 'vue';
import type { Router } from 'vue-router';
import type { Pinia } from 'pinia';
import { createPinia } from 'pinia';

import renderClient from '@web/entry-client';
import renderServer from '@web/entry-server';
import renderCommon from '@web/entry-common';

import App from '@web/app.vue';
import createRouter from '@web/router';

import '@web/styles/tailwind.css';
import '@web/styles/index.scss';

export interface RenderOptions {
  App: any;
  router: Router;
  store: Pinia;
  renderCommon: (app: TypeApp) => void;
}

const renderOptions: RenderOptions = {
  App,
  router: createRouter(),
  store: createPinia(),
  renderCommon
};

const renderMethods = EASY_ENV_IS_NODE ? renderServer : renderClient;

export default renderMethods(renderOptions);

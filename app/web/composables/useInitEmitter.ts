import { onBeforeUnmount, onMounted, getCurrentInstance } from 'vue';

import $emitter from '@web/plugins/emit';

import { transformToArray } from '@root/shared/utils/array';

type eventType = {
  event: string;
  handler: any;
};

const initEventEmitter = (evtName, handler) => {
  $emitter.$on(evtName, handler);
};

const removeEventEmitter = (evtName, handler) => {
  $emitter.$off(evtName, handler);
};

export default (params: eventType | eventType[]) => {
  const eventList = transformToArray(params);
  onMounted(() => {
    eventList.forEach((eventItem) => {
      const { event, handler } = eventItem;
      if (typeof handler === 'string') {
        const vm = getCurrentInstance();
        console.log('vm=========================>', vm);
        // const { ctx = {} } = vm;
        // handler = ctx[handler];
      }
      // 注册事件
      initEventEmitter(event, handler);
    });
  });
  // 移除事件
  onBeforeUnmount(() => {
    eventList.forEach((eventItem) => {
      const { event, handler } = eventItem;
      removeEventEmitter(event, handler);
    });
  });
};

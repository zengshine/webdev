import { watch, toRef } from 'vue';

import useUserStore from '@web/store/user';

import { isPreferDarkTheme } from '@web/utils/browser';

import { THEME_TYPE, THEME_CLASS } from '@web/const/common';

const setThemeClass = (theme) => {
  const themeClass = THEME_CLASS[theme];
  themeClass && document.documentElement.classList.add(themeClass);
};

const removeThemeClass = (theme) => {
  const themeClass = THEME_CLASS[theme];
  themeClass && document.documentElement.classList.remove(themeClass);
};

export default () => {
  const userStore = useUserStore();
  const themeRef = toRef(userStore.config, 'theme');

  watch(themeRef, (newVal, oldVal) => {
    removeThemeClass(oldVal);
    setThemeClass(newVal);
  });

  const getThemeSetting = () => {
    const theme = userStore.config.theme;
    return theme
      ? theme
      : isPreferDarkTheme()
      ? THEME_TYPE.dark
      : THEME_TYPE.light;
  };

  const setupThemeConfig = () => {
    userStore.setTheme(getThemeSetting());
  };

  return {
    setupThemeConfig
  };
};

import { ref, onMounted } from 'vue';

function getProp() {
  const propList = [
    {
      name: 'requestFullscreen',
      exitName: 'exitFullscreen',
      eventName: 'fullscreenchange',
      eleName: 'fullscreenElement'
    },
    {
      name: 'webkitRequestFullscreen',
      exitName: 'webkitExitFullscreen',
      eventName: 'webkitfullscreenchange',
      eleName: 'webkitFullscreenElement'
    },
    {
      name: 'mozRequestFullScreen',
      exitName: 'mozExitFullscreen',
      eventName: 'mozfullscreenchange',
      eleName: 'mozFullscreenElement'
    },
    {
      name: 'msRequestFullscreen',
      exitName: 'msExitFullscreen',
      eventName: 'msFullscreenChange',
      eleName: 'msFullscreenElement'
    }
  ];
  return propList.find((item) => document.documentElement[item.name]);
}

function compatibleExecutor(fn) {
  const prop = getProp();

  if (!prop) return;

  return fn(prop);
}

export default () => {
  const isFullScreenMode = ref(false);

  function setFullScreenState(val) {
    isFullScreenMode.value = val;
  }

  function exitFullScreen() {
    return compatibleExecutor((prop) => {
      return document[prop.exitName]();
    });
  }

  function enterFullScreen() {
    return compatibleExecutor((prop) => {
      return document.documentElement[prop.name]();
    });
  }

  function addEventListener() {
    compatibleExecutor((prop) => {
      document.addEventListener(prop.eventName, () => {
        const isFullScreen = !!document[prop.eleName];
        setFullScreenState(isFullScreen);
      });
    });
  }

  onMounted(() => {
    addEventListener();
  });

  return {
    isFullScreenMode,
    exitFullScreen,
    enterFullScreen,
    setFullScreenState
  };
};

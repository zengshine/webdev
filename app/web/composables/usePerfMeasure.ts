import { onMounted, onBeforeUpdate, onUpdated } from 'vue';

type ParamsType = {
  name: string;
};

const LIFE_CYCLE = {
  beforeCreate: 'beforeCreate',
  mounted: 'mounted',
  beforeUpdate: 'beforeUpdate',
  updated: 'updated'
};

export default function (params: ParamsType) {
  const { name } = params;

  performance.mark(LIFE_CYCLE.beforeCreate);

  onMounted(() => {
    performance.mark(LIFE_CYCLE.mounted);

    performance.measure(
      `${name} mounted`,
      LIFE_CYCLE.beforeCreate,
      LIFE_CYCLE.mounted
    );
  });

  onBeforeUpdate(() => {
    performance.mark(LIFE_CYCLE.beforeUpdate);
  });

  onUpdated(() => {
    performance.mark(LIFE_CYCLE.updated);

    performance.measure(
      `${name} updated`,
      LIFE_CYCLE.beforeUpdate,
      LIFE_CYCLE.updated
    );
  });
}

import { throttle } from 'lodash';

export default function () {
  const $router = useRouter();

  const interval = 100;

  let closeTimer = 0;

  onMounted(() => {
    window.addEventListener('popstate', handlePopstate);
  });

  onBeforeUnmount(() => {
    setTimeout(() => {
      window.removeEventListener('popstate', handlePopstate);
    }, interval + 50);
  });

  const handleBack = throttle(() => {
    setCloseTimer();
    $router.back();
  }, interval);

  function setCloseTimer() {
    closeTimer = setTimeout(() => {
      handleClose();
    }, interval);
  }

  function handleClose() {
    window.close();
  }

  function handlePopstate() {
    if (closeTimer) {
      clearTimeout(closeTimer);
      closeTimer = 0;
    }
  }

  return {
    handleBack
  };
}

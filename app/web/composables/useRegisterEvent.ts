import { onMounted, onBeforeUnmount, nextTick } from 'vue';

type Params = {
  element: HTMLElement | Window;
  event: string;
  handler: EventListener;
  options?: EventListenerOptions;
};

export default function () {
  const eventList: Array<Params> = [];

  function addEventListener(params: Params) {
    const { element, event, handler, options } = params;
    element.addEventListener(event, handler, options);
    addToEventList(params);
  }

  function removeEventListener(params: Params) {
    const { element, event, handler } = params;
    element.removeEventListener(event, handler);
  }

  function registerEventListener(params: Params) {
    eventList.push(params);
  }

  function addToEventList(params: Params) {
    const evt = eventList.find((i) => i === params);
    if (evt) return;

    eventList.push(params);
  }

  function initEvents() {
    eventList.forEach((params) => {
      addEventListener(params);
    });
  }

  function removeEvents() {
    eventList.forEach((params) => {
      removeEventListener(params);
    });
  }

  onMounted(() => {
    nextTick(() => {
      initEvents();
    });
  });

  onBeforeUnmount(() => {
    removeEvents();
  });

  return {
    addEventListener,
    removeEventListener,
    registerEventListener
  };
}

import type { Application } from 'egg';

import { Strategy } from 'passport-local';

export function setupAuthorization(app: Application) {
  const LocalStrategy = new Strategy(
    {
      passReqToCallback: true
    },
    (req, username, password, done) => {
      // format user
      const user = {
        provider: 'local',
        username,
        password
      };
      app.passport.doVerify(req, user, done);
    }
  );

  app.passport.use(LocalStrategy);

  app.passport.verify(async (ctx, user) => {
    console.log('this.passport.verify==========================>', user);
    const { provider } = user;

    // local strategy：authenticate by userName/password
    if (provider === 'local') {
      const userInfo = await ctx.service.user.findFirst({
        name: user.username,
        password: user.password
      });

      // user not exist
      if (!userInfo) return false;

      return userInfo;
    }

    const { providerId, name } = user;
    const auth: any = await ctx.service.authorization.findUnique({
      providerId
    });

    if (!auth) {
      const createdUser = await ctx.service.user.create({ name, providerId });
      const { id: userId } = createdUser;
      const authorization = Object.assign(user, { user_id: userId });
      await ctx.service.authorization.create(authorization);
      return createdUser;
    }

    const userInfo = await ctx.service.user.findUnique({ id: auth.user_id });
    return userInfo;
  });

  app.passport.serializeUser(async (ctx, user) => {
    app.logger.debug('serializeUser', ctx, user);
    return user;
  });

  app.passport.deserializeUser(async (ctx, user) => {
    app.logger.debug('deserializeUser', ctx, user);
    return user;
  });
}

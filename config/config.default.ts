import type { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

import path from 'path';

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1656076425096_2925';

  config.middleware = ['authenticate'];

  // egg-view-vue-ssr
  config.vuessr = {
    manifest: path.join(appInfo.baseDir, 'config/manifest.json'),
    baseDir: path.join(appInfo.baseDir, 'app/view')
  };

  // egg-static
  config.static = {
    dir: [path.join(appInfo.baseDir, 'public')]
  };

  // root static file
  config.siteFile = {};

  // passport-github
  config.passportGithub = {
    key: 'db038613b99ae73afc74', // github clientID
    secret: '24b10825b2c0b268988c68d8d6944e5290d62494' // github secret
  };

  // password-yuque
  config.passportYuque = {
    key: 'nnnULGMAu4zb7V6OsSBG',
    secret: 'O52XjqP6IABjeKEzDfgIUxdT8QmpvuegMXwDuTJV'
  };

  // middleware: authenticate
  config.authenticate = {
    ignore: (ctx) => {
      const ignoreList = [
        '/passport/github',
        '/passport/yuque',
        '/login',
        '/api/login'
      ];
      return ignoreList.includes(ctx.path);
    }
  };

  // add your special config in here
  const bizConfig = {
    sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`
  };

  // the return config will combines to EggAppConfig
  return {
    ...config,
    ...bizConfig
  };
};

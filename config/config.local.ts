import type { EggAppConfig, PowerPartial } from 'egg';

import webpackConfigList from '@root/webpack.config';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};

  config.development = {
    watchDirs: ['lib/plugins', 'lib/localize', 'prisma', 'shared'],
    ignoreDirs: [
      'app/web',
      'config/manifest.json',
      'config/assets-manifest.json'
    ]
  };

  config.webpack = {
    webpackConfigList
  };

  config.vuessr = {
    requireCache: false
  };

  return config;
};

import type { EggPlugin } from 'egg';

import path from 'path';

const plugin: EggPlugin = {
  passport: {
    enable: true,
    package: 'egg-passport'
  },

  passportGithub: {
    enable: true,
    package: 'egg-passport-github'
  },

  passportYuque: {
    enable: true,
    package: 'egg-passport-yuque'
  },

  webpack: {
    enable: process.env.NODE_ENV !== 'production',
    // package: 'egg-webpack'
    path: path.join(__dirname, '../lib/plugins/egg-webpack')
  },

  vuessr: {
    enable: true,
    path: path.join(__dirname, '../lib/plugins/egg-view-vue-ssr')
  },

  prisma: {
    enable: true,
    path: path.join(__dirname, '../lib/plugins/egg-prisma')
  },

  graphqlApollo: {
    enable: true,
    path: path.join(__dirname, '../lib/plugins/egg-graphql-apollo')
  }
};

export default plugin;

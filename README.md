# webdev

a ssr project build on vue、egg、webpack.

## QuickStart

### Development

```bash
$ npm i
$ npx prisma generate
$ npm run dev
$ open http://localhost:7001/
```

Don't tsc compile at development mode, if you had run `tsc` then you need to `npm run clean` before `npm run dev`.

### Deploy

```bash
$ npm run tsc
$ npm start
```

### Npm Scripts

- Use `npm run lint` to check code style
- Use `npm test` to run unit test
- Use `npm run clean` to clean compiled js at development mode once

### Requirement

- Node.js 14.x
- Typescript 2.8+

## VsCode plugins

- StyleLint
- Volar(vue ts)
- Prisma
- PostCSS Language Support
- Tailwind CSS IntelliSense
- GraphQL: Language Feature Support

## Directory

```
|-- app
| |-- web
| | |-- components
| | | |-- base             // atom level components: without business logic
| | | |-- feature          // global level components
| | | |-- biz              // page level components
|-- build                  // view build
|-- config                 // egg config
|-- docs
|-- env
|-- lib                    // local libs
|-- prisma                 // prisma
|-- public
|-- shared                 // shared code between client and server
|-- typing                 // type declarations
|-- webpack.config.ts
```

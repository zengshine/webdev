## Plan

- performance

- node.js; egg.js

- dsa

- typescript

# project

- file base CMS

- [] modular system

- [] hook engine

- hybrid rendering

## Service

- [] File Service

## Node

- base service: prisma delegate type;

- replace coRequest(request): reference to nuxt proxy

## web

- development: state change but HMR doesn't work

- app/utils; app lifecycle; logic

- type axios request result

- [] service factory

- [] gitlab ci/cd: docker, nginx

- pwa: service worker

- [] light-house-ci

- [] theme change: flash of content

- [] theme config

- [x] app/web/tsconfig.json extend not working

- [] css flash of unstyled concent: inline critical css & inline async chunk css

- [] html lang attribute (dynamic international)

- [] authenticate

- [] ssr async data

- [] compiler: web editor

- [] custom head && seo for each page

- [] update login page

### component

- [] lazy-hydrate component HMR failed

- [] unicode component

- i18n

### build

- [x] async chunk refresh failed

- [] splitChunks

- [] worker reload performance

- [x] eslint error

- [] integrate vue-bundle-runner

- [] TypeScript Vue Plugin (Volar)

### workflow

- [] issue template

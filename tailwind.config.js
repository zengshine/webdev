/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ['app/web/**/*.{html,vue,tsx, jsx}'],
  darkMode: 'js-dark-theme',
  theme: {
    extend: {}
  },
  plugins: []
};

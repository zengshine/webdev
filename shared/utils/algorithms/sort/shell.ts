import { swap } from '../../array';

export function shellSort(list: number[]) {
  const length = list.length;
  let gap = list.length;
  while (gap > 1) {
    gap = Math.floor(gap / 2);
    let preIndex = 0;
    let nextIndex = gap;
    for (let i = nextIndex; i < length; i++) {
      if (list[preIndex] > list[nextIndex]) swap(list, preIndex, nextIndex);
      preIndex++;
      nextIndex++;
    }
  }
}

const testData = [1, 2, 22, 21, 87, 2, 3, 7, 7, 10];
shellSort(testData);

console.log('result=========================>', testData);

export function countingSort(list: number[], max: number) {
  const listLength = list.length;
  const countArray = Array(max).fill(0);
  for (let i = 0; i < listLength; i++) {
    countArray[list[i]]++;
  }

  for (let i = 1; i < countArray.length; i++) {
    countArray[i] = countArray[i - 1] + countArray[i];
  }

  const sortedArray = Array(listLength);
  for (let i = listLength - 1; i >= 0; i--) {
    sortedArray[--countArray[list[i]]] = list[i];
  }

  return sortedArray;
}

const testData = [1, 3, 6, 4, 5, 9, 7, 8];
const sortedArray = countingSort(testData, 10);

console.log('result=========================>', sortedArray);

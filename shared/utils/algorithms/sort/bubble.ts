import { swap } from '../../array';

export function bubbleSort(list: number[]) {
  const listLength = list.length;
  for (let i = listLength - 1; i > 0; i--) {
    for (let j = 0; j < i; j++) {
      if (list[j] > list[j + 1]) swap(list, j, j + 1);
    }
  }
}

const testData = [1, 2, 22, 21, 87, 2, 3, 7, 7, 10];
bubbleSort(testData);

console.log('result=========================>', testData);

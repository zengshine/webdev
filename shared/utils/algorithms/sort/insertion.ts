import { swap } from '../../array';

export function insertionSort(list: number[]) {
  const listLength = list.length;
  for (let i = 1; i <= listLength - 1; i++) {
    for (let j = i; j > 0; j--) {
      if (list[j] < list[j - 1]) {
        swap(list, j, j - 1);
      } else {
        break;
      }
    }
  }
}

const testData = [1, 2, 22, 21, 87, 2, 3, 7, 7, 10];
insertionSort(testData);

console.log('result=========================>', testData);

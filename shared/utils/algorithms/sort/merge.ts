export function mergeSort(list: number[], start: number, end: number) {
  if (start >= end) return [list[start]];

  const mid = Math.floor((start + end) / 2);
  const leftSort = mergeSort(list, start, mid);
  const rightSort = mergeSort(list, mid + 1, end);

  let i = 0,
    j = 0,
    k = 0,
    l = 0,
    restArray;
  const result = Array(end - start + 1);

  while (i < leftSort.length && j < rightSort.length) {
    if (leftSort[i] < rightSort[j]) {
      result[k++] = leftSort[i++];
    } else {
      result[k++] = rightSort[j++];
    }
  }

  if (i === leftSort.length) {
    l = j;
    restArray = rightSort;
  } else {
    l = i;
    restArray = leftSort;
  }

  for (; l < restArray.length; ) {
    result[k++] = restArray[l++];
  }

  return result;
}

const testData = [1, 4, 3, 1, 8, 23, 11, 44, 1];
const result = mergeSort(testData, 0, testData.length - 1);

console.log('result=========================>', result);

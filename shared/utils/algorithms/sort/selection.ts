import { swap } from '../../array';

export function selectionSort(list: number[]) {
  const listLength = list.length;
  for (let i = 0; i < listLength - 2; i++) {
    let selectedIndex = i;
    for (let j = i + 1; j <= listLength - 1; j++) {
      if (list[j] < list[selectedIndex]) selectedIndex = j;
    }
    swap(list, i, selectedIndex);
  }
}

const testData = [1, 2, 22, 21, 87, 2, 3, 7, 7, 10];
selectionSort(testData);

console.log('result=========================>', testData);

import { swap } from '../../array';

export function heapSort(list: number[]) {
  const listLength = list.length;
  for (let i = 1; i < listLength; i++) {
    heapifyUp(list, i);
  }

  for (let i = listLength - 1; i > 0; i--) {
    swap(list, i, 0);
    heapifyDown(list, i - 1);
  }
}

function heapifyDown(list: number[], index: number) {
  let curIndex = 0;
  const endIndx = Math.floor((index - 1) / 2);
  while (curIndex <= endIndx) {
    const leftIndex = curIndex * 2 + 1;
    const rightIndex = curIndex * 2 + 2;

    if (list[curIndex] >= list[leftIndex] && list[curIndex] >= list[rightIndex])
      break;

    let maxIndex;
    if (rightIndex > index) {
      maxIndex = leftIndex;
    } else {
      maxIndex = list[rightIndex] > list[leftIndex] ? rightIndex : leftIndex;
    }

    swap(list, curIndex, maxIndex);
    curIndex = maxIndex;
  }
}

function heapifyUp(list: number[], index: number) {
  let curIndex = index;
  while (curIndex > 0) {
    const parentIndex = Math.floor((curIndex - 1) / 2);
    if (list[curIndex] <= list[parentIndex]) break;

    swap(list, curIndex, parentIndex);
    curIndex = parentIndex;
  }
}

const testData = [1, 4, 3, 1, 44, 2, 11, 345, 33, 8, 23, 11, 44, 1];
heapSort(testData);

console.log('result=========================>', testData);

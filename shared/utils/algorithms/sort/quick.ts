import { swap } from '../../array';

export function quickSort(list: number[], start: number, end: number) {
  if (start >= end) return;

  const listLength = list.length;
  let pivot = Math.floor((start + end) / 2);
  const pivotValue = list[pivot];
  let k = 0;
  for (let i = 0; i < listLength; i++) {
    if (list[i] < pivotValue) {
      swap(list, k++, i);
    }
  }
  if (list[k] > pivotValue) {
    swap(list, k++, pivot);
    pivot = k;
  }

  quickSort(list, start, pivot - 1);
  quickSort(list, pivot + 1, end);
}

const testData = [1, 2, 22, 21, 87, 2, 3, 7, 7, 10];
quickSort(testData, 0, testData.length - 1);

console.log('result=========================>', testData);

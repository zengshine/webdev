export function radixSort(list: number[], digit: number, max: number) {
  let result = list;
  const listLength = result.length;
  for (let i = digit - 1; i >= 0; i--) {
    const indexedArray = Array(max).fill(0);
    const sortedArray = Array(listLength);
    for (let j = 0; j < listLength; j++) {
      const val = String(result[j]).padStart(digit, '0')[i];
      indexedArray[val]++;
    }
    for (let l = 1; l < max; l++) {
      indexedArray[l] = indexedArray[l] + indexedArray[l - 1];
    }
    for (let k = listLength - 1; k >= 0; k--) {
      const val = String(result[k]).padStart(digit, '0')[i];
      sortedArray[--indexedArray[val]] = result[k];
    }
    result = sortedArray;
  }
  return result;
}

const testData = [127455, 223458, 324456, 334456, 425456, 127456, 526456];
const result = radixSort(testData, 6, 10);

console.log('result=========================>', result);

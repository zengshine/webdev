export default function maxProfit(list: number[]) {
  const listLength = list.length;
  let profit = 0;
  let k = 0;
  for (let i = 1; i < listLength; i++) {
    if (list[i] < list[i - 1]) {
      if (list[k] < list[i - 1]) {
        profit += list[i - 1] - list[k];
        k = i;
      }
    }
  }
  if (list[k] < list[listLength - 1]) {
    profit += list[listLength - 1] - list[k];
  }
  return profit;
}

const testData = [1, 2, 2, 3, 11, 4, 4, 6, 7, 7, 7, 10];
const profit = maxProfit(testData);

console.log('result=========================>', profit);

export function rotateArray(list: number[], step: number) {
  const listLength = list.length;
  let curStep = 0,
    nextStep = 0;
  let preStepVal = list[curStep];
  for (let i = 0; i < listLength; i++) {
    nextStep = curStep + step;
    if (nextStep >= listLength) nextStep = nextStep - listLength;

    const temp = list[nextStep];
    list[nextStep] = preStepVal;
    preStepVal = temp;

    curStep = nextStep;
  }
}

const testData = [1, 2, 2, 3, 7, 7, 10];
rotateArray(testData, 3);

console.log('result=========================>', testData);

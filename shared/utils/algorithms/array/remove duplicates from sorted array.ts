export default function deDuplicateSortNumbers(list: number[]) {
  const listLength = list.length;
  let k = 1;
  for (let i = 1; i < listLength; i++) {
    if (list[i] !== list[i - 1]) {
      list[k++] = list[i];
    }
  }
  for (let i = k; i < listLength; i++) {
    list[i] = NaN;
  }
}

const testData = [1, 2, 2, 3, 4, 4, 4, 6, 7, 7, 7, 10];
deDuplicateSortNumbers(testData);

console.log('result=========================>', testData);

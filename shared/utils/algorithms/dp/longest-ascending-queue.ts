export function findLongestAscQueue(list: number[]): number[] {
  const length = list.length;
  const stateList = Array(length).fill(1);

  for (let i = 0; i < length; i++) {
    for (let j = 0; j < i; j++) {
      if (list[i] >= list[j]) {
        if (stateList[j] >= stateList[i]) stateList[i] = stateList[j] + 1;
      }
    }
  }

  let lastIndex = 0;
  let maxVal = stateList[lastIndex];
  for (let i = 0; i < length; i++) {
    if (stateList[i] > maxVal) {
      maxVal = stateList[i];
      lastIndex = i;
    }
  }
  return list.filter((item) => item <= list[lastIndex]);
}

const testData = [1, 2, 22, 21, 87, 2, 3, 7, 7, 10];
const result = findLongestAscQueue(testData);

console.log('result=========================>', result);

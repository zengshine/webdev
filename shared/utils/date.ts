/**
 * 时间格式化
 * @param timestamp timestamp
 * @param format format
 * @return formatted string
 */

export function formatDate(timestamp, format?) {
  const date = new Date(timestamp);
  const year = date.getFullYear();
  const month = `${date.getMonth() + 1}`.padStart(2, '0');
  const mDate = `${date.getDate()}`.padStart(2, '0');

  if (!format) return `${year}-${month}-${mDate}`;

  const holderReg = /[ymd]/gi;

  let formatStr = '';
  const valueMap = {
    y: year,
    m: month,
    d: mDate
  };
  for (let i = 0; i < format.length; i++) {
    const chart = format[i];
    const isHolder = holderReg.test(chart);
    formatStr += isHolder ? valueMap[chart] : chart;
  }

  return formatStr;
}

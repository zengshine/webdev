export * from '@root/shared/utils/array';

/**
 * empty function
 * @return {void}
 */
export const NOOP = () => {
  return;
};

/**
 * whether it is production mode
 * @return {boolean} result
 */
export const isProduction = () => {
  return process.env.NODE_ENV === 'production';
};

/**
 * whether it is browser environment
 * @return {boolean} result
 */
export const isBrowser = () => {
  return typeof window === 'object';
};

/**
 * whether it is an empty object
 * @param val input
 * @return {boolean} result
 */
export const isEmptyObject = (val: object) => {
  if (!val) return true;

  return Object.keys(val).length === 0;
};

/**
 * get datatype of value
 * @param val input
 * @return {string} result
 */
export function getDataType(val): string {
  return Object.prototype.toString.call(val).slice(8, -1);
}

/**
 * object values transform
 * @param val object value
 * @param transform  transform function
 * @return {object} transformed object
 */
export function transformObjectProperties(
  val: object,
  transform: (any) => any
): object {
  const result = {};
  for (const p in val) {
    const keyVal = val[p];
    result[p] = transform(keyVal) || keyVal;
  }
  return result;
}

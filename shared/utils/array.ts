/**
 * swap the value of random index
 * @param list dataList
 * @param i swapIndex
 * @param j swapIndex
 */
export function swap(list, i: number, j: number): void {
  const temp = list[i];
  list[i] = list[j];
  list[j] = temp;
}

/**
 * transform to array
 * @param value input
 * @return {Array} transformed array
 */
export function transformToArray<T = any>(value): T[] {
  return Array.isArray(value) ? value : [value];
}

/**
 * iterate collection of handlers to transform value
 * @param {array} handlers collection of handlers
 * @return {any} value transformed value
 */
export function mapReduce(handlers: anyFn[]): (...args: any) => {
  [key: string]: any;
} {
  return (args) => {
    return handlers.reduce((innerArgs, handler) => {
      return handler(innerArgs);
    }, args);
  };
}

/**
 * concat and duplicate array
 * @param {array} args collection of array
 * @return {array} result of concat and duplicate array
 */
export function mergeArray<T = any>(...args: T[][]): T[] {
  const concatArray = args.reduce((res, cur) => [...res, ...cur], []);
  if (typeof Set !== 'undefined') {
    return Array.from(new Set(concatArray));
  }

  for (let i = 0; i < concatArray.length - 1; i++) {
    for (let j = i + 1; j < concatArray.length; j++) {
      if (concatArray[i] === concatArray[j]) {
        concatArray.splice(j--, 1);
      }
    }
  }

  return concatArray;
}

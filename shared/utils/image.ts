export function getImageSize(
  url
): Promise<{ image: HTMLImageElement; width: number; height: number }> {
  return new Promise((resolve) => {
    const image = new Image();
    image.onload = function () {
      const { width, height } = image;
      resolve({ image, width, height });
    };
    image.src = url;
  });
}

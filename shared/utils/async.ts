/**
 * resolve after {time} milliseconds
 * @param time
 * @return Promise<number>
 */

export function sleep(time?: number): Promise<number> {
  if (!time) return Promise.resolve(0);

  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(time);
    }, time);
  });
}

/**
 * async function composer
 * @param funcList compose function list
 * @return { Function } return function
 */
export function compose(funcList: anyFn[]) {
  return function (context, next) {
    let index = 0;
    return dispatch(0);

    function dispatch(nextIndex) {
      if (nextIndex < index) {
        return Promise.reject(new Error('next exec more than one time'));
      }

      const fn = funcList[nextIndex] || next;
      if (!fn) return Promise.resolve();

      try {
        return Promise.resolve(fn(context, dispatch.bind(null, ++index)));
      } catch (err) {
        return Promise.resolve(err);
      }
    }
  };
}

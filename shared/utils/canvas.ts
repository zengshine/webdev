import { getImageSize } from '@root/shared/utils/image';

export async function toGrayscaleImage(url) {
  const { image, width, height } = await getImageSize(url);
  const canvas = document.createElement('canvas');
  document.body.append(canvas);
  const ctx = canvas.getContext('2d')!;
  ctx.drawImage(image, 0, 0, width, height);
  const imageData = ctx.getImageData(0, 0, width, height)!;
  const { data } = imageData;
  for (let i = 0; i < data.length; i += 4) {
    const avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
    data[i] = avg; // red
    data[i + 1] = avg; // green
    data[i + 2] = avg; // blue
    // data[i + 3] = 1; // alpha
  }
  ctx.putImageData(imageData, 0, 0);
}

export interface Abortable {
  abort: (msg: string) => void;
  msg: string;
}

interface ExecutorFunction<T> {
  (
    resolve: (value: PromiseLike<T> | T) => void,
    reject: (error?: any) => void
  ): void;
}

interface AbortableExecutorFunction<T> {
  (
    resolve: (value: PromiseLike<T> | T) => void,
    reject: (error?: any) => void,
    abortSignal: AbortSignal
  ): void;
}

export class AbortError extends Error {
  constructor(msg = 'abort') {
    super(msg);
    this.name = msg;
  }
}

export class AbortablePromise<T> extends Promise<T> implements Abortable {
  public abort: Abortable['abort'];
  public get abortReason(): string | undefined {
    return this._abortReason;
  }

  private _abortReason?: string;

  constructor(executor: AbortableExecutorFunction<T>) {
    const abortController = new AbortController();
    const signal = abortController.signal;
    const normalExecutor: ExecutorFunction<T> = (resolve, reject) => {
      signal.addEventListener('abort', () => {
        reject(new AbortError(this.abortReason));
      });
      executor(resolve, reject, signal);
    };

    super(normalExecutor);

    this.abort = (msg: string) => {
      this._abortReason = msg;
      abortController.abort();
    };
  }
  msg: string;

  static from = <T>(promise: Promise<T>): AbortablePromise<T> => {
    if (promise instanceof AbortablePromise) {
      return promise;
    }

    return new AbortablePromise((resolve, reject) => {
      return promise.then(resolve).catch(reject);
    });
  };
}

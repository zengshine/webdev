/**
 * Debounce function
 * @param fn callback
 * @param interval interval
 * @param immediate immediate execute
 * @return debounce function
 */

export function debounce<T extends anyFn>(
  fn: T,
  interval: number,
  immediate?: boolean
) {
  // const context = this
  let timerId: number;
  const clearTimer = () => {
    clearTimeout(timerId);
    timerId = 0;
  };
  const setTimer = (fn: anyFn) => {
    return window.setTimeout(() => fn(), interval);
  };

  const executor = (...args) => {
    fn.apply(null, ...(args as []));
  };

  const delayExecutor = (...args) => {
    return setTimer(() => {
      executor(...args);
      clearTimer();
    });
  };

  return (...args) => {
    if (timerId) {
      clearTimer();
      timerId = delayExecutor(...args);
    } else if (immediate) {
      executor(...args);
      timerId = setTimer(clearTimer);
    } else {
      timerId = delayExecutor(...args);
    }
  };
}

/**
 * Throttle function
 * @param fn callback
 * @param interval interval
 * @param immediate immediate execute
 * @return Throttle function
 */

export function throttle<T extends anyFn>(
  fn: T,
  interval: number,
  immediate?: boolean
) {
  // const context = this
  let timerId: number;
  const setTimer = (fn: anyFn) => {
    return window.setTimeout(() => fn(), interval);
  };
  const clearTimer = () => {
    clearTimeout(timerId);
    timerId = 0;
  };
  const executor = (...args) => {
    fn.apply(null, ...(args as []));
  };
  const delayExecutor = (...args) => {
    return setTimer(() => {
      executor(...args);
      clearTimer();
    });
  };

  return (...args) => {
    if (timerId) return;

    if (immediate) {
      executor(...args);
      timerId = setTimer(clearTimer);
    } else {
      timerId = delayExecutor(...args);
    }
  };
}

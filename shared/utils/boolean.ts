/**
 * transform to boolean
 * @param val input
 * @return {boolean} transformed value
 */
export function is(val: any): boolean {
  const toNum = parseFloat(val);
  if (!Number.isNaN(toNum)) return Boolean(toNum);

  return Boolean(val);
}

import { createHead, renderHeadToString } from '@vueuse/head';

const headConfig = createHead({
  meta: [
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1, maximum-scale=5'
    },
    {
      name: 'description',
      content: 'Code for fun and life!'
    }
  ],
  script: [
    {
      src: '/public/assets/iconfont/font.js?v=0.0.2',
      defer: true
    }
  ],
  link: [
    {
      rel: 'manifest',
      href: '/public/assets/manifest.json'
    }
  ]
});

export async function getSSRHeadConfig() {
  return await renderHeadToString(headConfig);
}

export function setAfterRenderedEntries() {
  headConfig.push({
    script: []
  });
}

export default headConfig;

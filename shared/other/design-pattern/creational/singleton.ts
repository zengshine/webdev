/**
 * The Singleton class defines the 'getInstance' method that lets clients access the unique singleton instance.
 */
class Singleton {
  private static instance: Singleton;

  /**
   * Singleton Pattern
   *
   * Singleton lets you ensure that a class has only one instance, while provide a global
   * access point to this instance.
   */

  /**
   * The Singleton's constructor should always be private to prevent direct construction calls
   * with the 'new' operator
   */
  private constructor() {
    console.log('private constructor=========================>');
  }

  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
    }

    return Singleton.instance;
  }

  public operation() {
    console.log('operation=========================>');
  }
}

const instance = Singleton.getInstance();
instance.operation();

export {};

/**
 * Factory Method Pattern
 *
 * Factory Method provides an interface for creating objects in a superclass but allows
 * subclass to alter the type of objects that will be created.
 */

/**
 * The creator class declares the factory method that is supposed to return an object
 * of a Product class. The creator's subclass usually provide the implementation of this method
 */
abstract class Creator {
  /**
   * The Creator may also provide some default implementation of the factory method.
   */
  public abstract factoryMethod(): Product;

  /**
   * The creator's primary responsibility is not creating product. Usually, it contains some
   * core business logic that relies on Product objects, returned by the factory method. Subclass
   * can indirectly change that business logic by override the factory method and returning a
   * different type of product from it.
   */
  public someOperation(): string {
    const product = this.factoryMethod();

    return `someOperation ==================== ${product.operation()}`;
  }
}

/**
 * Concrete Creators override the factory method in order to change the resulting product's type
 */
class ConcreteCreatorA extends Creator {
  public factoryMethod(): Product {
    return new ConcreteProductA();
  }
}

/**
 * The product interface declare the operations that all concrete products must implement
 */
class ConcreteCreatorB extends Creator {
  public factoryMethod(): Product {
    return new ConcreteProductB();
  }
}

/**
 * The Product interface declares the operations that all products must implement
 */
interface Product {
  operation(): string;
}

/**
 * Concrete Products provide various implements of the Product interface.
 */
class ConcreteProductA implements Product {
  public operation(): string {
    return 'ConcreteProductA operation=========================>';
  }
}

class ConcreteProductB implements Product {
  public operation(): string {
    return 'ConcreteProductB operation=========================>';
  }
}

const creatorA = new ConcreteCreatorA();
const creatorB = new ConcreteCreatorB();
creatorA.someOperation();
creatorB.someOperation();

export {};

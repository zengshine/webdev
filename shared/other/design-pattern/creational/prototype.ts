/**
 * Prototype Pattern
 *
 * Prototype lets you copy existing objects without making your code dependent on their classes.
 */

/**
 * The example class that has clothing ability.
 */
class Prototype {
  public primitive: any;
  public component: object;
  public circularReference: ComponentWithBackReference;

  public clone(): this {
    const clone = Object.create(this);

    clone.component = Object.create(this.component);

    /**
     * Cloning an object that has a nested object with backReference require
     * special treatment. After the cloning is completed, the nested object
     * should point to the cloned object, instead of the original object.
     * spread operator can be handy for this case.
     */
    clone.circularReference = {
      ...this.circularReference,
      prototype: {
        ...this
      }
    };

    return clone;
  }
}

class ComponentWithBackReference {
  public prototype;

  constructor(prototype: Prototype) {
    this.prototype = prototype;
  }
}

const p1 = new Prototype();
p1.primitive = 245;
p1.component = new Date();
p1.circularReference = new ComponentWithBackReference(p1);

const p2 = p1.clone();
console.log(
  'p2=========================>',
  p2.circularReference === p1.circularReference
);

export {};

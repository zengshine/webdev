/**
 * Decorator Pattern
 *
 * Decorator lets you attach new behaviors to objects by placing these objects inside
 * special wrapper objects that contain the behaviors.
 */

/**
 * The base Component interface defines operations that can be altered by decorators.
 */
interface Component {
  operation(): string;
}

/**
 * Concrete Components provide default implementations of the operation. There might
 * be several variations of these classes.
 */
class ConcreteComponent implements Component {
  public operation(): string {
    return 'ConcreteComponent';
  }
}

/**
 * The base Decorator class follows the same interface as the other components.
 * The primary purpose of this is to define the wrapping interface for all
 * concrete decorators. The default implementation of the wrapping code might
 * include a field for storing a wrapped component and means to initialize it.
 */
class Decorator implements Component {
  protected component: Component;

  constructor(component: Component) {
    this.component = component;
  }

  /**
   * The operator delegate all work to the wrapped component
   */
  public operation(): string {
    return this.component.operation();
  }
}

/**
 * Concrete decorator call the wrapped object and alter its result in some way.
 */
class ConcreteDecorator extends Decorator {
  public operation(): string {
    return `ConcreteDecorator ${super.operation()}`;
  }
}

const concreteComponent = new ConcreteComponent();
const concreteDecorator = new ConcreteDecorator(concreteComponent);
concreteDecorator.operation();

export {};

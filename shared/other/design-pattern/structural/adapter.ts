/**
 * Adapter Pattern
 *
 * Adapter allows objects with incompatible interfaces to collaborate.
 */

/**
 * The target defines the domain-specific interface used by the client
 */
class Target {
  public request(): string {
    return `Target: The default target's behaviors`;
  }
}

/**
 * The Adapted contains some useful behavior, but its behavior is incompatible
 * with the existing client code. The Adaptee needs some adaptation before the
 * client code can use it.
 */
class Adaptee {
  public specificRequest(): string {
    return 'Adaptee specificRequest=========================>';
  }
}

/**
 * The Adapter makes the Adaptee's interface compatible with the target's interface
 */
class Adapter extends Target {
  private adaptee: Adaptee;

  constructor(adaptee: Adaptee) {
    super();
    this.adaptee = adaptee;
  }

  public request(): string {
    return this.adaptee.specificRequest();
  }
}

// client code
const adapter = new Adapter(new Adaptee());
adapter.request();

export {};

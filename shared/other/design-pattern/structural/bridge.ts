/**
 * Bridge Pattern
 *
 * Bridge lets you split a large class or a set of closely related classes into two separate hierarchies -
 * abstraction and implementation - which can be developed independently of each other
 */

/**
 * The Abstraction defines the interface for the 'control' part of the two class hierarchy. It maintains a
 * reference to an object of the Implementation hierarchy and delegate all of work to this object.
 */
class Abstraction {
  protected implementation: Implementation;

  constructor(implementation: Implementation) {
    this.implementation = implementation;
  }

  public operation(): string {
    return this.implementation.operationImplementation();
  }
}

class ExtendedAbstraction extends Abstraction {
  public operation(): string {
    console.log('ExtendedAbstraction=========================>');
    return (
      this.implementation.operationImplementation() + 'ExtendedAbstraction'
    );
  }
}

/**
 * The Implementation defines the interface for all Implementation classes.
 * It doesn't have to match the Abstraction's interface. In fact, the two interfaces
 * can be entirely different. Typically the Implementation interface provides only primitive
 * operations, while the Abstraction defines higher-level operations based on those primitives
 */

interface Implementation {
  operationImplementation(): string;
}

/**
 * Each Concrete Implementation corresponds to a specific platform and implements the implementation
 * interface using that platform API.
 */
class ConcreteImplementationA implements Implementation {
  public operationImplementation(): string {
    return 'ConcreteImplementationA';
  }
}

class ConcreteImplementationB implements Implementation {
  public operationImplementation(): string {
    return 'ConcreteImplementationB';
  }
}

const abstractionA = new Abstraction(new ConcreteImplementationA());
const abstractionB = new Abstraction(new ConcreteImplementationB());
const extendAbstraction = new ExtendedAbstraction(
  new ConcreteImplementationA()
);

abstractionA.operation();
abstractionB.operation();
extendAbstraction.operation();

export {};

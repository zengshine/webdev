/**
 * Composite Pattern
 *
 * Composite lets you compose objects into tree structures and then work with these structures as
 * if the were individual objects.
 */

/**
 * The base Component class declares common operation for both simple and complex objects of a composition
 */
type ComponentParams = Component | null;
abstract class Component {
  protected parent!: Component | null;

  /**
   * Optionally, the base Component can declare an interface for setting and accessing a parent of
   * component in a tree structure. It can also provide some default implementation for these methods.
   */
  public setParent(parent: ComponentParams) {
    this.parent = parent;
  }

  public getParent(): ComponentParams {
    return this.parent;
  }

  /**
   * In some cases, it would be beneficial to define the child-management
   * operations right in the base Component class. This way, you won't need to
   * expose any concrete component classes to the client code, even during the
   * object tree assembly. The downside is that these methods will be empty
   * for the leaf-level components.
   */
  // public add(component: Component):void {}
  // public remove(component: Component):void {}

  public abstract isComposite(): boolean;

  public abstract operate(): string;
}

/**
 * The Leaf class represents the end objects of a composition. A leaf can't have any children
 *
 * Usually, it's the Leaf objects that do the actual work, whereas Composite objects only
 * delegate to their sub-components
 */
class Leaf extends Component {
  public isComposite(): boolean {
    return false;
  }

  public operate(): string {
    return 'leaf';
  }
}

/**
 * The Composite class represents the complex components that may have children. Usually, the
 * Composite objects delegate the actual work to their children and them "sum-up" the result
 */
class Composite extends Component {
  protected children: Component[] = [];

  /**
   * A composite object can add or remove other components (both simple or
   * complex) to or from its child list.
   */
  public add(component: Component): void {
    this.children.push(component);
    component.setParent(this);
  }

  public remove(component: Component): void {
    const componentIndex = this.children.indexOf(component);
    this.children.splice(componentIndex, 1);

    component.setParent(null);
  }

  public isComposite(): boolean {
    return true;
  }

  /**
   * The Composite executes its primary logic in a particular way. It traverses recursively
   * through all its children, collecting and summing their results.
   */
  public operate(): string {
    const results: string[] = [];
    for (const child of this.children) {
      results.push(child.operate());
    }
    return results.join();
  }
}

const composite = new Composite();
composite.add(new Leaf());
composite.add(new Leaf());
composite.operate();

export {};

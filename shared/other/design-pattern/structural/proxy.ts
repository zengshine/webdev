/**
 * Proxy Pattern
 *
 * Proxy lets you provide a substitute or placeholder for another object. A proxy
 * controls access to the original object, allowing you to perform something either
 * before or after the request gets through to the original object.
 */

/**
 * The Subject interface declares common operations for both RealSubject and the Proxy.
 * As long as the client works with RealSubject using this interface, you' ll be able
 * to pass it a proxy instead of a real subject.
 */
interface Subject {
  request(): void;
}

/**
 * The RealSubject contains some core business logic. Usually, RealSubjects are capable of
 * doing some useful work which may also be very slow or sensitive - e.g. correcting input
 * data. A proxy solve these issues without any changes to the RealSubject's code.
 */
class RealSubject implements Subject {
  public request(): void {
    console.log('RealObject request=========================>');
  }
}

/**
 * The Proxy maintains a reference to an object of the RealObject class. It can be either lazy-loaded
 * or passed to the Proxy by the client
 */
class Proxy implements Subject {
  private realSubject: RealSubject;

  /**
   * The most common applications of the Proxy pattern are lazy loading,
   * caching, controlling the access, logging, etc. A Proxy can perform one of
   * these things and then, depending on the result, pass the execution to the
   * same method in a linked RealSubject object.
   */
  constructor(realSubject: RealSubject) {
    this.realSubject = realSubject;
  }

  public request(): void {
    if (!this.checkAccess()) {
      console.log('unable to access=========================>');
      return;
    }
    this.realSubject.request();
    this.logAccess();
  }

  public checkAccess() {
    return Math.random() > 0.5;
  }

  public logAccess() {
    console.log('logAccess=========================>', Date.now());
  }
}

const proxy = new Proxy(new RealSubject());
proxy.request();

export {};

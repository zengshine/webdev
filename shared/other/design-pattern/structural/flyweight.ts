/**
 * Flyweight Pattern
 *
 * Flyweight Lets you fit more objects into available amount of RAM by sharing common parts of state
 * between multiple objects instead of keeping all of the data in each object.
 */

/**
 * The Flyweight stores a common portion of the state(also called intrinsic state) that belongs
 * to multiple real business entities. The Flyweight accepts the rest of the state(extrinsic state,
 * unique for each entity) via its method parameters.
 */
class Flyweight {
  private sharedState: any;

  constructor(shareState: any) {
    this.sharedState = shareState;
  }

  public operation(uniqueState): void {
    console.log(
      'operation=========================>',
      JSON.stringify(this.sharedState),
      JSON.stringify(uniqueState)
    );
  }
}

/**
 * The Flyweight factory creates and manages the Flyweight objects. It ensures that flyweight are
 * shared correctly. When the client request a flyweight, the factory either returns an existing
 * instance or create a new one, if it doesn't exist.
 */
class FlyweightFactory {
  private flyweights: { [key: string]: Flyweight } = <any>{};

  constructor(flyweights: string[][]) {
    for (const state of flyweights) {
      this.flyweights[this.getKey(state)] = new Flyweight(state);
    }
  }

  /**
   * Returns a Flyweight's string hash for a given state
   */
  private getKey(state: string[]) {
    return state.join('_');
  }

  /**
   * Returns an existing Flyweight with given state or creates a new one.
   */
  public getFlyweight(shareState: string[]): Flyweight {
    const key = this.getKey(shareState);

    if (!this.flyweights[key]) {
      this.flyweights[key] = new Flyweight(shareState);
    }

    return this.flyweights[key];
  }
}

// client code
const factory = new FlyweightFactory([
  ['Mercedes Benz', 'C300', 'black'],
  ['BMW', 'M5', 'red'],
  ['BMW', 'X6', 'white']
]);

const flyweight = factory.getFlyweight(['BMW', 'M5', 'red']);

flyweight.operation({ action: 'power up' });

export {};

/**
 * chain-Responsibility Pattern
 *
 * This pattern lets you pass requests along a chain of handlers.
 * Upon receiving a request, each handler decides either to process the request
 * or pass it to the next handler in the chain.
 */

/**
 * The handler interface declare a method for building the chain of handlers.
 * It also declares a method for executing a request
 */
interface Handler {
  setNext(handler: Handler): Handler;
  handle(request: string): string;
}

/**
 * The default behavior can be implemented inside a base handler class.
 */
abstract class AbstractHandler implements Handler {
  private nextHandler: Handler;

  public setNext(handler: Handler): Handler {
    this.nextHandler = handler;
    return handler;
  }

  public handle(request: string): string {
    if (this.nextHandler) {
      return this.nextHandler.handle(request);
    }

    return '';
  }
}

/**
 * All concrete Handlers either handle a request or pass it to the next handler in the chain.
 */
class MonkeyHandler extends AbstractHandler {
  public handle(request: string): string {
    console.log('MonkeyHandler=========================>', request);
    return super.handle(request);
  }
}

class DogHandler extends AbstractHandler {
  public handle(request: string): string {
    console.log('DogHandler=========================>', request);
    return super.handle(request);
  }
}

// client code
const monkeyHandler = new MonkeyHandler();
const dogHandler = new DogHandler();

const handlers = monkeyHandler.setNext(dogHandler);

handlers.handle('client code');

export {};

/**
 * Command Pattern
 *
 * Turns a request into a stand-alone object that contains all information about the request.
 * This transformation lets you pass request as a method arguments, delay or queue a request's execution,
 * and support undo-operations
 */

/**
 * The command interface declare a method for executing a command
 */
interface Command {
  execute(): void;
}

class SimpleCommand implements Command {
  private payload: string;

  constructor(payload: string) {
    this.payload = payload;
  }

  public execute(): void {
    console.log('simple execute=========================>', this.payload);
  }
}

/**
 * some commands can delegate complex operation to other Object,
 * called receivers
 */
class ComplexCommand implements Command {
  private payload: string;
  private receiver: Receiver;

  constructor(receiver: Receiver, payload: string) {
    this.payload = payload;
    this.receiver = receiver;
  }

  public execute(): void {
    this.receiver.operate(this.payload);
  }
}

/**
 * The Receiver classes contain some important business logic.
 * They know how to  perform all kinds of operations, associated with carrying out a request.
 */
class Receiver {
  public operate(payload) {
    console.log('Receiver payload=========================>', payload);
  }
}

/**
 * The Invoker is associated with one or several commands.
 * It sends a request to the command
 */
class Invoker {
  private onStart: Command;
  private onEnd: Command;

  public setStartCommand(payload: Command) {
    this.onStart = payload;
  }

  public setEndCommand(payload: Command) {
    this.onEnd = payload;
  }

  operate() {
    this.onStart.execute();
    this.onEnd.execute();
  }
}

const invoker = new Invoker();
invoker.setStartCommand(new SimpleCommand('simple'));
invoker.setEndCommand(new ComplexCommand(new Receiver(), 'complex'));
invoker.operate();

export {};

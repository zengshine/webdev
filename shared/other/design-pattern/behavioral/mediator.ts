/**
 * Mediator Pattern
 *
 * Mediator lets you reduce chaotic dependencies between objects. The pattern restrict direct
 * communication between the objects and forces them to collaborate only via a mediator object
 */

/**
 * The mediator interface declares a method used by components to notify the mediator about various events.
 * The mediator may react to these events and pass the execution to other components.
 */
interface Mediator {
  notify(sender: object, event: string): void;
}

/**
 * Concrete Mediators implement cooperative behavior by coordinating several components
 */
class ConcreteMediator implements Mediator {
  private componentA: ComponentA;
  private componentB: ComponentB;

  constructor(cA: ComponentA, cB: ComponentB) {
    this.componentA = cA;
    this.componentA.setMediator(this);
    this.componentB = cB;
    this.componentB.setMediator(this);
  }

  public notify(sender: object, event: string): void {
    if (event === 'A') this.componentB.handle();

    if (event === 'B') this.componentA.handle();
  }
}

/**
 * The base Component provides the basic functionality of storing a mediator's instance inside component objects.
 */
class BaseComponent {
  protected mediator: Mediator;

  constructor(mediator?: Mediator) {
    if (mediator) this.mediator = mediator;
  }

  public setMediator(mediator: Mediator) {
    this.mediator = mediator;
  }
}

/**
 * Concrete Components implement various functionality. They don't depend on
 * other components. They also don't depend on any concrete mediator classes.
 */
class ComponentA extends BaseComponent {
  public handle() {
    console.log('ComponentA handle=========================>');
    this.mediator.notify(this, 'A');
  }
}

class ComponentB extends BaseComponent {
  public handle() {
    console.log('ComponentB handle=========================>');
    this.mediator.notify(this, 'B');
  }
}

const cA = new ComponentA();
const cB = new ComponentB();
new ConcreteMediator(cA, cB);
cA.handle();
cB.handle();

export {};

/**
 * The Component interface declares an 'accept' method that should take the base visitor interface as an argument
 */
interface Component {
  accept(visitor: Visitor): void;
}

/**
 * Each Concrete Component must implement the 'accept' method in such a way that
 * it calls the visitor's method corresponding to the component's class.
 */
class ConcreteComponentA implements Component {
  accept(visitor: Visitor) {
    visitor.visitConcreteComponentA(this);
  }

  customMethodA() {
    console.log('ConcreteComponentA customMethodA=========================>');
  }
}

class ConcreteComponentB implements Component {
  accept(visitor: Visitor) {
    visitor.visitConcreteComponentB(this);
  }

  customMethodB() {
    console.log('ConcreteComponentA customMethodB=========================>');
  }
}

/**
 * The Visitor interface declares a set of visiting methods that correspond to component classes.
 * The signature of a visiting method allows the visitor to identify the exact class of the component that it dealing with.
 */
interface Visitor {
  visitConcreteComponentA(element: ConcreteComponentA): void;
  visitConcreteComponentB(element: ConcreteComponentB): void;
}

/**
 * Concrete Visitors implement several versions of the same algorithm, which can work with all concrete component classes.
 */

class ConcreteVisitorA implements Visitor {
  public visitConcreteComponentA(element: ConcreteComponentA): void {
    console.log(
      'ConcreteVisitorA=========================>',
      element.customMethodA()
    );
  }

  public visitConcreteComponentB(element: ConcreteComponentB): void {
    console.log(
      'ConcreteVisitorA=========================>',
      element.customMethodB()
    );
  }
}

class ConcreteVisitorB implements Visitor {
  public visitConcreteComponentA(element: ConcreteComponentA): void {
    console.log(
      'ConcreteVisitorB=========================>',
      element.customMethodA()
    );
  }

  public visitConcreteComponentB(element: ConcreteComponentB): void {
    console.log(
      'ConcreteVisitorB=========================>',
      element.customMethodB()
    );
  }
}

// client code

function clientVisit(components: Component[], visitor: Visitor) {
  for (const component of components) {
    component.accept(visitor);
  }
}

const components = [new ConcreteComponentA(), new ConcreteComponentB()];

clientVisit(components, new ConcreteVisitorA());
clientVisit(components, new ConcreteVisitorB());

export {};

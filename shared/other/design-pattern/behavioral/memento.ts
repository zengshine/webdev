/**
 * Memento Pattern
 *
 * Let you save and restore the previous state of an object without revealing the details of its implementation.
 */

/**
 * The Originator holds some important state that may change over time.
 * It also defines a method for saving the state inside a memento and
 * another method for restoring the state from it.
 */
class Originator {
  private state: string;

  constructor(state: string) {
    this.state = state;
  }

  updateState(payload: string) {
    this.state = payload;
  }

  save(): Memento {
    return new ConcreteMemento(this.state);
  }

  restore(memento: Memento) {
    this.state = memento.getState();
  }
}

/**
 * The Memento interface provides a way to retrieve the memento's metadata.
 * However it doesn't expose the Originator's state.
 */
interface Memento {
  getState(): string;
}

/**
 * The concrete Memento contains the infrastructure for storing Originator's state
 */
class ConcreteMemento implements Memento {
  private state: string;

  constructor(state: string) {
    this.state = state;
  }

  public getState(): string {
    return this.state;
  }
}

/**
 * The CareTaker doesn't depend on the Concrete Memento class. Therefor it doesn't have access
 * to the originator's state, store inside the memento. It works with all mementos via the base Memento interface.
 */
class CareTaker {
  private mementos: Memento[];
  private originator: Originator;

  constructor(originator: Originator) {
    this.originator = originator;
  }

  public backup() {
    this.mementos.push(this.originator.save());
  }

  public undo() {
    if (!this.mementos.length) return;

    const memento = this.mementos.pop();
    this.originator.restore(memento!);
  }
}

// client code
const originator = new Originator('memento');
const careTaker = new CareTaker(originator);

careTaker.backup();

export {};

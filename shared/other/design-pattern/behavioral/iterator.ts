/**
 * Iterator Pattern
 *
 * Iterator lets you traverse elements of a collection without exposing its underlying representation.
 */

interface CustomIterator<T> {
  // Return the current element
  current(): T;

  // Return the current element and move forward to next element
  next(): T;

  // Return the key of current element
  key(): number;

  // Checks if current position is valid
  valid(): boolean;

  // Rewind the Iterator to the first element
  rewind(): void;
}

interface Aggregator<T> {
  // Retrieve an external iterator.
  getIterator(): CustomIterator<T>;
}

/**
 * Concrete Iterators implement various traversal algorithms. These classes
 * store the current traversal position at all times.
 */
class AlphabeticalOrderIterator implements CustomIterator<string> {
  private collection: WordsCollection;

  /**
   * Stores the current traversal position. An iterator may have a lot of other fields for storing iteration state,
   * especially when it is supposed to work with particular kind of collection
   */
  private position = 0;

  // This variable indicates the traversal direction
  private reverse = false;

  constructor(collection: WordsCollection, reverse = false) {
    this.collection = collection;
    this.reverse = reverse;

    if (reverse) {
      this.position = collection.getCount() - 1;
    }
  }

  public rewind() {
    this.position = this.reverse ? this.collection.getCount() - 1 : 0;
  }

  public current(): string {
    return this.collection.getItems()[this.position];
  }

  public key(): number {
    return this.position;
  }

  public next(): string {
    const currentItem = this.current();
    this.position += this.reverse ? -1 : 1;
    return currentItem;
  }

  public valid(): boolean {
    return this.reverse
      ? this.position >= 0
      : this.position < this.collection.getCount();
  }
}

/**
 * Concrete Collections provide one or several methods for retrieving fresh
 * iterator instance, compatible with the collection class.
 */
class WordsCollection implements Aggregator<string> {
  private items: string[] = [];

  public getItems(): string[] {
    return this.items;
  }

  public getCount(): number {
    return this.items.length;
  }

  public addItem(item: string) {
    this.items.push(item);
  }

  public getIterator(): CustomIterator<string> {
    return new AlphabeticalOrderIterator(this);
  }

  public getReverseIterator(): CustomIterator<string> {
    return new AlphabeticalOrderIterator(this, true);
  }
}

const collection = new WordsCollection();
collection.addItem('Apple');
collection.addItem('Mars');
collection.addItem('Light');

const iterator = collection.getIterator();
while (iterator.valid()) {
  console.log('iterate=========================>', iterator.next());
}

export {};

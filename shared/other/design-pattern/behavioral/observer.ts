/**
 * Observer Pattern
 *
 * Observer lets you define a subscription mechanism to notify multiple objects about any events
 * that happen to the object they're observing.
 */

/**
 * The Subject interface declares a set of methods for managing subscribers.
 */
interface Subject {
  // Attach an observer from the subject
  attach(observer: Observer): void;

  // Detach an observer from the subject
  detach(observer: Observer): void;

  // Notify all observers about an event
  notify(): void;
}

/**
 * The Subject owns some important state and notifies observers when the state changes.
 */
class ConcreteSubject implements Subject {
  /**
   * @type {number} For the sake of simplicity, the Subject's state, essential to all subscribers,
   * is stored in this variable.
   */
  private state: number;

  private observers: Observer[] = [];

  public attach(observer: Observer): void {
    const isExist = this.observers.includes(observer);
    if (isExist) return;

    this.observers.push(observer);
  }

  public detach(observer: Observer): void {
    const index = this.observers.indexOf(observer);
    if (index === -1) return;

    this.observers.splice(index, 1);
  }

  public notify(): void {
    for (const observer of this.observers) {
      observer.update(this);
    }
  }

  public updateState(state: number) {
    this.state = state;

    this.notify();
  }

  public getState() {
    return this.state;
  }
}

/**
 * The Observer interface declares the update method, used by subjects.
 */
interface Observer {
  update(subject: Subject): void;
}

/**
 * Concrete Observers react to the update issued by Subject they had been attached to.
 */
class ConcreteObserver implements Observer {
  public update(subject: Subject) {
    if (subject instanceof ConcreteSubject && subject.getState() === 1) {
      console.log('ConcreteObserver update=========================>');
    }
  }
}
class ConcreteObserverB implements Observer {
  public update(subject: Subject) {
    if (subject instanceof ConcreteSubject && subject.getState() === 2) {
      console.log('ConcreteObserverB update=========================>');
    }
  }
}

const subject = new ConcreteSubject();
subject.attach(new ConcreteObserver());
subject.attach(new ConcreteObserverB());

subject.updateState(1);
subject.updateState(2);

export {};

/**
 * State Pattern
 *
 * It lets an object alter its behavior when its internal state changes.
 * It appears as if the object change its class.
 */

/**
 * The context defines the interface of interest to client. It also maintains a reference
 * to an instance of a State subclass, which represent the current state of the context.
 */
class StateContext {
  /**
   * @type {state} A reference to the current state of the context.
   */
  private state: State;

  constructor(state: State) {
    this.state = state;
  }

  // The context allows changing the State object at runtime
  public transitionTo(state: State) {
    this.state = state;
    this.state.setContext(this);
  }

  public request(): void {
    this.state.handle();
  }
}

/**
 * The base State class declares methods that all Concrete State should implement
 * and also provides a back reference to the context object, associated with the state.
 * The context object can be used to transition the context to another state.
 */
abstract class State {
  protected context: StateContext;

  public setContext(context: StateContext) {
    this.context = context;
  }

  public abstract handle(): void;
}

/**
 * Concrete States implement various behaviors, associated with a state of the Context.
 */
class ConcreteStateA extends State {
  public handle(): void {
    console.log('ConcreteStateA=========================>');
    this.context.transitionTo(new ConcreteStateB());
  }
}

class ConcreteStateB extends State {
  public handle(): void {
    console.log('ConcreteStateB=========================>');
  }
}

// client code
const stateContext = new StateContext(new ConcreteStateA());
stateContext.request();

export {};

/**
 * Strategy Pattern
 *
 * It turns a set of behaviors into objects and makes them interchangeable inside original context object.
 */

/**
 * The context defines the interface of interest to clients
 */
class StrategyContext {
  /**
   * @type {Strategy} The Context maintains  a reference to one of the Strategy object.
   * The Context does not know the concrete class of a strategy. It should work with all
   * strategies via the Strategy interface.
   */
  private strategy: Strategy;

  /**
   * Usually, the Context accepts a strategy through the constructor, but also provides
   * a setter to change it a runtime.
   */
  constructor(strategy: Strategy) {
    this.strategy = strategy;
  }

  public setStrategy(strategy: Strategy) {
    this.strategy = strategy;
  }

  public request() {
    this.strategy.handle();
  }
}

/**
 * The Strategy interface declares operations common to all supported versions of some algorithm.
 */
interface Strategy {
  handle(): void;
}

/**
 * Concrete strategies implement the algorithm while following the base Strategy interface.
 * The interface makes them interchangeable in the Context.
 */
class ConcreteStrategy implements Strategy {
  handle() {
    console.log('ConcreteStrategy handle=========================>');
  }
}

class ConcreteStrategyB implements Strategy {
  handle() {
    console.log('ConcreteStrategy handle=========================>');
  }
}

// client code
const strategyContext = new StrategyContext(new ConcreteStrategy());
strategyContext.request();

strategyContext.setStrategy(new ConcreteStrategyB());
strategyContext.request();

export {};

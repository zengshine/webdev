/**
 * Template Methods
 *
 * This pattern defines the skeleton of an algorithm in the superclass
 * but let the subclass override specific steps of the algorithm without changing its structure
 */

/**
 * The Abstract Class defines a template method that contains a skeleton of some algorithm,
 * composed of calls to (usually) abstract primitive operation.
 *
 * Concrete subclass should implement these operations, but leave the template method itself intact.
 */
abstract class AbstractTemplate {
  // The templateMethod define the skeleton of an algorithm
  public templateMethod(): void {
    this.beforeHook();
    this.baseOperation();
    this.requiredOperation();
    this.middleOperation();
    this.finalOperation();
    this.afterHook();
  }

  /**
   * implemented operation
   */
  protected baseOperation() {
    console.log('baseOperation=========================>');
  }

  protected middleOperation() {
    console.log('middleOperation=========================>');
  }

  /**
   * operations need to be implemented in subclass
   */
  protected abstract requiredOperation(): void;
  protected abstract finalOperation(): void;

  /**
   * These are 'hooks'. Subclass may override them, but it's not mandatory since
   * the hook already have default(but empty) implementation. Hooks provide additional extension
   * in some crucial places of the algorithm.
   */
  protected beforeHook() {
    console.log('beforeHook=========================>');
  }
  protected afterHook() {
    console.log('afterHook=========================>');
  }
}

/**
 * Concrete classes have to implement all abstract operation of the base class.
 * The can also override some operations with a default implementation.
 */
class ConcreteClass extends AbstractTemplate {
  protected requiredOperation(): void {
    console.log('ConcreteClass requiredOperation=========================>');
  }

  protected finalOperation(): void {
    console.log('ConcreteClass finalOperation=========================>');
  }

  protected afterHook(): void {
    console.log('ConcreteClass afterHook =========================>');
  }
}

// client code
const concreteClass = new ConcreteClass();
concreteClass.templateMethod();

export {};

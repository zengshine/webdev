import * as dotenv from 'dotenv';

const { STAGE } = process.env;
const suffix = STAGE ? (STAGE === 'prod' ? '' : `.${STAGE}`) : '';
const { parsed } = dotenv.config({
  path: `${process.cwd()}/.env${suffix}`
});

export default parsed;

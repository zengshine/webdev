import prismaClient from '@root/prisma/client';

export default {
  ...prismaClient
};

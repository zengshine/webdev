import { Application } from 'egg';
import { PrismaClient } from '@prisma/client';

declare module 'egg' {
  interface Application {
    $prisma: PrismaClient;
  }
}

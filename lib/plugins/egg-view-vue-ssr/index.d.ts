import { Application } from 'egg';

import RenderEngine from '../../lib/engine';

declare module 'egg' {
  interface Application {
    vuessr: RenderEngine;
  }
}

import type { Context } from 'egg';

export default {
  renderClient(name, locals, options) {
    const ctx = this as unknown as Context;
    return ctx.app.vuessr
      .renderClient(name, locals, options)
      .then((html) => (ctx.body = html));
  }
};

import type { Application } from 'egg';
import RenderEngine from '@root/lib/plugins/egg-view-vue-ssr/lib/engine';

const VUE_ENGINE = Symbol('Application#vue');

const app = {
  get vuessr() {
    if (!this[VUE_ENGINE]) {
      this[VUE_ENGINE] = new RenderEngine(this as unknown as Application);
    }
    return this[VUE_ENGINE];
  }
};

export default app;

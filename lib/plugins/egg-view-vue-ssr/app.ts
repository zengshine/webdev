import type { Application } from 'egg';
import type { ViewBase } from 'egg-view';

import ViewEngin from './lib/view';

import { VIEW_ENGINE_NAME } from './const';

export default class VueSSR {
  constructor(app: Application) {
    app.view.use(VIEW_ENGINE_NAME, ViewEngin as unknown as ViewBase);

    if (app.config.env === 'local') {
      app.view.resolve = (name) => Promise.resolve(name);
    }
  }
}

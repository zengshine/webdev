import type { EggAppConfig, PowerPartial } from 'egg';

import { VIEW_ENGINE_NAME } from '../const';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};

  config.view = {
    mapping: {
      '.js': VIEW_ENGINE_NAME
    }
  };

  config.vuessr = {
    renderOptions: {}
  };

  return config;
};

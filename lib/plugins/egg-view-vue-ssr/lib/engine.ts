import fs from 'fs';
import path from 'path';

import 'module-alias/register';
import Resource from 'server-side-render-resource';
import devalue from '@nuxt/devalue';

import type { Application } from 'egg';

import { getSSRHeadConfig } from '@root/shared/plugins/useHead';
import { createBundle } from '@root/lib/bundle-runner';
import { renderToString } from '@vue/server-renderer';
import { isEmptyObject } from '@root/shared/utils';
import { mergeArray } from '@root/shared/utils/array';
import { is } from '@root/shared/utils/boolean';

const getCheerIo = () => import('cheerio').then((r) => r.load);

const VUE_RESOURCE = Symbol('Application#VueResource');

class RenderEngin {
  app: any;
  config: any;
  basedir: string;

  constructor(app: Application) {
    this.app = app;
    this.config = app.config.vuessr;
    this.basedir = this.config.baseDir;
  }

  get resource() {
    if (!this[VUE_RESOURCE]) {
      if (fs.existsSync(this.config.manifest)) {
        this[VUE_RESOURCE] = new Resource(this.app, this.config);
      }
    }

    return this[VUE_RESOURCE];
  }

  getTemplate(options) {
    const filePath = options.layout;
    if (filePath) {
      const content = fs.readFileSync(filePath, 'utf8');
      return content;
    }

    return options.renderOptions && options.renderOptions.template;
  }

  getAssets(filename) {
    const manifest = this.resource?.manifest || {};
    const deps = manifest.deps || {};
    const res = deps[filename] || {};
    const { js = [], css = [] } = res;

    return {
      js,
      css
    };
  }

  getRenderAssets(filename) {
    const entryAssets = this.getAssets(filename);
    const swAssets = this.getAssets('sw.js'); // TODO: delete
    const swRuntime = swAssets.js.filter((item) => item.includes('runtime'));
    entryAssets.js = mergeArray(entryAssets.js, swRuntime);
    return entryAssets;
  }

  async getFile(filename) {
    const { app } = this;
    if (app.config.env === 'local') {
      const filePath = path.join(this.config.baseDir, filename);
      return await app.webpack.fileSystem.readWebpackMemoryFile(
        filePath,
        filename
      );
    }

    return filename;
  }

  async renderHtml({ filename, content, store, ssrContext }) {
    const pageTitle = 'WebDev';
    const renderAssets = this.getRenderAssets(filename);
    const { headTags } = await getSSRHeadConfig();

    const scriptLinks = renderAssets.js.reduce((pre, cur) => {
      return `${pre}<script src="${cur}"></script>`;
    }, '');

    const styleLinks = renderAssets.css.reduce((pre, cur) => {
      return `${pre}<link rel="stylesheet" href="${cur}"></link>`;
    }, '');

    const storeTemplate = `<script>window.__INITIAL_STATE__=${store}</script>`;
    const serviceWorkerTemplate = is(process.env.WORKBOX_SERVICE_WORKER_ENABLE)
      ? `<script>
          // Don't register the service worker until the page has fully loaded
          window.addEventListener('load', () => {
            // Is service worker available?
            if ('serviceWorker' in navigator) {
              navigator.serviceWorker.register('/service-worker.js').then(() => {
                console.log('Service worker registered!');
              }).catch((error) => {
                console.warn('Error registering service worker:');
                console.warn(error);
              });
            }
          });
        </script>`
      : '';

    const injectTeleportTemplate = async (html) => {
      const $ = await getCheerIo().then((load) => load(html));
      const { teleports = {} } = ssrContext;
      for (const key in teleports) {
        const value = teleports[key];
        $(key).prepend(value);
      }
      return $.html();
    };

    let html = `<!DOCTYPE html>
                <html lang="en" style="opacity:0;">
                  <head>
                    <title>${pageTitle}</title>
                    ${styleLinks}
                    ${storeTemplate}
                    ${serviceWorkerTemplate}
                    ${headTags}
                  </head>
                  <body>
                    <div id="app">${content}</div>
                    ${scriptLinks}
                  </body>
                </html>
                `.trim();

    if (!isEmptyObject(ssrContext.teleports))
      html = await injectTeleportTemplate(html);

    return html;
  }

  renderString(...args) {
    console.log('renderString=========================>', args);
  }

  async renderClient(filename, locals, options) {
    const { app, config } = this;
    const { baseDir, requireCache } = config;
    const { url } = options.locals;
    const { name } = options;
    const file = await this.getFile(filename);
    const { evaluateEntry, rewriteErrorTrace } = createBundle(file, {
      runInNewContext: 'once',
      basedir: baseDir,
      requireCache
    });

    const renderContent = await (async (
      evalContext: Record<string, unknown>
    ) => {
      try {
        const ssrContext = {};
        const ctx = app.createAnonymousContext();
        const createSSRVueInstance = await evaluateEntry(evalContext);
        const { app: vueInstance, store } = await createSSRVueInstance({
          ctx,
          url
        });
        const renderContent = await renderToString(vueInstance, ssrContext);
        const storeValue = devalue(store.state.value);

        return this.renderHtml({
          filename: name,
          ssrContext,
          content: renderContent,
          store: storeValue
        });
      } catch (err: any) {
        rewriteErrorTrace(err);
        throw err;
      }
    })({});
    return renderContent;
  }
}

export default RenderEngin;

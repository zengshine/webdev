import type { Context } from 'egg';
import type { RenderOptions } from 'egg-view';

class ViewEngine {
  ctx;
  app;
  config;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.app = ctx.app;
    this.config = this.app.config.vuessr;
  }
  /**
   * Render a file by view engine, then set to body
   * @param {String} name - the file path based on root
   * @param {Object} [locals] - data used by template
   * @param {Object} [options] - view options, you can use `options.viewEngine` to specify view engine
   * @return {Promise<String>} result - return a promise with a render result
   */
  render(name: string, locals?: any, options?: RenderOptions): Promise<string> {
    const ctx = this as unknown as Context;
    return ctx.app.vuessr.renderClient(name, locals, options);
  }

  /**
   * Render a file by view engine and return it
   * @param {String} name - the file path based on root
   * @param {Object} [locals] - data used by template
   * @param {Object} [options] - view options, you can use `options.viewEngine` to specify view engine
   * @return {Promise<String>} result - return a promise with a render result
   */
  renderView(
    name: string,
    locals?: any,
    options?: RenderOptions
  ): Promise<string> {
    console.log('=========================>', name, locals, options);
    return Promise.resolve('renderView');
  }

  /**
   * Render a template string by view engine
   * @param {String} name - template string
   * @param {Object} [locals] - data used by template
   * @param {Object} [options] - view options, you can use `options.viewEngine` to specify view engine
   * @return {Promise<String>} result - return a promise with a render result
   */
  renderString(
    name: string,
    locals?: any,
    options?: RenderOptions
  ): Promise<string> {
    return this.app.vuessr.renderString(name, locals, options);
  }
}

export default ViewEngine;

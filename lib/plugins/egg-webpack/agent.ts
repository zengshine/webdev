import type { Agent, IBoot } from 'egg';
import WebpackServer from '@root/lib/plugins/egg-webpack/lib/server';
import MultiProcessWebpackServer from '@root/lib/plugins/egg-webpack/lib/multi-process-server';

import {
  getPort,
  isUseMultiProcess
} from '@root/lib/plugins/egg-webpack/lib/utils';
import { transformToArray } from '@root/shared/utils/array';

export default class RootApp implements IBoot {
  constructor(agent: Agent) {
    agent.messenger.on('egg-ready', () => {
      const switchOff = false;
      if (switchOff) return;
      const config = agent.config.webpack;
      agent.messenger.setMaxListeners(config.maxListeners || 10000);
      // 兼容 Node 前端渲染只有一个 webpack 配置
      config.webpackConfigList = transformToArray(config.webpackConfigList);
      // webpack-tool not need proxy again
      const pluginConfig = Object.assign({}, config, { proxy: false });
      if (isUseMultiProcess(agent.baseDir, config)) {
        new MultiProcessWebpackServer(agent, pluginConfig).start();
      } else {
        const port = getPort(config.port);
        pluginConfig.port = port;
        new WebpackServer(agent, pluginConfig).start();
      }
    });

    process.on('exit' as any, () => {
      process.exit(0);
    });
  }
}

import path from 'path';
import fs from 'fs';
import { openBrowser } from '@root/lib/build/webpack-tool/lib/utils';
import type { Application, IBoot } from 'egg';

import proxy from '@root/lib/plugins/egg-webpack/lib/proxy';
import {
  EVENT_NAME,
  MAX_LISTENER
} from '@root/lib/plugins/egg-webpack/constant';

function setProxyMiddleware(app: Application, config: any) {
  // 解决 proxy middleware 插入需要在静态资源和自定义中间件前面
  let proxyIndex = -1;
  const mwNames = [
    'static',
    'bodyParser',
    'overrideMethod',
    'session',
    'securities',
    'notfound',
    'siteFile',
    'meta'
  ];
  while (mwNames.length) {
    const name = mwNames.shift();
    proxyIndex = app.middleware.findIndex((mw: any) => {
      return mw._name === name; // TODO: using mw.name will fail the hmr(the middleware order is important?)
    });
    if (proxyIndex > -1) {
      break;
    }
  }
  app.middleware.splice(proxyIndex, 0, proxy(config.proxy));
}

function loadingMiddlewareWrapper(app) {
  return async (ctx, next) => {
    if (app.WEBPACK_BUILD_READY) {
      await next();
    } else if (app.WEBPACK_LOADING_TEXT) {
      ctx.body = app.WEBPACK_LOADING_TEXT;
    } else {
      const filePath = path.resolve(__dirname, './template/loading.html');
      ctx.body = app.WEBPACK_LOADING_TEXT = fs.readFileSync(filePath, 'utf8');
    }
  };
}

export default class RootApp implements IBoot {
  private app: Application;
  constructor(app: Application) {
    this.app = app;

    app.use(loadingMiddlewareWrapper(app));

    app.setMaxListeners(app.config.webpack.maxListeners || MAX_LISTENER);

    app.messenger.on(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, (data) => {
      app.WEBPACK_BUILD_READY = data.state;
      const config = app.config.webpack;
      const port = data.port || config.port;

      if (app.WEBPACK_BUILD_PROXY || !config.proxy) return;

      app.WEBPACK_BUILD_PROXY = true;
      const proxyHost = `http://127.0.0.1:${port}`;
      if (typeof config.proxy === 'boolean') {
        config.proxy = {
          host: proxyHost,
          match: /^\/public\//
        };
      } else if (config.proxy.force !== true) {
        config.proxy.host = proxyHost;
      }

      setProxyMiddleware(this.app, config);
    });

    app.messenger.on(EVENT_NAME.EVENT_WEBPACK_OPEN_BROWSER, () => {
      const browser = app.config.webpack.browser;
      if (browser === false) return;
      const { ENV_EGG_SERVER_PORT } = process.env;
      if (/^https?/.test(browser)) {
        openBrowser(ENV_EGG_SERVER_PORT, browser);
      } else if (browser === undefined || browser === true) {
        openBrowser(ENV_EGG_SERVER_PORT, '');
      }
    });
  }

  async didReady(): Promise<void> {
    this.app.messenger.sendToAgent(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, null);
  }
}

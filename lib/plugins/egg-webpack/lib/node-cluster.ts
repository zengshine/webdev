'use strict';

import cluster from 'cluster';
import { EventEmitter } from 'events';

class NodeCluster {
  running;
  emitter;
  runUntil;
  task;
  constructor() {
    this.running = true;
    this.emitter = new EventEmitter();
    this.runUntil = Date.now() + 1000;
    this.task = null;
    this.start();
  }

  start() {
    this.listen();
  }

  listen() {
    this.emitter.once('shutdown', this.shutdown);
    cluster.on('exit', this.restart);
    process.on('SIGINT' as any, this.signal).on('SIGTERM' as any, this.signal);
  }

  shutdown() {
    this.running = false;
    for (const id in cluster.workers) {
      cluster.workers[id].process.kill();
    }
    setTimeout(this.kill, 1000).unref();
  }

  signal() {
    this.emitter.emit('shutdown');
  }

  restart() {
    if (this.running && Date.now() < this.runUntil) {
      cluster.fork();
    }
  }

  kill() {
    for (const id in cluster.workers) {
      cluster.workers[id].kill();
    }
    process.exit();
  }
}

exports.run = (task) => {
  if (cluster.isWorker) {
    console.log('NodeCluster isWorker');
    task(cluster.worker.id);
  } else {
    console.log('NodeCluster isMaster');
    new NodeCluster();
    cluster.fork();
  }
};

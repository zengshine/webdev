import WebpackTool from '@root/lib/build/webpack-tool';
import type { Agent } from 'egg';

import { buildDll } from '@root/lib/plugins/egg-webpack/lib/build-dll';
import {
  getPort,
  getWebpackConfig,
  readWebpackMemoryFile
} from '@root/lib/plugins/egg-webpack/lib/utils';
import { transformToArray } from '@root/shared/utils/array';

import { EVENT_NAME } from '@root/lib/plugins/egg-webpack/constant';

export default class WebpackServer extends WebpackTool {
  private agent: Agent;
  private port: number;
  // private endTime: number;
  private webpackConfigList;

  constructor(agent, config) {
    super(config);

    this.agent = agent;
    this.port = getPort(config.port);
    const webpackConfigList = transformToArray(this.config.webpackConfigList);
    if (webpackConfigList.length) {
      this.webpackConfigList = webpackConfigList;
    } else {
      this.webpackConfigList = getWebpackConfig(config, {
        baseDir: agent.baseDir || process.cwd()
      });
    }
  }

  start() {
    const { config, agent, webpackConfigList } = this;
    buildDll(
      config,
      {
        baseDir: agent.baseDir
      },
      () => {
        const compilers = this.dev(webpackConfigList, {}, () => {
          this.finish();
        });
        this.listen(compilers);
      }
    );
  }

  finish() {
    const { agent, port } = this;
    agent.messenger.sendToApp(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, {
      state: true,
      port
    });
    // this.endTime = Date.now();
    // console.log(`\n webpack build cost:${this.endTime - this.startTime}ms`);
    this.openBrowser();
  }

  listen(compilers) {
    const { agent, port } = this;
    agent.messenger.on(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, () => {
      agent.messenger.sendToApp(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, {
        state: this.ready,
        port
      });
    });

    agent.messenger.on(EVENT_NAME.EVENT_WEBPACK_READ_FILE_MEMORY, (data) => {
      const { filePath, target } = data;
      const compiler = compilers.find((item) => item.options.target === target);
      if (!compiler) {
        console.log('compiler not found====================');
        return;
      }

      const fileContent = readWebpackMemoryFile(compilers, filePath);
      agent.messenger.sendToApp(
        EVENT_NAME.EVENT_WEBPACK_READ_FILE_MEMORY_CONTENT,
        {
          fileContent,
          filePath
        }
      );
    });
  }

  openBrowser() {
    if (this.opened) return;

    this.opened = true;
    this.agent.messenger.sendToApp(EVENT_NAME.EVENT_WEBPACK_OPEN_BROWSER, null);
  }
}

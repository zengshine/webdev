import FileSystem from '@root/lib/plugins/egg-webpack/lib/filesystem';

interface Engine {
  fileSystem: FileSystem;
}

export default function (app): Engine {
  return {
    fileSystem: new FileSystem(app)
  };
}

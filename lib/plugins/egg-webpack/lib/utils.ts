import fs from 'fs';
import path from 'path';

import {} from '@root/lib/build/webpack-tool';

import { transformToArray } from '@root/shared/utils/array';

function getFramework() {
  return 'vue';
}

function requireModule(moduleName, baseDir) {
  try {
    const modulePath = path.join(baseDir, 'node_modules', moduleName);
    return require(modulePath);
  } catch (e) {
    return require(moduleName);
  }
}

function requireEasyModule(moduleName, basedir) {
  const PKG_PREFIX = '@easy-team';
  const name = `${PKG_PREFIX}/${moduleName}`;
  return requireModule(name, basedir);
}

function requireEasyCompatibleModule(moduleName, baseDir) {
  try {
    return requireModule(moduleName, baseDir);
  } catch (e) {
    return requireEasyModule(moduleName, baseDir);
  }
}

function getPkgInfo(baseDir = '') {
  const pkgFile = path.join(baseDir || process.cwd(), 'package.json');
  if (fs.existsSync(pkgFile)) {
    return require(pkgFile);
  }
  return {};
}

export function getPort(port) {
  const pkgInfo = getPkgInfo();
  const name = pkgInfo.name || 'webpack-project';
  const EASY_ENV_DEV_PORT = `EASY_ENV_DEV_PORT_${name}`;
  return Number(process.env[EASY_ENV_DEV_PORT] || port);
}

export function getWebpackConfigJSON(baseDir, pluginConfig) {
  const filename = pluginConfig.webpackConfigFile || 'webpack.config.js';
  const webpackConfigFile = path.isAbsolute(filename)
    ? filename
    : path.join(baseDir, filename);
  if (fs.existsSync(webpackConfigFile)) {
    return require(webpackConfigFile);
  }

  const pkgFile = path.join(baseDir, 'package.json');
  if (fs.existsSync(pkgFile)) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const pkg = require(pkgFile);
    return pkg.webpack || {};
  }

  return {};
}

export function getWebpackConfig(eggWebpackConfig, option) {
  const { baseDir } = option;
  const framework = getFramework();
  option.framework = framework;

  const module = framework ? `easywebpack-${framework}` : 'easywebpack';
  const webpackConfigFile = eggWebpackConfig.webpackConfigFile;
  const easywebpack = requireEasyCompatibleModule(module, baseDir);
  return easywebpack.getWebpackConfig(webpackConfigFile, option);
}

export function isUseMultiProcess(baseDir, pluginConfig) {
  if (pluginConfig && pluginConfig.webpackConfigList.length) {
    return false;
  }

  const cliConfig = exports.getWebpackConfigJSON(baseDir, pluginConfig);
  if (cliConfig) {
    if (cliConfig.type === undefined && cliConfig.target === undefined) {
      return true;
    }
    if (Array.isArray(cliConfig.type) && cliConfig.type.length > 1) {
      return true;
    }

    if (Array.isArray(cliConfig.target) && cliConfig.target.length > 1) {
      return true;
    }
  }
  return false;
}

export function readWebpackMemoryFile(compiler, filePath) {
  const compilerList = transformToArray(compiler);
  for (let i = 0; i < compilerList.length; i++) {
    const compilerItem = compilerList[i];
    const compilers = compilerItem.compilers
      ? compilerItem.compilers
      : [compilerItem];
    const fileCompiler = compilers?.filter((item) => {
      return item.outputFileSystem.existsSync(filePath);
    });

    if (fileCompiler && fileCompiler.length) {
      const IMG_REGEX = /\.(png|jpe?g|gif|svg)(\?.*)?$/;
      const ext = path.extname(filePath).toLocaleLowerCase();
      if (IMG_REGEX.test(ext)) {
        const base64 = fileCompiler[0].outputFileSystem
          .readFileSync(filePath)
          .toString('base64');
        return `data:image/${ext.replace(/^\./, '')};base64,${base64}`;
      }

      return fileCompiler[0].outputFileSystem
        .readFileSync(filePath)
        .toString('utf-8');
    }
  }

  return '';
}

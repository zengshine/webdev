import WebpackTool from '@root/lib/build/webpack-tool/index';

import {
  getWebpackConfig,
  getWebpackConfigJSON
} from '@root/lib/plugins/egg-webpack/lib/utils';

export function buildDll(pluginConfig, option, callback) {
  const { baseDir } = option;
  const cliConfig = getWebpackConfigJSON(baseDir, pluginConfig);
  if (cliConfig.dll) {
    const webpackTool = new WebpackTool(pluginConfig);
    const webpackConfig = getWebpackConfig(pluginConfig, {
      onlyDll: true,
      baseDir
    });
    if (!webpackConfig) {
      callback();
      return;
    }
    webpackTool.build(webpackConfig, {}, callback);
  } else {
    callback();
  }
}

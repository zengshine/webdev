import { EVENT_NAME } from '@root/lib/plugins/egg-webpack/constant';

export default class FileSystem {
  app;
  constructor(app) {
    this.app = app;
  }

  readWebpackMemoryFile(filePath, fileName, target = 'node') {
    const { app } = this;
    return new Promise((resolve) => {
      app.messenger.sendToAgent(EVENT_NAME.EVENT_WEBPACK_READ_FILE_MEMORY, {
        filePath,
        fileName,
        target
      });

      app.messenger.once(
        EVENT_NAME.EVENT_WEBPACK_READ_FILE_MEMORY_CONTENT,
        (data) => {
          if (filePath === data.filePath) {
            resolve(data.fileContent);
          }
        }
      );
    });
  }
}

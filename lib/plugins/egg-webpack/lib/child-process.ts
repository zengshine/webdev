// import 'module-alias/register';
// import '@root/env';

import WebpackTool from '@root/lib/build/webpack-tool';

import {
  getPort,
  readWebpackMemoryFile
} from '@root/lib/plugins/egg-webpack/lib/utils';

let compiler;

process.on('message' as any, async (m: any) => {
  if (m.action === 'building') {
    const webpackConfigList = await import('@root/webpack.config');
    const target = m.target;
    const webpackConfig = webpackConfigList.default.find(
      (item) => item.target === target
    );
    const webPort = getPort(m.port);
    const nodePort = webPort + 1;
    const port = target === 'web' ? webPort : nodePort;
    const webpackTool = new WebpackTool({ port });
    compiler = webpackTool.createWebpackCompiler(webpackConfig, () => {
      if (process.send)
        process.send({
          action: 'done',
          port: webPort
        });
    });
    webpackTool.createWebpackServer(compiler);
  } else if (m.action === 'file') {
    const { filePath } = m;
    const fileContent = readWebpackMemoryFile([compiler], filePath);
    if (process.send)
      process.send({
        action: 'file',
        fileContent,
        filePath
      });
  }
});

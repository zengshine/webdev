import path from 'path';
import child_process from 'child_process';

import { EVENT_NAME } from '@root/lib/plugins/egg-webpack/constant';

import { buildDll } from '@root/lib/plugins/egg-webpack/lib/build-dll';

export default class WebpackServer {
  agent;
  config;
  opened: boolean;
  state: boolean;
  compilerCount: number;
  compileDoneCount: number;
  startTime: number;
  debugPort: number;
  port: number;
  constructor(agent, config) {
    Object.assign(this, {
      agent,
      config,
      compilerCount: 2,
      compileDoneCount: 0,
      startTime: Date.now(),
      debugPort: config.debugPort || 5900,
      port: config.port
    });
  }

  start() {
    const { config, agent } = this;
    const { port } = config;
    // build dll first
    buildDll(config, { baseDir: agent.baseDir }, () => {
      this.process('web', port);
    });
    this.process('node', port + 1);
    this.listen();
  }

  process(target, port) {
    const { agent, config } = this;
    const execArgv = agent.options?.isDebug
      ? process.execArgv.concat([`--debug-port=${this.debugPort++}`])
      : process.execArgv;
    const child = child_process.fork(
      path.join(__dirname, 'child-process'),
      [],
      { execArgv }
    );
    child.on('message', (m) => {
      if (m.action === 'done') {
        this.compileDoneCount++;
        this.state =
          this.compileDoneCount > 0 &&
          this.compileDoneCount % this.compilerCount === 0;
        this.port = m.port;
        this.sendToApp();
        this.openBrowser();
      } else if (m.action === 'file') {
        const { fileContent, filePath } = m;
        agent.messenger.sendToApp(
          EVENT_NAME.EVENT_WEBPACK_READ_FILE_MEMORY_CONTENT,
          {
            fileContent,
            filePath
          }
        );
      }
    });

    // send message to child_process, trigger building
    child.send({
      target,
      port,
      action: 'building',
      baseDir: agent.baseDir,
      config
    });
    agent.messenger.on(EVENT_NAME.EVENT_WEBPACK_READ_FILE_MEMORY, (data) => {
      if (data.target === target) {
        child.send({ action: 'file', filePath: data.filePath });
      }
    });
    process.on('SIGINT' as any, () => {
      child_process.spawn('kill', [String(child.pid)]);
    });
  }

  listen() {
    this.agent.messenger.on(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, () => {
      this.sendToApp();
    });
  }

  sendToApp() {
    if (this.state) {
      this.agent.messenger.sendToApp(EVENT_NAME.EVENT_WEBPACK_BUILD_STATE, {
        state: this.state,
        port: this.port
      });
    }
  }

  openBrowser() {
    if (!this.opened && this.state) {
      this.opened = true;
      this.agent.messenger.sendToApp(EVENT_NAME.EVENT_WEBPACK_OPEN_BROWSER);
    }
  }
}

import iconv from 'iconv-lite';
import coRequest from 'request';
import url from 'url';

import { getDataType } from '@root/shared/utils';

const join = url.resolve;

// function customResolve (from, to) {
//   const resolvedUrl = new URL(to, new URL(from, 'resolve://'));
//   if (resolvedUrl.protocol === 'resolve:') {
//     // `from` is a relative URL.
//     const { pathname, search, hash } = resolvedUrl;
//     return pathname + search + hash;
//   }
//   return resolvedUrl.toString();
// }

function ignoreQuery(url) {
  return url ? url.split('?')[0] : null;
}

function resolve(path, options) {
  let { url, host, map } = options;
  if (url) {
    if (!/^http/.test(url)) {
      url = host ? join(host, url) : null;
    }
    return ignoreQuery(url);
  }

  if (typeof map === 'object') {
    if (map && map[path]) {
      path = ignoreQuery(path);
    }
  } else if (typeof map === 'function') {
    path = map(path);
  }

  return host ? join(host, path) : null;
}

function getParsedBody(ctx) {
  let body = ctx.request.body;
  if (body === undefined || body === null) {
    return undefined;
  }

  const contentType = ctx.request.header['content-type'];
  if (!Buffer.isBuffer(body) && typeof body !== 'string') {
    if (contentType?.indexOf('json') !== -1) {
      body = JSON.stringify(body);
    } else {
      body = body.toString();
    }
  }

  return body;
}

export default function (options = {} as any) {
  const {
    host,
    map,
    url,
    jar,
    match,
    requestOptions,
    followRedirect,
    suppressRequestHeaders,
    suppressResponseHeaders
  } = options;
  const request = coRequest.defaults({
    jar: jar === true
  });

  if (!(host || map || url)) throw new Error('options required');

  return async function proxy(ctx, next) {
    const { header, path, method, querystring = '' } = ctx;
    const url = resolve(path, options);
    if (getDataType(suppressRequestHeaders) === 'array') {
      suppressRequestHeaders.forEach((val, index) => {
        suppressRequestHeaders[index] = val.toLowerCase();
      });
    }

    const transformedSuppressResponseHeaders: string[] = [];
    if (getDataType(suppressResponseHeaders) === 'array') {
      suppressResponseHeaders.forEach((val) => {
        transformedSuppressResponseHeaders.push(val.toLowerCase());
      });
    }

    if (!url) {
      await next();
      return;
    }

    if (match && !ctx.path.match(match)) {
      await next();
      return;
    }

    const parseBody = getParsedBody(ctx);
    let opt = {
      url: `${url}${querystring ? '?' + querystring : ''}`,
      headers: header,
      encoding: null,
      followRedirect: !(followRedirect === false),
      method,
      body: parseBody,
      proxy: false
    };

    if (host)
      opt.headers.host = host
        .slice(options.host.indexOf('://') + 3)
        .replace(/\/$/, '');
    if (requestOptions) {
      if (getDataType(requestOptions) === 'function') {
        opt = requestOptions(ctx.request, opt);
      } else {
        Object.keys(requestOptions).forEach((key) => {
          opt[key] = requestOptions[key];
        });
      }
    }

    for (const key in opt.headers) {
      if (
        suppressRequestHeaders &&
        suppressRequestHeaders.indexOf(key.toLowerCase()) >= 0
      ) {
        delete opt.headers[key];
      }
    }

    const requestPromise = new Promise((resolve, reject) => {
      request(opt, (error, response) => {
        if (error) reject(error);

        resolve(response);
      });
    });

    const res: any = await requestPromise;

    if (res.statusCode === 404) {
      console.log('proxy 404====================');
      await next();
    } else {
      ctx.status = res.statusCode;
      for (const name in res.headers) {
        if (transformedSuppressResponseHeaders.includes(name.toLowerCase())) {
          continue;
        }

        if (name === 'transfer-encoding') {
          continue;
        }

        ctx.set(name, res.headers[name]);
      }

      if (options.encoding === 'gbk') {
        ctx.body = iconv.decode(res.body, 'gbk');
        return;
      }

      ctx.set('x-served-by', 'custom-egg-webpack');
      ctx.body = res.body;

      if (options.yieldNext) {
        await next();
      }
    }
  };
}

import Engine from '@root/lib/plugins/egg-webpack/lib/engine';
const WEBPACK = Symbol('Application#webpack');

export default {
  get webpack() {
    if (!this[WEBPACK]) {
      this[WEBPACK] = Engine(this);
    }

    return this[WEBPACK];
  }
};

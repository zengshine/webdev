declare module '@root/lib/build/webpack-tool' {
  class WebpackTool {
    static utils: any;
    constructor(config);
    dev: any;
    ready: boolean;
    opened: boolean;
    startTime: number;
    build: any;
    config: any;
    createWebpackServer: any;
    createWebpackCompiler: any;
  }

  export default WebpackTool;
}

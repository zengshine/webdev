import { Application } from 'egg';
import { ApolloServer } from '@apollo/server';

import type { Engine } from '@root/lib/plugins/egg-webpack/lib/engine';

declare module 'egg' {
  interface Application {
    WEBPACK_BUILD_READY: boolean;
    WEBPACK_BUILD_PROXY: boolean;
    WEBPACK_LOADING_TEXT: string;
    webpack: Engine;
  }
}

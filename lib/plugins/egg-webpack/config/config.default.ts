import type { EggAppConfig, PowerPartial } from 'egg';

const { ENV_EGG_WEBPACK_DEV_PORT } = process.env;

export default () => {
  const config = {} as PowerPartial<EggAppConfig>;

  const port = ENV_EGG_WEBPACK_DEV_PORT || 9000;
  const bizConfig = {
    webpack: {
      port,
      proxy: {
        host: `http://127.0.0.1:${port}`, // target host that matched path will be proxy to
        match: /^\/public\// // path pattern.
      },
      webpackConfigList: []
    }
  };

  return {
    ...(config as {}),
    ...bizConfig
  };
};

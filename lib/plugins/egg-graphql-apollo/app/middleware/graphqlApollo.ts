import type { EggAppConfig, Application } from 'egg';
import createApolloMiddleWare from '@root/lib/plugins/egg-graphql-apollo/apollo/middleware';

export default function graphql(
  options: EggAppConfig['graphql-apollo'],
  app: Application
) {
  const apolloServer = app.config.graphqlApollo.server;

  return createApolloMiddleWare(apolloServer, {
    context: async ({ ctx }) => ({ token: ctx.headers.token })
  });
}

import type { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config = {} as PowerPartial<EggAppConfig>;

  // config.middleware = ['graphqlApollo'];

  const bizConfig = {
    graphqlApollo: {
      routePath: '/graphql',
      wsPort: 4000
    }
  };

  return {
    ...(config as {}),
    ...bizConfig
  };
};

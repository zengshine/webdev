import type { Application, IBoot } from 'egg';

import createApolloServer from '@root/lib/plugins/egg-graphql-apollo/apollo/server';

export default class RootApp implements IBoot {
  private app: Application;
  constructor(app: Application) {
    this.app = app;
    app.config.middleware.push('graphqlApollo');
  }

  async configWillLoad(): Promise<void> {
    const apolloServer = await createApolloServer(this.app);
    this.app.$apolloServer = apolloServer;
  }
}

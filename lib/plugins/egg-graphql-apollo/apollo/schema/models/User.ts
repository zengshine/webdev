import { builder } from '@root/lib/plugins/egg-graphql-apollo/apollo/builder';

import prismaClient from '@root/prisma/client';

builder.prismaObject('users', {
  fields: (t) => ({
    id: t.exposeID('id'),
    name: t.exposeString('name', {
      nullable: true
    }),
    update_at: t.expose('update_at' as never, {
      type: 'Date',
      nullable: true
    })
  })
});

builder.queryField('users', (t) => {
  return t.prismaField({
    type: ['users'],
    resolve: async () => {
      return prismaClient.$prisma.users.findMany({});
    }
  });
});

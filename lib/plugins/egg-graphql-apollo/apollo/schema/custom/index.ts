import { makeExecutableSchema } from '@graphql-tools/schema';
import {
  DateScalar,
  VoidScalar
} from '@root/lib/plugins/egg-graphql-apollo/apollo/schema/scalar';
import { withFilter, PubSub } from 'graphql-subscriptions/dist';

const pubsubInstance = new PubSub();

class SubQuery {
  private version: string;
  private queryString: string;
  constructor(version: string) {
    this.version = version;
    this.queryString = `${version} query string`;
  }

  getConfig() {
    const { version, queryString } = this;

    return { version, queryString };
  }
}

class QueryConfig {
  private version: string;
  private subQueryList: SubQuery[];
  constructor(version) {
    this.version = version;
    this.subQueryList = [new SubQuery(version)];
  }

  get queryList() {
    return this.subQueryList;
  }

  reset(version: string) {
    if (this.version !== version) this.version = version;
  }
}

// The GraphQL schema
const typeDefs = `#graphql
  scalar Timestamp
  scalar Void

  input SubQueryInput {
    version: String,
    queryString: String
  }

  type SubQuery {
    version: String,
    queryString: String
  }

  type QueryConfig {
    version: String,
    reset(version: String): Void,
    subQueryList: [SubQuery]
  }

  type Mutation {
    updateQueryString(subQuery: SubQueryInput): SubQuery
  }
  
  type Query {
    hello: String,
    userId: Float,
    queryHistory(startDate: Timestamp!, endDate: Timestamp): [QueryConfig]
  }

  type Subscription {
    getHeartBeat: [String]
  }
`;

// A map of functions which return data for the schema.
const resolvers = {
  Timestamp: DateScalar,
  Void: VoidScalar,

  Query: {
    hello: () => 'world',
    userId: () => Math.random(),
    queryHistory: (root, args) => {
      // root,args,context,info
      pubsubInstance.publish('pubsub-emit-heartbeat', [
        String(Math.random() * 100),
        '2'
      ]);
      const { startDate, endDate } = args;
      return [
        new QueryConfig(String(startDate)),
        new QueryConfig(String(endDate))
      ];
    }
  },

  Mutation: {
    updateQueryString: (root, args) => {
      const { version } = args.subQuery;
      return new SubQuery(version);
    }
  },

  Subscription: {
    getHeartBeat: {
      subscribe: withFilter(
        () => {
          // root,args,context,info
          return pubsubInstance.asyncIterator(['pubsub-emit-heartbeat']);
        },
        (...args) => {
          // payload, variable
          return args.length > 0;
        }
      ),
      resolve: (payload) => payload
    }
  }
};

export const customSchema = makeExecutableSchema({ typeDefs, resolvers });

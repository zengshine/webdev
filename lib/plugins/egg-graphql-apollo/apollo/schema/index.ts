import { stitchSchemas } from '@graphql-tools/stitch';
import { builder } from '@root/lib/plugins/egg-graphql-apollo/apollo/builder';
import { customSchema } from '@root/lib/plugins/egg-graphql-apollo/apollo/schema/custom';

import '@root/lib/plugins/egg-graphql-apollo/apollo/schema/models';

const pothosSchema = builder.toSchema({});
export const schema = stitchSchemas({
  subschemas: [customSchema, pothosSchema]
});

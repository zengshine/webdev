import { GraphQLScalarType, Kind } from 'graphql';

const DateScalar = new GraphQLScalarType({
  name: 'Timestamp',
  description: 'Date custom scalar type',
  serialize(value: unknown) {
    return (value as Date).getTime(); // Convert outgoing Date to integer for JSON
  },
  parseValue(value: unknown) {
    return new Date(value as number); // Convert incoming integer to Date
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      // Convert hard-coded AST string to integer and then to Date
      return new Date(parseInt(ast.value, 10));
    }
    // Invalid hard-coded value (not an integer)
    return null;
  }
});

const VoidScalar = new GraphQLScalarType({
  name: 'Void',
  description: 'Represents Null values',
  serialize() {
    return;
  },
  parseValue() {
    return;
  },
  parseLiteral() {
    return;
  }
});

export { DateScalar, VoidScalar };

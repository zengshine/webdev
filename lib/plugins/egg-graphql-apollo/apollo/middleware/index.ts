import type Koa from 'koa';

import { parse } from 'url';

import type { Context } from 'egg';
import type { WithRequired } from '@apollo/utils.withrequired';
import type {
  ApolloServer,
  BaseContext,
  ContextFunction,
  HTTPGraphQLRequest
} from '@apollo/server';

export interface KoaContextFunctionArgument {
  ctx: Context;
}

type EggMiddleware = (ctx: Context, next: Koa.Next) => any;

interface KoaMiddlewareOptions<TContext extends BaseContext> {
  context?: ContextFunction<[KoaContextFunctionArgument], TContext>;
}

export function koaMiddleware(
  server: ApolloServer<BaseContext>,
  options?: KoaMiddlewareOptions<BaseContext>
): EggMiddleware;
export function koaMiddleware<TContext extends BaseContext>(
  server: ApolloServer<TContext>,
  options: WithRequired<KoaMiddlewareOptions<TContext>, 'context'>
): EggMiddleware;
export default function koaMiddleware<TContext extends BaseContext>(
  apolloServer: ApolloServer<TContext>,
  options?: KoaMiddlewareOptions<TContext>
): EggMiddleware {
  // server.assertStarted('koaMiddleware');
  const defaultContext: ContextFunction<
    [KoaContextFunctionArgument],
    any
  > = async () => ({});
  const context: ContextFunction<[KoaContextFunctionArgument], TContext> =
    options?.context ?? defaultContext;

  /**
   * Middleware Body
   */
  return async (ctx: Context, next) => {
    const server = apolloServer ?? ctx.app.$apolloServer;
    if (!ctx.url.startsWith('/graphql')) {
      await next();
      return;
    }

    if (!ctx.request.body) {
      // The json koa-bodyparser *always* sets ctx.request.body to {} if it's unset
      // (even if the Context-Type doesn't match), so if it isn't set, you probable
      // forgot to set up koa-bodyparser

      ctx.status = 500;
      ctx.body =
        '`ctx.request.body` is not set; this probably means you forgot to set up the ' +
        '`koa-bodyparser` middleware before the Apollo Server middleware.';
      return;
    }

    const headers: any = new Map<string, string>();
    for (const [key, value] of Object.entries(ctx.headers)) {
      if (value === undefined) continue;

      // Node/Koa headers can be an array or a single value. We join
      // multi-valued headers with `, ` just like the Fetch API's `Headers`
      // does. We assume that keys are already lower-cased (as per the Node
      // docs on IncomingMessage.headers) and so we don't bother to lower-case
      // them or combine across multiple keys that would lower-case to the
      // same value.
      headers.set(
        key,
        Array.isArray(value) ? value.join(',') : (value as string)
      );
    }

    const httpGraphQLRequest: HTTPGraphQLRequest = {
      headers,
      method: ctx.method.toUpperCase(),
      search: parse(ctx.url).search ?? '',
      body: ctx.request.body
    };

    try {
      const { body, headers, status } = await server.executeHTTPGraphQLRequest({
        httpGraphQLRequest,
        context: () => context({ ctx })
      });

      for (const [key, value] of headers) {
        ctx.set(key, value);
      }

      ctx.status = status || 200;

      if (body.kind === 'complete') {
        ctx.body = body.string;
        return;
      }
    } catch (ex) {
      console.log('executeHTTPGraphQLRequest=========================>', ex);
      await next();
    }
  };
}

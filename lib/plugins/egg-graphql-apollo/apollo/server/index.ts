import { ApolloServer } from '@apollo/server';
import { createServer } from 'http';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import { schema } from '@root/lib/plugins/egg-graphql-apollo/apollo/schema';

import type { Application } from 'egg';

export default async function createApolloServer(app: Application) {
  const { routePath, wsPort } = app.config.graphqlApollo;
  const { ENV_EGG_SERVER_PORT, ENV_APOLLO_WS_PORT } = process.env;
  const wsServerPort = ENV_APOLLO_WS_PORT || wsPort;
  const httpServer = createServer(app.callback());
  const wsServer = new WebSocketServer({
    server: httpServer,
    path: routePath
  });

  const serverCleanup = useServer({ schema }, wsServer);

  const apolloServer = new ApolloServer({
    schema,
    plugins: [
      // Proper shutdown for the HTTP server.
      ApolloServerPluginDrainHttpServer({ httpServer }),

      // Proper shutdown for the WebSocket server.
      {
        async serverWillStart() {
          return {
            async drainServer() {
              await serverCleanup.dispose();
            }
          };
        }
      }
    ]
  });
  await apolloServer.start();

  httpServer.listen(wsServerPort, () => {
    console.log(
      `🚀 Graphql endpoint ready at http://localhost:${ENV_EGG_SERVER_PORT}${routePath}`
    );
    console.log(
      `🚀 Subscription endpoint ready at ws://localhost:${wsServerPort}${routePath}`
    );
  });

  return apolloServer;
}

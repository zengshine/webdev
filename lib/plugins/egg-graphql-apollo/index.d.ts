import { Application } from 'egg';
import { ApolloServer } from '@apollo/server';

declare module 'egg' {
  interface Application {
    $apolloServer: ApolloServer;
  }
}

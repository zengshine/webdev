'use strict';

import Koa from 'koa';
import cors from 'kcors';
import merge from 'webpack-merge';
import open from 'opn';

import { setEasyInfo } from 'easy-helper';
import { join } from 'path';
import { existsSync } from 'fs';
import { blueBright, green as _green, red as _red } from 'chalk';
import { wrapRequired } from '@root/shared/utils/modules';

import hotMiddleware from './hot';
import devMiddleware from './dev';
import proxyMiddleware from './proxy';
import NavigationPage from './nav';

import {
  getWebpack,
  getCLI,
  normalizeHotEntry as _normalizeHotEntry,
  normalizeURL,
  getPort as _getPort,
  getBrowserUrl,
  getHost
} from './utils';

const webpack = getWebpack();

class WebpackTool {
  private config;
  private ready;
  private cli;
  private baseDir;
  private pkgInfo;
  private devServer;
  private url;

  static webpack = webpack;
  static merge = merge;
  static KoaWebpackHotMiddleware = hotMiddleware;
  static KoaWebpackDevMiddleware = devMiddleware;
  static KoaProxyMiddleware = proxyMiddleware;

  constructor(config) {
    this.config = merge(
      {
        debugPort: 8888,
        hot: false
      },
      config
    );
    this.ready = false;
    this.cli = getCLI(this.config.cli);
    this.baseDir = this.config.baseDir || process.cwd();
    const pkgFile = join(this.baseDir, 'package.json');
    const defaultPkgInfo = { name: 'webpack-project', version: '1.0.0' };
    if (existsSync(pkgFile)) {
      this.pkgInfo = Object.assign({}, defaultPkgInfo, wrapRequired(pkgFile));
    } else {
      this.pkgInfo = defaultPkgInfo;
    }
  }

  /* istanbul ignore next */
  green(msg, ex = '') {
    /* istanbul ignore next */
    console.log(blueBright(`\r\n[${this.cli.name}] ${_green(msg)}`), ex);
  }

  /* istanbul ignore next */
  red(msg, ex = '') {
    /* istanbul ignore next */
    console.log(blueBright(`\r\n[${this.cli.name}] ${_red(msg)}`), ex);
  }

  processCompilation(compilation) {
    compilation.stats.forEach((stat) => {
      stat.compilation.children = stat.compilation.children.filter((child) => {
        return (
          !/html-webpack-plugin/.test(child.name) &&
          !/mini-css-extract-plugin/.test(child.name)
        );
      });
    });
  }

  printCompilation(compilation) {
    compilation.stats.forEach((stat) => {
      process.stdout.write(
        `${stat.toString(
          merge(
            {
              colors: true,
              modules: false,
              children: true,
              chunks: false,
              chunkModules: false,
              entrypoints: false
            },
            compilation.stat
          )
        )}\n`
      );
    });
  }

  normalizeHotEntry(webpackConfig) {
    const target = webpackConfig.target;
    if (target !== 'web') return;

    const port = this.getPort(target);
    _normalizeHotEntry(webpackConfig, port);
  }

  normalizeWebpackConfig(webpackConfig) {
    const webpackConfigList = Array.isArray(webpackConfig)
      ? webpackConfig
      : [webpackConfig];
    webpackConfigList.forEach((item) => {
      if (item.devServer) {
        this.devServer = item.devServer;
        delete item.devServer;
      }
    });
    return webpackConfigList;
  }

  compilerHook(compiler, callback) {
    compiler.hooks.done.tap('webpack-tool-build-done', (compilation) => {
      callback(compilation);
    });
  }

  getPort(target = 'web', offset = 0) {
    const EASY_ENV_DEV_PORT = `EASY_ENV_DEV_PORT_${this.pkgInfo.name}`;
    const port =
      this.config.port || Number(process.env[EASY_ENV_DEV_PORT]) || 9000;
    // https://github.com/easy-team/egg-webpack/issues/24
    if (!this.config.offsetPort && target === 'web') {
      return port;
    }
    return port + offset;
  }

  // start webpack dev server and webpack build result view
  server(webpackConfig, options, callback) {
    return this.dev(
      webpackConfig,
      options,
      (compiler, compilation, webpackConfigItem) => {
        callback && callback(compiler, compilation, webpackConfigItem);
        // only one html file
        const htmlList = Object.keys(compilation.compilation.assets)
          .filter((url) => {
            return /\.(html|htm)$/.test(url);
          })
          .sort();
        const port = this.getPort();
        if (htmlList.length === 1) {
          const webpackConfig = compiler.options;
          const publicPath = webpackConfig.output.publicPath;
          const url = normalizeURL(port, publicPath, htmlList[0]);
          setTimeout(() => {
            this.green(`http visit url: ${url}`);
          }, 200);
        } else {
          this.createDebugServer(compiler, compilation);
        }
      }
    );
  }

  // start webpack dev server
  dev(webpackConfig, options, callback) {
    let readyCount = 0;
    const compilers: any[] = [];
    const webpackConfigList = this.normalizeWebpackConfig(webpackConfig);
    webpackConfigList.forEach((webpackConfigItem, index) => {
      this.normalizeHotEntry(webpackConfigItem);
      const compiler = webpack(webpackConfigItem);
      this.compilerHook(compiler, (compilation) => {
        readyCount++;
        if (!this.ready && readyCount % webpackConfigList.length === 0) {
          this.ready = true;
          callback && callback(compiler, compilation, webpackConfigItem);
        }
      });
      this.createWebpackServer(compiler, { offset: index });
      compilers.push(compiler);
    });
    return compilers;
  }

  // webpack build
  build(webpackConfig, options, callback) {
    const webpackConfigList = this.normalizeWebpackConfig(webpackConfig);
    const compiler = webpack(webpackConfigList, (err, compilation) => {
      // https://webpack.js.org/api/node/#webpack-
      if (err || (compilation && compilation.hasErrors())) {
        if (err) {
          this.red(`[${__filename}] webpack build error`, err);
        }
        process.exit(1);
      }
    });
    this.compilerHook(compiler, (compilation) => {
      this.processCompilation(compilation);
      this.printCompilation(compilation);
      callback && callback(compiler, compilation);
    });
    return compiler;
  }

  createDebugServer(compiler, stats) {
    const config = this.config;
    const app = new Koa();
    app.use(cors());
    app.use(async (ctx, next) => {
      if (this.url === '/debug') {
        ctx.body = new NavigationPage(config, compiler, stats).create();
      } else {
        await next;
      }
    });

    _getPort(this.config.debugPort).then((port) => {
      app.listen(port, () => {
        const devServer = this.devServer || {};
        const url = getBrowserUrl(port, 'debug');
        if (devServer.open) {
          open(url);
        }
        if (devServer.openPage) {
          open(devServer.openPage);
        }
        this.green(`start webpack build navigation ui view: ${url}`);
      });
    });
    return app;
  }

  createWebpackCompiler(webpackConfig, callback) {
    const compiler = webpack(webpackConfig);
    compiler.hooks.done.tap('webpack-tool-build-done', (compilation) => {
      callback && callback(compiler, compilation);
    });
    return compiler;
  }

  createDevServerOptions(webpackConfig, devServer: any = {}) {
    const { output } = webpackConfig;
    const {
      headers = {},
      index,
      methods,
      mimeTypes,
      publicPath,
      writeToDisk
    } = devServer;
    return {
      index,
      writeToDisk,
      mimeTypes,
      methods,
      publicPath: publicPath || output.publicPath,
      headers: {
        'x-webpack': 'easywebpack',
        'cache-control': 'max-age=0',
        ...headers
      }
    };
  }

  createWebpackServer(compiler, options: any = {}) {
    const offset = options.offset;
    const webpackConfig = compiler.options;
    const target = webpackConfig.target;
    const output = webpackConfig.output;
    const { devServer = {} } = this.config;
    const { before, after, proxy = {} } = devServer;
    const app = new Koa();

    // https://webpack.docschina.org/configuration/dev-server/#devserver-before
    if (typeof before === 'function') {
      before(app);
    }

    app.use(cors());
    // only web use hot middleware
    if (target === 'web' || target === undefined) {
      // http-proxy
      Object.keys(proxy).forEach((key) => {
        app.use(proxyMiddleware(key, proxy[key]));
      });
      // https://webpack.docschina.org/configuration/dev-server/#devserver-historyapifallback
      // const historyOptions = historyApiFallback === true ? {} : historyApiFallback;
      // app.use(historyMiddleware(historyOptions));
      app.use(hotMiddleware(compiler, { log: false, reload: true }));
    }

    const devOptions = this.createDevServerOptions(webpackConfig, devServer);
    app.use(devMiddleware(compiler, devOptions));

    // https://webpack.docschina.org/configuration/dev-server/#devserver-after
    if (typeof after === 'function') {
      after(app);
    }

    const port = this.getPort(target, offset);
    // https://github.com/easy-team/egg-webpack/issues/24
    if (!this.config.port) {
      this.config.port = port;
    }
    app.listen(port, () => {
      const url = getHost(port);
      if (target) {
        this.green(`start webpack ${target} building server: ${url}`);
      } else {
        this.green(`start webpack building server: ${url}`);
      }
      const key = target || 'web';
      setEasyInfo({
        [key]: {
          url,
          port,
          webpack: {
            context: webpackConfig.context,
            output: {
              path: output.path,
              publicPath: output.publicPath
            }
          }
        }
      });
    });
    return app;
  }
}

export default WebpackTool;

'use strict';
import os from 'os';
import fs from 'fs';
import open from 'opn';
import path from 'path';
import detect from 'detect-port';

import { getIPAddress } from '@root/shared/utils/server/net';

const getIp = (position) => {
  const interfaces = os.networkInterfaces();
  const ips: string[] = [];

  if (interfaces.en0) {
    for (let i = 0; i < interfaces.en0.length; i++) {
      if (interfaces.en0[i].family === 'IPv4') {
        ips.push(interfaces.en0[i].address);
      }
    }
  }
  if (interfaces.en1) {
    for (let i = 0; i < interfaces.en1.length; i++) {
      if (interfaces.en1[i].family === 'IPv4') {
        ips.push(interfaces.en1[i].address);
      }
    }
  }
  if (position > 0 && position <= ips.length) {
    return ips[position - 1];
  } else if (ips.length) {
    return ips[0];
  }
  return '127.0.0.1';
};

const getHost = (port) => {
  return `http://${getIPAddress()}:${port}`;
};

const getBrowserUrl = (port, url) => {
  let browserUrl;
  if (/^(https?:|\/\/)/.test(url)) {
    browserUrl = url;
  } else {
    const host = getHost(port);
    if (url) {
      browserUrl = `${host}/${url}`;
    } else {
      browserUrl = host;
    }
  }
  return browserUrl;
};

const openBrowser = (port, url) => {
  const browserUrl = getBrowserUrl(port, url);
  open(browserUrl);
  return browserUrl;
};

const normalizeURL = (port, publicPath, filename) => {
  if (/^(https?:|\/\/)/.test(publicPath)) {
    return publicPath + filename;
  }
  const host = getHost(port);
  return `${host + publicPath + filename}`;
};

const normalizeHotEntry = (webpackConfig, port) => {
  const hotMiddleware = require
    .resolve('webpack-hot-middleware')
    .split(path.sep);
  hotMiddleware.pop();
  const hotConfig = `${path.posix.join(
    hotMiddleware.join(path.sep)
  )}/client?path=http://${getIp(
    0
  )}:${port}/__webpack_hmr&noInfo=false&reload=true&quiet=false&test=true`;
  Object.keys(webpackConfig.entry).forEach((name) => {
    const value = webpackConfig.entry[name];
    const tempValues = Array.isArray(value) ? value : [value];
    const isHot = tempValues.some((v) => {
      return /webpack-hot-middleware/.test(v);
    });
    if (!isHot) {
      webpackConfig.entry[name] = [hotConfig].concat(value);
    }
  });
  return webpackConfig;
};

const getPort = (port) => {
  return detect(port)
    .then((_port) => {
      return _port;
    })
    .catch(() => {
      return port;
    });
};

const getCLI = (cli) => {
  if (cli && cli.name && cli.cmd) {
    return cli;
  }
  return (global as any).EASY_CLI || { name: 'easywebpack-cli', cmd: 'easy' };
};

// webpack-tool 的 package.json 中 webpack 版本设定为 4.28.4, 可以项目安装指定版本覆盖内置版本
const getWebpack = () => {
  const webpackPath = path.resolve(process.cwd(), 'node_modules', 'webpack');
  if (fs.existsSync(webpackPath)) {
    return require(webpackPath);
  }
  return require('webpack');
};

export {
  getWebpack,
  getIp,
  getHost,
  getBrowserUrl,
  openBrowser,
  normalizeURL,
  normalizeHotEntry,
  getPort,
  getCLI
};

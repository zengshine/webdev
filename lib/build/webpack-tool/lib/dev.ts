'use strict';

import webpackDevMiddleware from 'webpack-dev-middleware';

function middleware(doIt, req, res) {
  const { send: originalSend, end: originalEnd } = res;

  return new Promise((resolve) => {
    res.end = function end(...args) {
      originalEnd.apply(this, args);
      resolve(0);
    };
    res.send = function send(...args) {
      originalSend.apply(this, args);
      resolve(0);
    };
    doIt(req, res, () => {
      resolve(1);
    });
  });
}

export default (compiler, option) => {
  const doIt = webpackDevMiddleware(compiler, option);

  async function koaMiddleware(ctx, next) {
    const { req } = ctx;
    const locals = ctx.locals || ctx.state;

    ctx.webpack = doIt;

    const runNext = await middleware(doIt, req, {
      locals,
      end(content) {
        ctx.body = content;
      },
      send(content) {
        ctx.body = content;
      },
      setHeader(...args) {
        ctx.set(...args);
      },
      set(...args) {
        ctx.set(...args);
      },
      get(...args) {
        ctx.get(...args);
      }
    });

    if (runNext) {
      await next();
    }
  }

  Object.keys(doIt).forEach((p) => {
    koaMiddleware[p] = doIt[p];
  });

  return koaMiddleware;
};

'use strict';
import c2k from 'koa2-connect';
import { createProxyMiddleware } from 'http-proxy-middleware';
import { wrapRequired } from '@root/shared/utils/modules';

export default function (proxyUrl, proxyOptions) {
  if (proxyOptions && proxyOptions.pathRewrite) {
    return async function (ctx, next) {
      if (new RegExp(proxyUrl).test(ctx.url)) {
        await c2k(createProxyMiddleware(proxyOptions))(ctx, next);
      } else {
        await next();
      }
    };
  }

  const proxy = wrapRequired('http-proxy').createProxyServer(proxyOptions);
  return async function (ctx, next) {
    if (new RegExp(proxyUrl).test(ctx.url)) {
      await function (callback) {
        proxy.web(ctx.req, ctx.res, callback);
      };
    } else {
      await next();
    }
  };
}

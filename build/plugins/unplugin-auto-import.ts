// eslint-disable-next-line
const AutoImport = require('unplugin-auto-import');

const autoImportPlugin = AutoImport.webpack({
  include: [
    /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
    /\.vue$/,
    /\.vue\?vue/ // .vue
  ],

  imports: ['vue', 'vue-router', 'pinia', '@vueuse/core'],

  dts:
    process.env.NODE_ENV === 'production'
      ? './app/web/typings/auto-imports-production.d.ts'
      : './app/web/typings/auto-imports-development.d.ts',

  eslintrc: {
    enabled: true
  }
});

export default autoImportPlugin;

import path from 'path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import type { Compiler } from 'webpack';

class VueSSRAsyncChunkPlugin {
  localize = true;
  opts;
  constructor(opts = {}) {
    this.opts = Object.assign({}, { chunk: true }, opts);
  }

  apply(compiler: Compiler) {
    compiler.hooks.emit.tap('VueSSRAsyncChunkPlugin', (compilation) => {
      const buildPath = compilation.options.output.path;
      const chunkPath = path.join(buildPath!, '');

      if (!fs.existsSync(chunkPath)) {
        mkdirp.sync(chunkPath);
      }

      compilation.chunks.forEach((chunk) => {
        if (!this.opts.chunk) {
          return;
        }

        const asyncChunks = chunk.getAllAsyncChunks();
        asyncChunks?.forEach((asyncChunk) => {
          asyncChunk.files.forEach((filename) => {
            const filepath = path.join(chunkPath, filename);
            const dirname = path.dirname(filepath);
            if (!fs.existsSync(dirname)) mkdirp.sync(dirname);

            const source = compilation.assets[filename].source();
            fs.writeFileSync(filepath, source, 'utf8');
          });
        });
      });
    });
  }
}

export default VueSSRAsyncChunkPlugin;

import path from 'path';

export * from './config';

export function resolve(filepath) {
  return path.resolve(process.cwd(), `${filepath}`);
}

export function getRuntimeChunkName(fileName) {
  return `runtime-${fileName}`;
}

export function getPackageJson(path) {
  return require(path);
}

export function getProcessArgc(param) {
  const argvString = process.argv[2] || '{}';
  return JSON.parse(argvString)[param];
}

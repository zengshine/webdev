export function dedupePlugins(config) {
  const plugins = config.plugins?.filter((plugin) => {
    const { __plugin__ } = plugin;
    const excludePluginNames = [
      'webpack-manifest-resource-plugin',
      'VueSSRDynamicChunkPlugin'
    ];
    return !excludePluginNames.includes(__plugin__);
  });

  return plugins;
}

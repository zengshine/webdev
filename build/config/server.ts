import merge from 'webpack-merge';
import VueSSrDynamicChunkPlugin from '@root/build/plugins/vue-ssr-dynamic-chunk';

import commonConfig, { getCssLoaderConfig } from '@root/build/config/common';

const serverConfig = merge(commonConfig, {
  target: 'node',
  devtool: '',
  module: {
    rules: [...getCssLoaderConfig({ emit: false })]
  },
  plugins: [new VueSSrDynamicChunkPlugin()]
});

export default serverConfig;

import path from 'path';
// import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import autoImportPlugin from '@root/build/plugins/unplugin-auto-import';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CopyPlugin from 'copy-webpack-plugin';
import DuplicatePackageCheckPlugin from 'duplicate-package-checker-webpack-plugin';
import SpeedMeasureWebpackPlugin from 'speed-measure-webpack-plugin';
import { EsbuildPlugin } from 'esbuild-loader';

import { isProduction } from '@root/shared/utils';
import {
  resolve,
  getRuntimeChunkName,
  getPackageJson
} from '@root/build/utils';

import envConfig from '@root/env';

const isProd = isProduction();
const PackageJson = getPackageJson(path.join(process.cwd(), 'package.json'));

// transform to full path
const aliasConfig = Object.entries(PackageJson._moduleAliases).reduce(
  (pre, cur) => {
    const [key, value] = cur;
    return Object.assign(pre, { [key]: resolve(value) });
  },
  {}
);

export function getCssLoaderConfig(params: { emit: boolean }) {
  const { emit = true } = params;
  return [
    {
      test: /\.(sc|sa)ss$/, // scss, sass, css
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
            emit
          }
        },
        'css-loader',
        {
          loader: 'postcss-loader'
        },
        {
          loader: 'sass-loader',
          options: {
            additionalData: `
            @import "@web/styles/mixins.scss"; 
            @import "@web/styles/variable.scss";
            `
          }
        }
      ],
      include: resolve('app/web')
    }
  ];
}

export default {
  egg: true,
  port: process.env.ENV_EGG_WEBPACK_DEV_PORT,
  mode: isProd ? 'production' : 'development',
  entry: {
    app: 'app/web/main.ts'
  },
  output: {
    clean: true,
    filename: ({ runtime }) => {
      if (runtime === 'sw') {
        // Output a service worker in the root of the dist directory
        // Also, ensure the output file name doesn't have a hash in it
        return '[name].js';
      }

      // Otherwise, output files as normal
      return '[name].js';
    }
  },
  devtool: '',
  optimization: {
    runtimeChunk: {
      name: (entrypoint) => getRuntimeChunkName(entrypoint.name)
    },
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          minChunks: 2,
          reuseExistingChunk: true,
          chunks: 'all',
          filename: '[name].[contenthash].js'
        },
        d3: {
          test: /[\\/]node_modules[\\/]d3[\\/]/,
          name: 'd3',
          minChunks: 1,
          reuseExistingChunk: false,
          chunks: 'all',
          filename: '[name].[contenthash].js'
        },
        styles: {
          name: 'styles',
          type: 'css/mini-extract',
          chunks: 'all',
          filename: '[name].[contenthash].js',
          enforce: true
        }
      }
    },
    minimize: isProd,
    minimizer: [
      new EsbuildPlugin({
        target: 'es2015',
        css: true
      })
    ]
  },
  resolve: {
    extensions: ['.vue', '.ts', '.tsx', '.mjs', '.js', '.scss'],
    alias: {
      ...aliasConfig,
      ...(isProd && { 'vue-types': 'vue-types/shim' })
    }
  },
  module: {
    rules: [
      { ts: false },
      { babel: false },
      {
        test: /\.ts$/,
        loader: 'esbuild-loader',
        options: {
          loader: 'ts',
          // target: 'es2015',
          tsconfig: 'app/web/tsconfig.json'
        }
      },
      {
        test: /\.tsx$/,
        use: [
          {
            loader: 'babel-loader'
          }
        ],
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    { extract: false },
    { speed: false },
    { copy: false },
    { commonsChunk: false },
    { clean: false },
    {
      define: {
        // webpack DefinePlugin
        ...envConfig,
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    },
    new MiniCssExtractPlugin({
      filename: isProd ? '[name].[contenthash].css' : '[name].css'
    }),
    new CopyPlugin({
      patterns: [{ from: resolve('app/web/assets'), to: 'assets' }]
    }),
    autoImportPlugin,
    new DuplicatePackageCheckPlugin()
  ],

  customize(config) {
    new SpeedMeasureWebpackPlugin({ disable: true }).wrap({}); // TODO: compatibility
    return config;
  }
};

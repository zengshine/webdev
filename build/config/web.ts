import path from 'path';
import merge from 'webpack-merge';
import StylelintPlugin from 'stylelint-webpack-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import { InjectManifest } from 'workbox-webpack-plugin';

import { isProduction } from '@root/shared/utils';
import { getProcessArgc } from '@root/build/utils';
import { is } from '@root/shared/utils/boolean';

import commonConfig, { getCssLoaderConfig } from '@root/build/config/common';

const isProd = isProduction();

const HASH_REG = /([a-f0-9]{20}\.?)/gi;

function generateManifest(seed, files, entries) {
  const deps = {};
  const mapping = {};
  Object.entries(entries).forEach(([entryName, dependKeys]: any) => {
    const entry = seed[entryName] || `${entryName}.js`;
    const dependencies = dependKeys.map((key) => key.replace(HASH_REG, ''));
    const initialFiles = files.filter((file) =>
      dependencies.includes(file.name)
    );

    const js = initialFiles
      .filter((file) => file.path.endsWith('.js'))
      .map((file) => file.path);

    const css = initialFiles
      .filter((file) => file.path.endsWith('.css'))
      .map((file) => file.path);

    const mappingItem = files.reduce((value, file) => {
      const { name, path } = file;
      value[name] = path;
      return value;
    }, {});

    Object.assign(mapping, mappingItem); // TODO: multi entry key conflict
    Object.assign(deps, {
      [entry]: {
        js,
        css
      }
    });
  });

  return {
    ...mapping,
    deps
  };
}

function getPlugins() {
  const stylelintPlugin = new StylelintPlugin({
    context: '.',
    files: ['app/web/**/*.{html,vue,css,sass,scss}'],
    fix: false,
    cache: true,
    emitError: false
  });
  const webpackManifestPlugin = new WebpackManifestPlugin({
    writeToFileEmit: true,
    removeKeyHash: HASH_REG,
    fileName: path.join(process.cwd(), 'config/manifest.json'),
    generate(seed, files, entries) {
      return generateManifest(seed, files, entries);
    }
  });

  // const generateSWPlugin = new GenerateSW({
  //   clientsClaim: true,
  //   skipWaiting: true,
  //   inlineWorkboxRuntime: true
  // });

  const workboxPlugin = new InjectManifest({
    swSrc: path.join(process.cwd(), 'app/web/service-worker/index.ts'),
    swDest: path.join(process.cwd(), 'public/service-worker.js')
  });

  const cssMinimizerPlugin = new CssMinimizerPlugin();

  const commonPlugins: any[] = [stylelintPlugin, webpackManifestPlugin];
  if (is(process.env.WORKBOX_SERVICE_WORKER_ENABLE)) {
    commonPlugins.push(workboxPlugin);
  }

  const devPlugins = [...commonPlugins];
  const prodPlugins = [...commonPlugins, cssMinimizerPlugin];

  if (getProcessArgc('analyze')) {
    devPlugins.push(new BundleAnalyzerPlugin());
  }

  return isProd ? prodPlugins : devPlugins;
}

const webConfig = merge(commonConfig, {
  target: 'web',
  devtool: isProd ? '' : 'source-map',
  output: {
    filename: isProd ? '[name].[contenthash].js' : '[name].js',
    chunkFilename: isProd
      ? 'js/chunk/[name].[contenthash].js'
      : 'js/chunk/[name].js'
  },
  plugins: getPlugins(),
  module: {
    rules: [...getCssLoaderConfig({ emit: true })]
  }
});

export default webConfig;

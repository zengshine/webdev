FROM node:16-alpine3.16

ENV TIME_ZONE=Asia/Shanghai

RUN \
  mkdir -p /usr/src/webdev \
  && echo "${TIME_ZONE}" > /etc/timezone \
  && ln -sf /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime

WORKDIR /usr/src/webdev

COPY package.json /usr/src/webdev

RUN yarn install --registry=https://registry.npm.taobao.org

COPY . /usr/src/webdev

RUN npm run build

EXPOSE 7001

CMD ["npm","start"]

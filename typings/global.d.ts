declare var EASY_ENV_IS_NODE: boolean;
declare var EASY_ENV_IS_BROWSER: boolean;
declare const ENV_APOLLO_WS_PORT: string;
declare const SERVER_HOST: string;
declare const ENV_APOLLO_WS_PORT_PROXY: string;

type TimerId = null | ReturnType<typeof setTimeout>;

type anyFn = (...args: any[]) => any;

interface ResponseData {
  code: number;
  success: boolean;
  msg: string;
  data: any;
}

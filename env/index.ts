import * as dotenv from 'dotenv';

import { transformObjectProperties } from '@root/shared/utils';

const { STAGE } = process.env;
const suffix = STAGE ? (STAGE === 'prod' ? '' : `.${STAGE}`) : '';
const { parsed } = dotenv.config({
  path: `${process.cwd()}/.env${suffix}`
});

const parsedValue = transformObjectProperties(parsed!, (val) =>
  JSON.stringify(val)
);

export default parsedValue;

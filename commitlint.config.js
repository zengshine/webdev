module.exports = {
  extends: ['@commitlint/config-conventional'],

  // Type:
  // feat: a new feature for the user, not a new feature for build script. Such commit will trigger a release bumping a MINOR version.
  // fix: a bug fix for the user, not a fix to a build script. Such commit will trigger a release bumping a PATCH version.
  // perf: performance improvements. Such commit will trigger a release bumping a PATCH version.
  // docs: changes to the documentation.
  // style: formatting changes, missing semicolons, etc.
  // refactor: refactoring production code, e.g. renaming a variable.
  // test: adding missing tests, refactoring tests; no production code change.
  // build: updating build configuration, development tools or other changes irrelevant to the user.
  // chore: changes to the build process or auxiliary tools and libraries such as documentation generation
};

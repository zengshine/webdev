import 'module-alias/register';
import type { Application, IBoot } from 'egg';

import '@root/env';
import { setupAuthorization } from '@root/app/utils/auth';

export default class RootApp implements IBoot {
  private readonly app: Application;

  constructor(app: Application) {
    console.log('🚀 app boot=========================>');
    this.app = app;
  }

  configWillLoad() {
    // Ready to call configDidLoad,
    // Config, plugin files are referred,
    // this is the last chance to modify the config.
  }

  configDidLoad() {
    // Config, plugin files have loaded.
  }

  async didLoad() {
    // All files have loaded, start plugin here.
  }

  async willReady() {
    // All plugins have started, can do some thing before app ready.
    setupAuthorization(this.app);
  }

  async didReady() {
    // Worker is ready, can do some things
    // don't need to block the app boot.
  }

  async serverDidReady() {
    // Server is listening.
  }

  async beforeClose() {
    // Do some thing before app close.
  }
}

# react 学习笔记

之前对 React 有个大概的认识，但没有很系统的去学习，想借这个假期完整理一遍官方文档，但由于其它原因，只是看了小部分，下面是看了后整理的一些简单笔记。

## 基本认识

React 应该是前端现今使用最广泛的一个 UI 库，生态非常完备，由于其灵活性，非常适合应用于交互复杂的中大型项目。跟 vue、angular 及其他现代框架一样，React 也遵循几个核心的概念：

- 组件化
- 声明式的写法来描述 UI
- 状态驱动视图

另外，React 也只提供核心的 UI 渲染相关的功能，其它的特性可以通过插件机制扩展，比如路由，状态管理等。

React 使用 JSX 来描述 UI，这种高度灵活的模板语法，结合 Hooks 去做状态和副作用管理，让我们可以应对复杂的交互场景，同时保持代码的可复用性和扩展性，应对不断变化的需求和增长的项目规模。

但这种灵活性动态声明写法也有不足的地方，跟 vue 常用的基于静态结构化的模板声明相比，少了可静态编译优化这个特性，因此在特定场景渲染性能并不占优势。

## 基本概念

### 组件化

组件化已经成为现在主流 UI 库/框架的核心概念，Web Components 也在逐步成为成熟的标准。

组件化就是 UI 的模块化，需要遵循单一职责原则，一个组件只做一件事情。

做好组件的状态管理是组件划分中一个核心点。要区分好有状态组件和无状态组件。要尽可能减少或移除组件内的状态，耦合状态越少，越利于组件的复用，但同时要警惕状态抽离后带来的复杂的组件间通信，必不可少的状态通信要遵循单向数据流的原则，全局状态增长到一定的复杂度可以引入全局状态管理库。

### 事件与状态

React 中事件的的声明也非常直观，声明事件回调然后在模板中绑定。事件回调就是一个规范的函数，可以使用任何标准的事件特性，作为函数也可以当作 props 在组件间传递。

组件中的当前状态是组件当前的快照，并不能直接更改状态，而是通过更新方法触发组件的重新渲染，从而得到一个新的快照。

组件的更新也是异步更新，多个更新会生成更新队列，重复更新会被合并，更新主要可以分为三步：

- 更改状态触发更新
- 组件渲染得出 DOM 变更
- 执行 DOM 变更

组件的状态一般采用 Hooks 的方式进行管理。

### Hooks

Hooks 允许在函数组件中对状态和副作用进行管理，本质上是提供了一种组件无关的小颗粒度的状态和副作用复用机制。

Hooks 解决了函数组件无法声明状态和副作用的问题，可以按照功能层级对逻辑进行划分和复用，避免了复杂组件随着功能增加会逐渐变得难以维护的问题。

Hooks 主要分为状态 Hooks 和副作用 Hooks 两种，Hooks 的使用要遵循以下几个规则：

- 只能在组件顶级作用域声明和调用，不能在循环块，条件块，嵌套函数中使用

- 只能在组件函数中调用

- Hooks 可以相互引用

#### React 内置了以下常用 hook:

- useCallback

- useContext

- useDebugValue

- useEffect

- useEvent

- useId

- useImperativeHandle

- useInsertionEffect

- useLayoutEffect

- useMemo

- useReducer

- useRef

- useState

- useSyncExternalStore

- useTransition

#### 自定义 Hooks

基于内置 Hooks 和 Hooks 可以互相引用的规则，我们根据实际场景自定义 Hooks，用于封装可复用逻辑。

下面是一个常见的基于 useState 和 useEffect 获取鼠标位置的 demo:

```javascript
import { useEffect, useState } from 'react';

export const useMousePosition = () => {
  const [position, setPosition] = useState({ x: 0, y: 0 });

  useEffect(() => {
    const setFromEvent = (e) => setPosition({ x: e.clientX, y: e.clientY });
    window.addEventListener('mousemove', setFromEvent);

    return () => {
      window.removeEventListener('mousemove', setFromEvent);
    };
  }, []);

  return position;
};
```

### 生态

react 的完备生态是其一大优势，下面写几个入门常见的

- 脚手架: create-react-app

- 跨平台: React Native

- 服务端渲染: next.js

- 小程序: taro

- 虚拟列表: react-virtualized, react-window

- 组件: react-table, react-draggable

更多详细可以参考 [awesome-react](https://github.com/enaqx/awesome-react)

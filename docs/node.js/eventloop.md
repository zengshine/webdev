### EventLoop

#### Phases

- timers: setTimeout & setInterval

- pending callbacks: I/O callback deferred to next loop iteration

- idle, prepare: used internally

- poll: I/O events; I/O related callbacks(exception of close callback)

- check: setImmediate callbacks

- close callback: closed callback, e.g. socket.on('close', ...)

# package.json

- use debug module

  ```bash
  cross-env DEBUG=egg-core:loader  egg-bin dev
  ```

# Prisma

## Scripts

- generate schema from existing db

  ```bash
  prisma db pull
  ```

- create db from existing schema

  ```
  prisma db push
  ```

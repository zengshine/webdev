#### why does tcp need handshakes to build up connection ?
> to make sure both the client and server can send & receive packets

#### why someone said that tcp isn't built with maximum efficiency ?
> - tcp sees all of the data it transports as single "file" which would lead to head of line blocking
> - tcp needs handshakes to build up connection which would cost a full network round trip

#### why can't http/2 be directly run on QUIC?
> mainly due to feature duplication

#### what are the main feature of http/3 ?
> faster connection setup(o-RRT)
> less head of line blocking(stream multiplexing by QUIC)
> support connection migration(connection ID)
> header compression(QPACK)
> congestion control by QUIC
> QUIC use frames(extensible)
> QUIC deeply integrates with TLS
> QUIC can evolve more easily

#### why quic is build on top of udp ?
> it will make quic more easier to deploy because udp is already implemented by all devices on the Internet


### installation

```bash
# avoid using snap for it's slow speed in cn mainland
apt-get update
sudo apt-get install certbot
```

### generate certification

```bash
certbot certonly --webroot
```

### renew certification

```bash
certbot renew
```

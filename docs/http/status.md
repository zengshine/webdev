### Http response status code

- 100 continuos

- 101 switch protocol

- 200 completely success

- 201 created success

- 202 accepted

- 204 no content

- 206 partial content

- 301 move permanently

- 302 found

- 303 see other: indicate that the redirect don't link to the request resource itself, but to another page

- 304 not modified

- 307 temporary redirect: difference between 307 and 302 is that 307 guarantee the the method and body will
  not be changed when the redirect request is made.

- 308 permanent redirect

- 400 bad request

- 401 unauthorized

- 402 payment required

- 403 forbidden

- 404 not found

- 405 method not allowed

- 500 internal error

- 501 not implemented

- 502 bad gateway: server as gateway or proxy received an invalid response from upstream

- 503 service unavailable: server is not ready to handle the request

- 504 gateway timeout

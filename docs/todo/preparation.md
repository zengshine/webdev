### Preparation

#### Performance

- [] what is web performance

#### vue & react

- [] why is vue a performant framework

- [] suspense: vue & react; lazy load

- [] async setup limitation: lost current component after first await

- [] sequence diff & longest ascending sequence

- scope: implementation & concepts

- defineCustomElement

- usage of: ExtractPropTypes

- custom element: web components

#### utilities

- [] the implementation of merge function

- [] async function executor

#### ES & Node.js

- module resolve: circular dependencies

- garbage collection & (weakMap, weakSet)

#### build

- implementation of webpack plugin & loader

- iconfont loader(svg-sprite-loader)

- file size limitation: workflow

#### UI-Library

- monorepo

- bit.dev

- create an ui-library

##### Headless UI

- foundation & adapter

- TanStack Table

#### iconfont

- svg
- css: unicode
- svg-sprite-loader: generate the svg-sprite. (vite svg sprite )

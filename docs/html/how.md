### how to communicate between tab ?

async function asyncExec() {
for(i of [1,2,3]) {
const res = await new Promise((resolve)=>{
setTimeout(()=>{
console.log('timeout', i)
resolve()
}, 1000)
})
console.log('res', i)
console.log('after res', i)
}
}

async function asyncExec() {
const arr = [1,2,3]
arr.forEach(async (i)=>{

const res = await new Promise((resolve)=>{
setTimeout(()=>{
console.log('timeout', i)
resolve()
}, 1000)
})
console.log('res', i)
console.log('after res', i)

})
}

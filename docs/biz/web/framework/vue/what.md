#### what is vue ?

> Vue is a progressive javascript framework for building user interface. It builds on top of standard Html, javascript, css and its reactivity, declarative rendering, component-base programming model could help you build complex user interface with efficiency.

#### what does 'createApp(options, props).mount('#app')' actually do ?

#### what is new in Vue3 ?

> composition-api
> native typescript support
> update performance: flag the static content in compile period

#### what is the pros and cons of VNode ? if it is necessary ?

> it the abstraction of real DOM, and it help to find the minimal difference of the new and old tree and maximize the update performance.
> but is there a way to directly update the DOM ? what if you can find the relationship between the update data and the minimal related DOM ?

#### what are the similarities and differences of Vue and React ?

> they are both progressive javascript library which base on the declarative rendering, component-base and data-driving programming model.
> But the implement way of data-driving model is different. Vue is based on an reactive data system while React is all about function execution.

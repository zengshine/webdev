### Hooks

```bash
issue:
The '.husky/pre-commit' hook was ignored because it's not set as executable.

solution
chmod ug+x .husky/* [https://github.com/typicode/husky/issues/1177#issuecomment-1212831091]
```

### Structure of params

- make params structure when methods would be call by different source, it could make debug easier;

```javascript
function targetMethod(params) {
  const { source = 'defaultSource' } = params
  console.log('the source field will make make you easier to find out who is the caller')
}

function sourceMethodA() {
  targetMethod({ source = 'sourceMethodA', payload = 'a' })
}

function sourceMethodB() {
  targetMethod({ source = 'sourceMethodB', payload = 'b' })
}

function sourceMethodC() {
  targetMethod({ source = 'sourceMethodC', payload = 'c' })
}
```

- magic number & magic string is not allowed

```javascript
function magicCode(state) {
  const STATE_CODE = {
    resolve: 'resolve',
    reject: 'reject',
    success: 0
  };

  if (state === 0) {
    console.log('magic number is not allowed');
  }

  if (state === 'resolve') {
    console.log('magic string is not allowed');
  }

  if (state === STATE_CODE.resolve) {
    console.log('better to use semantic constant');
  }
}
```

- the url's params & query comment of each page

- don't repeat yourself

- use module to decompose complexity

- be semantic on any naming scenario

- use pure function and avoid sideEffect

- unit test for the sharing code (refactor)

- must specify the reason of the usage of setTimeout

- condition judgement should be sematic and simplified

- must remove global event listener when unload page/component

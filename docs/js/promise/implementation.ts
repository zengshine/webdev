const PROMISE_STATE = {
  pending: 'pending',
  resolved: 'resolved',
  rejected: 'rejected'
};

export class PromiseA {
  private state: string = PROMISE_STATE.pending;
  private value: any;
  private handlers: any[];

  constructor(executor) {
    let { resolve, reject } = this;
    resolve = resolve.bind(this);
    reject = reject.bind(this);
    try {
      executor(resolve, reject);
    } catch (ex) {
      reject(ex);
      console.log('ex=========================>', ex);
    }
  }

  private resolve(val) {
    if (this.state !== PROMISE_STATE.pending) return;

    setTimeout(() => {
      this.doResolve(val, (val) => {
        if (this.state !== PROMISE_STATE.pending) return;

        this.value = val;
        this.state = PROMISE_STATE.resolved;
        this.handlers
          .filter((item) => item.type === PROMISE_STATE.resolved)
          .forEach((handler) => handler(this.value));
      });
    });
  }

  private reject(err) {
    if (this.state !== PROMISE_STATE.pending) return;

    this.value = err;
    this.state = PROMISE_STATE.rejected;
    this.handlers
      .filter((item) => item.type === PROMISE_STATE.resolved)
      .forEach((handler) => handler(this.value));
  }

  private doResolve(val, resolveHandler) {
    try {
      const { then } = val;
      if (typeof then === 'function') {
        then.bind(val, resolveHandler, this.reject.bind(this));
        return;
      }

      resolveHandler(val);
    } catch (ex) {
      this.reject(ex);
    }
  }

  then(resolveHandler, rejectHandler) {
    return new PromiseA((resolve, reject) => {
      const resolvePendingHandler = (val) => {
        try {
          const result = resolveHandler(val);
          resolve(result);
        } catch (ex) {
          reject(ex);
        }
      };
      const rejectPendingHandler = (val) => {
        try {
          const result = rejectHandler(val);
          reject(result);
        } catch (ex) {
          reject(ex);
        }
      };
      if (this.state === PROMISE_STATE.pending) {
        this.handlers.push(
          {
            type: PROMISE_STATE.resolved,
            handler: resolvePendingHandler
          },
          {
            type: PROMISE_STATE.rejected,
            handler: rejectPendingHandler
          }
        );
      } else if (this.state === PROMISE_STATE.resolved) {
        resolvePendingHandler(this.value);
      } else if (this.state === PROMISE_STATE.rejected) {
        rejectPendingHandler(this.value);
      }
    });
  }
}

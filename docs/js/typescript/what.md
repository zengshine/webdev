#### what are the pros and cons of typescript ?

> pros:
>
> - static type checking helps reduce runtime error and make it easier to refactor code in large-scale applications
> - typing helps improve develop experience by enable type-based auto-completion in IDE.
> - typing helps enchant the readability of the code and collaborative of the team.
>
> cons:
>
> - lost the flexibility of dynamic type system of javascript.
> - it add complexity to the codebase
> - it takes time to compile

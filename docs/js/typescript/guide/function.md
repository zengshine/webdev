### function

给函数变量声明类型的方式如下：

```typescript
// 写法一
const hello = function (txt: string) {
  console.log('hello ' + txt);
};

// 写法二
const hello: (txt: string) => void = function (txt) {
  console.log('hello ' + txt);
};

// 写法三：对象形式
// {
//   (参数列表): 返回值
// }

const hello: {
  (txt: string): void;
};
```

以上写法二在有多个函数复用同一类型的情况下通常会提取成公用类型：

```typescript
type MyFunc = (txt: string) => void;
```

函数的实际参数个数可以少于类型指定的参数个数，但是不能多于，即 typescript 运行省略参数。

可以通过 typeof 操作符返回函数的类型：

```typescript
function add(x: number, y: number) {
  return x + y;
}

const myAdd: typeof add = function (x, y) {
  return x + y;
};
```

#### Function 类型

typescript 提供 Function 类型表示函数，任何函数都属于这个类型（是任何函数的父类型）。
Function 函数类型可以接受任意数量的参数，每个参数类型都是 any。

#### 箭头函数（arrow function）

#### 可选参数 (optional argument)

如果函数的某个参数可以省略，则在参数名后面加问号(?)表示。

```typescript
function f(x?: number) {
  // ...
}
```

数名带有问号，表示该参数的类型实际上是原始类型|undefined，它有可能为 undefined。

#### 参数默认值

#### 参数解构

#### rest 参数

rest 参数表示函数的所有参数，它们可以是数组，也可能是元组。

如果元组里面的参数是可选的，则要使用可选参数。

#### readonly 参数

#### void 类型

void 类型表示函数没有返回值。但允许返回 null 或 undefined。

需要特别注意的是，如果变量、对象方法、函数参数是一个返回值为 void 类型的函数，那么并不代表不能赋值为有返回值的函数。恰恰相反，该变量、对象方法和函数参数可以接受返回任意值的函数，这时并不会报错。
这是因为，这时 TypeScript 认为，这里的 void 类型只是表示该函数的返回值没有利用价值，或者说不应该使用该函数的返回值。只要不用到这里的返回值，就不会报错。

```typescript
type voidFunc = () => void;

const f: voidFunc = () => {
  return 123;
};
```

#### never

never 类型表示空值或根本不会出现的值。它用在函数的返回值，就表示某个函数肯定不会返回值，即函数不会正常执行结束。

never 返回值类型主要有以下两种情况：

- 抛出移除的函数
- 无限执行的函数

#### 局部类型

#### 高阶函数

一个函数的返回值还是一个函数，那么这个函数就是高阶函数（higher-order-function）

#### 函数重载（function overload）

有些函数可以接受不同类型或不同个数的参数，并且根据参数的不同，会有不同的函数行为，这种根据不同参数类型，执行不同逻辑的行为，称为函数重载。

#### 构造函数

javascript 使用构造函数生成对象的实例，构造函数的最大特点就是必须使用 new 命令调用。

构造函数的类型写法，就是在参数列表前面加上 new 命令。

```typescript
class Animal {
  numLegs: number = 4;
}

type AnimalConstructor = new () => Animal;

function create(c: AnimalConstructor): Animal {
  return new c();
}

const a = create(Animal);
```

上面示例中，类型 AnimalConstructor 就是一个构造函数，而函数 create()需要传入一个构造函数。在 JavaScript 中，类（class）本质上是构造函数，所以 Animal 这个类可以传入 create()。

构造函数还有另一种类型写法，就是采用对象形式。

```typescript
type F = {
  new (s: string): void;
};
```

某些函数既是构造函数，又可以当作普通函数使用，比如 Date()。这时，类型声明可以写成下面这样。

```typescript
type F = {
  new (s: string): object;
  (n?: number): number;
};
```

### interface

interface 是对象的模板，可以看作一种类型的约定，使用了某个模板的对象，就拥有了指定的类型结构。

interface 可以表示对象的各种语法，它的成员有 5 种形式：

- 对象属性
- 对象的索引属性
- 对象的方法
- 函数
- 构造函数

#### interface 的继承

interface 可以使用 extends 关键字继承其他类型，有以下几种情况：

##### interface 继承 interface

继承其他 interface

```typescript
interface Shape {
  name: string;
}

interface Circle extends Shape {
  radius: number;
}
```

interface 允许多重继承

```typescript
interface Style {
  color: string;
}

interface Shape {
  name: string;
}

interface Circle extends Style, Shape {
  radius: number;
}
```

多重接口继承，实际上相当于多个父接口的合并。

如果子接口与父接口存在同名属性，那么子接口的属性会覆盖父接口的属性。注意，子接口与父接口的同名属性必须是类型兼容的，不能有冲突，否则会报错。

##### interface 继承 type

interface 可以继承 type 命令定义的对象类型。

```typescript
type Country = {
  name: string;
  capital: string;
};

interface CountryWithPop extends Country {
  population: number;
}
```

##### interface 继承 class

```typescript
class A {
  x: string = '';

  y(): boolean {
    return true;
  }
}

interface B extends A {
  z: number;
}
```

#### interface 合并

同名 interface 会进行属性合并

#### type & interface 的异同

type 和 interface 都可以声明一个对象类型。但有以下不同点：

- type 能够表示非对象类型，而 interface 只能表示对象类型（包括数组、函数等）。
- interface 可以继承其他类型，type 不支持继承
- 同名 interface 会自动合并，同名 type 则会报错。也就是说，TypeScript 不允许使用 type 多次定义同一个类型。
- interface 不能包含属性映射，type 可以

  ```typescript
  interface Point {
    x: number;
    y: number;
  }

  type pointMap = {
    [key in keyof Point]: Point[key];
  };
  ```

- this 关键字只能用于 interface 中，type 定义中不支持

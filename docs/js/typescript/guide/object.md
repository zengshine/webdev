### 对象

对象是 javascript 中最基本的数据结构。
其类型声明方式如下：

```typescript
// 字面量形式
type typeObject = {
  a: string,
  op?: string, // optional
  readonly rp: string, // readonly
  [p: string]: number // object index
  [i: number]: string // array index
  [s: symbol]: string
  f(p1: string, p2: number): void
  // 或者
  f1: p1: string, p2: number) => void
}

// 接口形式
interface interfaceObject {
  a: string,
  b: number
}
```

对象类型可以使用方括号读取属性类型：

```typescript
type User = {
  name: string;
  age: number;
};

type userAge = User['age']; // number
```

typescript 不区分对象的自身属性和继承属性，一律视为对象属性：

```typescript
interface MyInterface {
  toString(): string; // 继承的属性
  prop: number; // 自身的属性
}

const obj: MyInterface = {
  // 正确
  prop: 123
};

// obj只写了prop属性，但是不报错。因为它可以继承原型上面的toString()方法
```

### 可选属性

#### 只读属性

#### 属性名的索引类型

#### 解构赋值

#### 结构类型原则(structure typing)

只要对象 B 满足 A 的结构特征，typescript 就认为 B 对象兼容 A 的类型，这就是"结构类型"原则。

TypeScript 之所以这样设计，是为了符合 JavaScript 的行为。JavaScript 并不关心对象是否严格相似，只要某个对象具有所要求的属性，就可以正确运行。

如果类型 B 可以赋值给类型 A，TypeScript 就认为 B 是 A 的子类型（subTyping），A 是 B 的父类型。子类型满足父类型的所有结构特征，同时还具有自己的特征。凡是可以使用父类型的地方，都可以使用子类型，即子类型兼容父类型。

#### 严格字面量检查

#### 最小可选属性原则

#### 空对象

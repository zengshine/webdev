### tuple

tuple 表示成员类型可以自由设置的数组，数组的各个成员的类型可以不同，但必须明确声明每个类型的类型。

元组的声明方式如下：

```typescript
let a: [number, string, boolean] = [1, '2', true];
```

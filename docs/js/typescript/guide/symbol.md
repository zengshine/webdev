### symbol

Symbol 是一种原始值类型的值，类似于字符串，但每个 Symbol 值都是唯一的，与其他任何值都不相等。

```typescript
let x: symbol = Symbol();
```

#### unique symbol

symbol 类型包含所有的 Symbol 值，但是无法表示某一个具体的 Symbol 值。 为了解决这个问题，typescript 新增了 symbol 的一个子类型 unique symbol; 它表示某个具体的 symbol 值，类型于字符串的字面量。
unique symbol 类型只能用 const 进行声明。

```typescript
const x: unique symbol = Symbol();
```

如果变量声明时没有给出类型，TypeScript 会推断某个 Symbol 值变量的类型。

let 命令声明的变量，推断类型为 symbol, const 命令声明的变量，推断类型为 unique symbol。

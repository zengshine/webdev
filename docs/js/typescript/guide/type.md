#### boolean

#### number

#### bigint

#### string

#### symbol

### object & Object

#### null & undefined

在 javascript 中，null 表示值为空，undefined 表示没有赋值。

typescript 中任何类型都可以被赋值为 null 和 undefined，但这样会导致一些运行时的类型错误，可以通过 strickNullCheck 禁用 null 和 undefined 赋值给其他类型。

#### 包装类型

#### 值类型

#### 联合类型 （union）

#### 交叉类型 (intersection)

#### type 命令

### typeof 运算符

在 javascript 中 typeof 返回字符串，该字符串对应值的类型说明：

```javascript
typeof undefined; // "undefined"
typeof true; // "boolean"
typeof 1337; // "number"
typeof 'foo'; // "string"
typeof {}; // "object"
typeof parseInt; // "function"
typeof Symbol(); // "symbol"
typeof 127n; // "bigint"
```

在 typescript 中 typeof 返回的时值的类型，可以作为类型声明使用：

```typescript
const a = { x: 0 };

type T0 = typeof a; // { x: number }
type T1 = typeof a.x; // number
```

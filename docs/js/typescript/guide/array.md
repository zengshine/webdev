#### 数组类型声明方式

```typescript
const arr: number[];

const arrUnion: (number | string)[];

const arrInterface: Array<number>;
```

#### readonly Array

readonly 数组是数组的父类型，因为 readonly 数组没有类似 push, shift 等方法。

readonly 数组的声明方式有以下几种：

```typescript
const arr: readonly number[];

const a1: ReadonlyArray<number>;

const a2: Readonly<number[]>;
```

#### 多维数组

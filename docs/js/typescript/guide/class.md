### class

#### 简介

类型（class）是面向对象编程的基本构件，封装了属性和方法。

类属性的类型可以在顶层声明，也可以在构造方法内部声明。

ypeScript 有一个配置项 strictPropertyInitialization，只要打开（默认是打开的），就会检查属性是否设置了初值，如果没有就报错。

```typescript
// 打开 strictPropertyInitialization
class Point {
  x: number; // 报错
  y: number; // 报错
}
```

类的只读属性可以在构造函数中进行初始化：

```typescript
class A {
  readonly id: string;

  constructor() {
    this.id = 'bar'; // 正确
  }
}
```

构造方法不能声明返回值类型，否则报错，因为它总是返回实例对象。

```typescript
class B {
  constructor(): object {
    // 报错
    // ...
  }
}
```

存取器（accessor）是特殊的类方法，包括取值器（getter）和存值器（setter）两种方法。

它们用于读写某个属性，取值器用来读取属性，存值器用来写入属性。

```typescript
class C {
  _name = '';
  get name(): string {
    return this._name;
  }
  set name(value: number | string) {
    this._name = String(value);
  }
}
```

#### 类的 interface

interface 接口或 type 别名，可以用对象的形式，为 class 指定一组检查条件。然后，类使用 implements 关键字，表示当前类满足这些外部类型条件的限制。

```typescript
interface Country {
  name: string;
  capital: string;
}
// 或者
type Country = {
  name: string;
  capital: string;
};

class MyCountry implements Country {
  name = '';
  capital = '';
}
```

interface 只是指定检查条件，如果不满足这些条件就会报错。它并不能代替 class 自身的类型声明。

```typescript
interface A {
  get(name: string): boolean;
}

class B implements A {
  get(s) {
    // s 的类型是 any
    return true;
  }
}
```

implements 关键字后面，不仅可以是接口，也可以是另一个类。这时，后面的类将被当作接口。

```typescript
class Car {
  id: number = 1;
  move(): void {}
}

class MyCar implements Car {
  id = 2; // 不可省略
  move(): void {} // 不可省略
}
```

类可以实现多个接口（其实是接受多重限制），每个接口之间使用逗号分隔。

```typescript
class Car implements MotorVehicle, Flyable, Swimmable {
  // ...
}
```

TypeScript 不允许两个同名的类，但是如果一个类和一个接口同名，那么接口会被合并进类。

#### class 的类型

typescript 的类本身就是一种类型，代表该类的实例的类型，但不是 class 本身的类型。

要获得类的自身类型，一个简便的方法是 typeof 运算符。

```typescript
function createPoint(PointClass: typeof Point, x: number, y: number): Point {
  return new PointClass(x, y);
}
```

另外，javascript 中类只是构造函数的语法糖，本质上是构造函数的另外一种写法，所以类的自身类型可以写成构造函数的形式：

```typescript
function createPoint(
  PointClass: new (x: number, y: number) => Point,
  x: number,
  y: number
): Point {
  return new PointClass(x, y);
}
```

根据上面的写法，可以把构造函数提取出来，单独定义一个接口（interface），这样可以大大提高代码的通用性。

```typescript
interface PointConstructor {
  new (x: number, y: number): Point;
}

function createPoint(
  PointClass: PointConstructor,
  x: number,
  y: number
): Point {
  return new PointClass(x, y);
}
```

#### 类的继承

#### override 关键字

子类继承父类时，可以覆盖父类的同名方法。但是这可能会造成隐式的覆盖问题，因此 typeScript 提供了一个编译参数 noImplicitOverride。一旦打开这个参数，子类覆盖父类的同名方法就会报错，除非使用了 override 关键字。

#### 可访问性修饰符

##### public

public 是默认修饰符， 表示公开属性，外部可以自由访问。

##### private

private 修饰符表示私有成员，只能在当前类内部使用，类的实例和子类都不能访问该成员。

另外 es2022 提供了私有属性的写法，可以替代 private 修饰符

```typescript
class A {
  #x = 1;
}
```

##### protected

protected 修饰符表示该成员是保护成员，只能在类的内部使用该成员，实例无法使用该成员，但是子类内部可以使用。

#### 实例属性的简洁声明

```typescript
class Point {
  constructor(public x: number, public y: number) {}
}
```

#### 顶层属性的处理方法

#### 静态成员

类的内部可以使用 static 关键字，定义静态成员。
static 关键字前面可以使用 public、private、protected 修饰符。

```typescript
class MyClass {
  private static x = 0;
}
```

#### 泛型类

类可以使用泛型参数定为泛型类

```typescript
class genericClass<T> {
  content: T;
  constructor(value: t) {
    this.content = value;
  }
}
```

注意，静态成员不能使用泛型的类型参数。

#### 抽象类 & 抽象成员

TypeScript 允许在类的定义前面，加上关键字 abstract，表示该类不能被实例化，只能当作其他类的模板。这种类就叫做“抽象类”（abstract class）。

#### this 的处理

有些场合需要给出 this 类型，但是 JavaScript 函数通常不带有 this 参数，这时 TypeScript 允许函数增加一个名为 this 的参数，放在参数列表的第一位，用来描述函数内部的 this 关键字的类型。

```typescript
// 编译前
function fn(this: SomeType, x: number) {
  /* ... */
}

// 编译后
function fn(x) {
  /* ... */
}
```

TypeScript 提供了一个 noImplicitThis 编译选项。如果打开了这个设置项，如果 this 的值推断为 any 类型，就会报错。

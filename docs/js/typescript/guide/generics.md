### generics

泛型的特点就是带有“类型参数”（type parameter）。

#### 泛型的写法

泛型只要用于四个场合： 函数，接口，类和别名。

##### 函数

```typescript
function id<T>(arg: T): T {
  return arg;
}
```

##### 接口

```typescript
interface Box<Type> {
  contents: Type;
}
```

##### 类

```typescript
class Pair<K, V> {
  key: K;
  value: V;
}
```

#### type

```typescript
type Nullable<T> = T | undefined | null;
```

#### 泛型参数默认值

```typescript
function getFirst<T = string>(arr: T[]): T {
  return arr[0];
}
```

#### 泛型数组

#### 泛型约束条件

TypeScript 提供了一种语法，允许在类型参数上面写明约束条件，如果不满足条件，编译时就会报错。这样也可以有良好的语义，对类型参数进行说明。

类型参数的约束条件采用下面的形式。

```typescript
<TypeParameter extends ConstraintType>
```

上面语法中，TypeParameter 表示类型参数，extends 是关键字，这是必须的，ConstraintType 表示类型参数要满足的条件，即类型参数应该是 ConstraintType 的子类型。

```typescript
function comp<T extends { length: number }>(a: T, b: T) {
  if (a.length >= b.length) {
    return a;
  }
  return b;
}
```

### How to improve web page performance

#### separate the biz code and third party dependencies

#### how to eliminate duplicate dependencies ?

#### how to deal with the situation that import the same dependency by sync and async way?

#### how to do the dynamic import, tree shaking, scope host ?

#### what is the difference between ES Module and CommonJs Module

#### which rendering mode you should use ?

#### how does the browser preload scanner work ?

#### what benefits we can get from streaming the request resources ?

#### how does bundler(webpack, vite) work ?

#### how does client hint work ?

#### how does SSG work ?

### Reference

> [front-end perform checklist](https://www.smashingmagazine.com/2021/01/front-end-performance-2021-free-pdf-checklist/#top)

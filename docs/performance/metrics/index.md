# 性能指标

## 是什么

下定义

与性能相关的`指标`, 量化的性能特征

指标：行为的量化；

### 指标的分类：不同类型指标适用不同场景

#### quantity based(资源阈值)

html, js, css, font, image, audio, video

- 请求数量

- 请求大小

- 节点数

- 内存

> 优：易监测、引起警惕 <br>
> 缺：跟性能间接关联

#### milestone metrics(事件)

- DomContentLoad

- Load

- FP

- FCP

- LCP

- TTFB(time to first byte): 接收到第一个字节

- TTI(time to interactive): 连续 5s 没有超出 50ms 的长任务

> 优：反映体验，易监测跟踪<br/>
> 缺：时间节点之间的演变不可知

#### render metrics

user timing,

- speedIndex: 页面填充速度
- cls: 页面填充的稳定度

> <br>
> 优：反映渲染性能 <br>
> 缺: 无优先级，无法反映局部情况<br>
> <br>

#### custom metric

根据实际需要进行定制，如组件挂载时间等

> 优：灵活性高 <br>
> 缺: 不可复用及横向对比<br>

#### 其它

- TBT: FP 和 TTI 的间隔时间

- CPU time spend: 主线程的阻塞时间

- Component-Level CPU Const

- FrustrationIndex

- Deviation Metrics

  ![Timeline](./images/metric.webp)

- 常规指标

  - load
  - server response time
  - start render time

- 自定义指标

- Synthetic testing tools & RUM: 实验室 & 真实用户场景

#### web-vitals

- LCP：主要内容渲染时间，等待时间

- CLS: 布局变化，稳定性

- FID：RUM 指标，用户首次输入的响应时间，响应时间

Not all metrics are equally important. In the end, it's not end load event or server response time that define the experience, but the perception of how snappy the interface feel

### why

性能优化的基础

性能优化的结果是指标的优化，指标的提升是优化要交付的结果

指标演进过程，存在什么问题，为了解决什么问题进行演进
a joyous user experience

### how

指标测量

目标&阈值

工作流整合

测试工具

module.exports = {
  ci: {
    collect: {
      url: ['https://www.nounknow.fun/login'],
      numberOfRuns: 1
    },
    settings: { chromeFlags: '--no-sandbox' },
    upload: {
      target: 'temporary-public-storage'
    }
  }
};

// command: lhci autorun --upload.target=temporary-public-storage --collect.settings.chromeFlags="--no-sandbox --headless --disable-gpu"
